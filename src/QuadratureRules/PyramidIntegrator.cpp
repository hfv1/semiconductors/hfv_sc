#include "PyramidIntegrator.h"
#include "DunavantQuadratureRule.h"

PyramidIntegrator::PyramidIntegrator(const Mesh * Th, const Integer & iK, const Integer & iF_loc, const Integer & doe)
{
  VF_ASSERT(iK < Th->numberOfCells());
  const Cell & K = Th->cell(iK);
  VF_ASSERT(iF_loc < K.numberOfFaces());
  const Point & xK = K.center();
  const Face & F = Th->face(K.faceId(iF_loc));
  const Point & xF = F.barycenter();
  const Real & mF = F.measure();
  const Point & nF = F.normal();

  Real dKF = std::fabs(inner_prod(nF, xF - xK));

  Integer _doe = std::max(doe, 1);

  // Geometric map
  Real mKF = dKF * mF / DIM;
  RealMatrix2x2 M;
  boost::numeric::ublas::matrix_column<RealMatrix2x2> c1(M,0); c1 = F.point(0).first - xK;
  boost::numeric::ublas::matrix_column<RealMatrix2x2> c2(M,1); c2 = F.point(1).first - xK;

  // Quadrature rule
  DunavantQuadratureRule qr(_doe);
  m_points.resize(qr.numberOfNodes());
  m_weights.resize(qr.numberOfNodes());
  for(std::size_t iQN = 0; iQN < qr.numberOfNodes(); iQN++) {
    m_weights[iQN] = mKF * qr.weight(iQN);
    m_points[iQN] = prod(M, qr.point(iQN)) + xK;
  } // for iQN
}
