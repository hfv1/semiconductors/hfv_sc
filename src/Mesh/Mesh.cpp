#include <algorithm>
#include <vector>
#include <fstream>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>

#include "Mesh.h"
#include "ITransformation.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

void Mesh::readFVCA5File_typ1(const char * a_file, ITransformation * a_transformation)
{
  std::fstream fvca5_file(a_file, std::ios::in);
  std::string marker;
  
  // Read nodes
  fvca5_file >> marker;
  while(marker.compare("vertices")) {
    fvca5_file >> marker;
    assert(!fvca5_file.eof());
  }
  Integer NP;
  fvca5_file >> NP;
  std::cout << "[ReadFVCA5Mesh] Reading " << NP << " nodes" << std::endl;
  m_points.resize(NP);
  m_nodes.resize(NP);
  for(Integer i = 0; i < NP; i++) {
    Real x, y;
    fvca5_file >> x >> y;
    Point P(2);
    P(0) = x; 
    P(1) = y;
    if(a_transformation!=NULL)
      m_points[i] = a_transformation->apply(P);
    else
      m_points[i] = P;
    m_nodes[i].putPoint(m_points[i]);
  }

  // Initialize maximum number of faces
  m_max_n_faces = 0;

  // Read triangles
  fvca5_file >> marker;
  while(marker.compare("triangles")) {
    fvca5_file >> marker;
    assert(!fvca5_file.eof());
  }
  int NT;
  fvca5_file >> NT;
  if(NT) m_max_n_faces = 3;
  std::vector<Cell> triangles;
  triangles.reserve(NT);
  std::cout << "[ReadFVCA5Mesh] Reading " << NT << " triangles" << std::endl;
  for(Integer i=0; i<NT; i++) {
    Integer n0, n1, n2;
    fvca5_file >> n0 >> n1 >> n2;

    Face area(3, 0, 0);
    area.addPoint(0, m_points[n0-1]);
    area.addPoint(1, m_points[n1-1]);
    area.addPoint(2, m_points[n2-1]);

    area.compute(Face::P_MEASURE | Face::P_CENTEROFGRAVITY);

    Cell cellk(3, area.centerOfGravity());
    cellk.setParameter(Cell::P_MEASURE, area.measure());	   
    cellk.setParameter(Cell::P_NODENUMBER, n0-1);
    cellk.setParameter(Cell::P_NODENUMBER, n1-1);
    cellk.setParameter(Cell::P_NODENUMBER, n2-1);
	   
    triangles.push_back(cellk);
  }

  // Read quadrangles
  fvca5_file >> marker;
  while(marker.compare("quadrangles")) {
    fvca5_file >> marker;
    assert(!fvca5_file.eof());
  }
  fvca5_file >> NT;
  if(NT) m_max_n_faces = 4;
  std::vector<Cell> quadrangles;
  quadrangles.reserve(NT);
  std::cout << "[ReadFVCA5Mesh] Reading " << NT << " quadrangles" << std::endl;
  for(Integer i=0; i<NT; i++) {
    Integer n0, n1, n2, n3;
    fvca5_file >> n0 >> n1 >> n2 >> n3;

    Face area(4, 0, 0);
    area.addPoint(0, m_points[n0-1]);
    area.addPoint(1, m_points[n1-1]);
    area.addPoint(2, m_points[n2-1]);
    area.addPoint(3, m_points[n3-1]);
    area.compute(Face::P_MEASURE  | Face::P_CENTEROFGRAVITY);

    Cell cellk(4, area.centerOfGravity());
    cellk.setParameter(Cell::P_MEASURE, area.measure());
    cellk.setParameter(Cell::P_NODENUMBER, n0-1);
    cellk.setParameter(Cell::P_NODENUMBER, n1-1);
    cellk.setParameter(Cell::P_NODENUMBER, n2-1);
    cellk.setParameter(Cell::P_NODENUMBER, n3-1);
	   
    quadrangles.push_back(cellk);
  }

  // Read pentagon
  std::vector<Cell> pentagons;
  fvca5_file >> marker;  
  if(!marker.compare("pentagons")) { // The mesh contains pentagons
    fvca5_file >> NT;
    if(NT) m_max_n_faces = 5;
    std::cout << "[ReadFVCA5Mesh] Reading " << NT << " pentagons" << std::endl;
    for(Integer i=0; i<NT; i++) {
      Integer n0, n1, n2, n3, n4;
      fvca5_file >> n0 >> n1 >> n2 >> n3 >> n4;

      Face area(5, 0, 0);
      area.addPoint(0, m_points[n0-1]);
      area.addPoint(1, m_points[n1-1]);
      area.addPoint(2, m_points[n2-1]);
      area.addPoint(3, m_points[n3-1]);
      area.addPoint(4, m_points[n4-1]);

      area.compute(Face::P_MEASURE | Face::P_CENTEROFGRAVITY);

      Cell cellk(5, area.centerOfGravity());
      cellk.setParameter(Cell::P_MEASURE, area.measure());	   
      cellk.setParameter(Cell::P_NODENUMBER, n0-1);
      cellk.setParameter(Cell::P_NODENUMBER, n1-1);
      cellk.setParameter(Cell::P_NODENUMBER, n2-1);
      cellk.setParameter(Cell::P_NODENUMBER, n3-1);
      cellk.setParameter(Cell::P_NODENUMBER, n4-1);
	   
      pentagons.push_back(cellk);
    }
  } 

  // Read hexagons
  std::vector<Cell> hexagons;
  fvca5_file >> marker;
  if(!marker.compare("hexagons")) { // The mesh contains hexagons

    fvca5_file >> NT;
    if(NT) m_max_n_faces = 6;
    std::cout << "[ReadFVCA5Mesh] Reading " << NT << " hexagons" << std::endl;
    for(Integer i=0; i<NT; i++) {
      Integer n0, n1, n2, n3, n4, n5;
      fvca5_file >> n0 >> n1 >> n2 >> n3 >> n4 >> n5;

      Face area(6, 0, 0);
      area.addPoint(0, m_points[n0-1]);
      area.addPoint(1, m_points[n1-1]);
      area.addPoint(2, m_points[n2-1]);
      area.addPoint(3, m_points[n3-1]);
      area.addPoint(4, m_points[n4-1]);
      area.addPoint(5, m_points[n5-1]);

      area.compute(Face::P_MEASURE | Face::P_CENTEROFGRAVITY);

      Cell cellk(6, area.centerOfGravity());
      cellk.setParameter(Cell::P_MEASURE, area.measure());	   
      cellk.setParameter(Cell::P_NODENUMBER, n0-1);
      cellk.setParameter(Cell::P_NODENUMBER, n1-1);
      cellk.setParameter(Cell::P_NODENUMBER, n2-1);
      cellk.setParameter(Cell::P_NODENUMBER, n3-1);
      cellk.setParameter(Cell::P_NODENUMBER, n4-1);
      cellk.setParameter(Cell::P_NODENUMBER, n5-1);
	   
      hexagons.push_back(cellk);
    }
  }
  
  // Copy all elements to m_cells
  m_cells.reserve(triangles.size()+quadrangles.size()+pentagons.size()+hexagons.size());
  std::copy(triangles.begin(), triangles.end(), std::back_inserter(m_cells));
  std::copy(quadrangles.begin(), quadrangles.end(), std::back_inserter(m_cells));
  std::copy(pentagons.begin(), pentagons.end(), std::back_inserter(m_cells));
  std::copy(hexagons.begin(), hexagons.end(), std::back_inserter(m_cells));

  // Read the number of boundary faces
  fvca5_file >> marker;
  while(marker.compare("boundary")) {
    fvca5_file >> marker;
    assert(!fvca5_file.eof());
  }
  int NBF;
  fvca5_file >> NBF;

  // Read faces
  fvca5_file >> marker;
  while(marker.compare("edges")) {
    fvca5_file >> marker;
    assert(!fvca5_file.eof());
  }
  int NF;
  fvca5_file >> NF;
  std::cout << "[ReadFVCA5Mesh] Reading " << NF << " faces" << std::endl;
  m_faces.reserve(NF);
  for(Integer i = 0; i < NF; i++){
    Integer n0, n1, m0, m1;
    fvca5_file >> n0 >> n1 >> m0 >> m1;
	
    Face edge(2, m0-1, m1-1);
    edge.addRegion(10);
    edge.addPoint(n0-1, m_points[n0-1]);
    edge.addPoint(n1-1, m_points[n1-1]);
    edge.compute(Face::P_NORMAL | Face::P_BARYCENTER | Face::P_MEASURE);
    m_faces.push_back(edge);
	
    // faces sharing a node
    m_nodes[n0-1].putNumeroFace(i);
    m_nodes[n1-1].putNumeroFace(i);
	
    // cells sharing a node
    if(m0 > 0){
      m_nodes[n0-1].putNumeroMaille(m0-1);
      m_nodes[n1-1].putNumeroMaille(m0-1);
    }
	
    if(m1 > 0){
      m_nodes[n0-1].putNumeroMaille(m1-1);
      m_nodes[n1-1].putNumeroMaille(m1-1);
    }
	
    //faces of a cell
    if(m0 > 0)
      m_cells[m0-1].setParameter(Cell::P_FACENUMBER, i);
    if(m1 > 0)
      m_cells[m1-1].setParameter(Cell::P_FACENUMBER, i);
  }
}

/*----------------------------------------------------------------------------*/

void Mesh::readFVCA5File_typ12(const char * a_file, ITransformation * a_transformation)
{
  std::fstream fvca5_file(a_file, std::ios::in);
  std::string marker;
  
  // Read nodes
  fvca5_file >> marker;
  while(marker.compare("vertices")) {
    fvca5_file >> marker;
    assert(!fvca5_file.eof());
  }
  Integer NP;
  fvca5_file >> NP;
  std::cout << "[ReadFVCA5Mesh] Reading " << NP << " nodes" << std::endl;
  m_points.resize(NP);
  m_nodes.resize(NP);
  for(Integer i = 0; i < NP; i++) {
    Real x, y;
    fvca5_file >> x >> y;
    Point P(2);
    P(0) = x; 
    P(1) = y;
    if(a_transformation!=NULL)
      m_points[i] = a_transformation->apply(P);
    else
      m_points[i] = P;
    m_nodes[i].putPoint(m_points[i]);
  }

  // Initialize maximum number of faces
  m_max_n_faces = 0;

  // Read cells
  std::vector<Cell> cells;
  fvca5_file >> marker;
  Integer Nb_cells;
  if(!marker.compare("cells")) { // The mesh contains cells (very usefull)

    fvca5_file >> Nb_cells;
    std::cout << "[ReadFVCA5Mesh] Reading " << Nb_cells << " cells" << std::endl;
    for(Integer k=0; k<Nb_cells; k++) {
      Integer nb_faces_cell;
      fvca5_file >> nb_faces_cell;
      m_max_n_faces = std::max(m_max_n_faces, nb_faces_cell);
      // read the face of the cell
      std::vector<Integer> list_faces; 
      list_faces.resize(nb_faces_cell); 
      for(int j = 0; j < nb_faces_cell ; j++ ){
        fvca5_file >> list_faces[j];
      } // for j 

      Face area(nb_faces_cell, 0, 0);
      for(int j = 0; j < nb_faces_cell ; j++ ){
        area.addPoint(j, m_points[list_faces[j]-1]);
      } // for j 

      area.compute(Face::P_MEASURE | Face::P_CENTEROFGRAVITY);
      
      Cell cellk(nb_faces_cell, area.centerOfGravity());
      cellk.setParameter(Cell::P_MEASURE, area.measure());	
      for(int j = 0; j < nb_faces_cell ; j++ ){
        cellk.setParameter(Cell::P_NODENUMBER, list_faces[j]-1);
      } // for j 
	   
      cells.push_back(cellk);
      //std::cout << "Cell n° " << k << " generated " << std::endl;
    } // for k 
  }

  
  
  // Copy all elements to m_cells
  m_cells.reserve(cells.size());
  std::copy(cells.begin(), cells.end(), std::back_inserter(m_cells));

  // Read the number of boundary faces
  fvca5_file >> marker;
  while(marker.compare("boundary")) {
    fvca5_file >> marker;
    assert(!fvca5_file.eof());
  }
  int NBF;
  fvca5_file >> NBF;

  // Read faces
  fvca5_file >> marker;
  while(marker.compare("edges")) {
    fvca5_file >> marker;
    assert(!fvca5_file.eof());
  }
  int NF;
  fvca5_file >> NF;
  std::cout << "[ReadFVCA5Mesh] Reading " << NF << " faces" << std::endl;
  m_faces.reserve(NF);
  for(Integer i = 0; i < NF; i++){
    Integer n0, n1, m0, m1;
    fvca5_file >> n0 >> n1 >> m0 >> m1;
	
    Face edge(2, m0-1, m1-1);
    edge.addRegion(10);
    edge.addPoint(n0-1, m_points[n0-1]);
    edge.addPoint(n1-1, m_points[n1-1]);
    edge.compute(Face::P_NORMAL | Face::P_BARYCENTER | Face::P_MEASURE);
    m_faces.push_back(edge);
	
    // faces sharing a node
    m_nodes[n0-1].putNumeroFace(i);
    m_nodes[n1-1].putNumeroFace(i);
	
    // cells sharing a node
    if(m0 > 0){
      m_nodes[n0-1].putNumeroMaille(m0-1);
      m_nodes[n1-1].putNumeroMaille(m0-1);
    }
	
    if(m1 > 0){
      m_nodes[n0-1].putNumeroMaille(m1-1);
      m_nodes[n1-1].putNumeroMaille(m1-1);
    }
	
    //faces of a cell
    if(m0 > 0)
      m_cells[m0-1].setParameter(Cell::P_FACENUMBER, i);
    if(m1 > 0)
      m_cells[m1-1].setParameter(Cell::P_FACENUMBER, i);
  }
}

/*----------------------------------------------------------------------------*/
void Mesh::readDGMFile(const char * a_file, ITransformation * a_transformation)
{
  std::fstream dgm_file(a_file, std::ios::in);
  std::string marker;
  
  // Read nodes
  dgm_file >> marker;
  while(marker.compare("$NODES")) {
    dgm_file >> marker;
    assert(!dgm_file.eof());
  }
  Integer NP;
  dgm_file >> NP;
  std::cout << "[ReadDGMMesh] Reading " << NP << " nodes" << std::endl;
  m_points.resize(NP);
  m_nodes.resize(NP);
  for(Integer i=0; i<NP; i++) {
    Real x, y, z;
    dgm_file >> x >> y >> z;
    Point P(2);
    P(0) = x; 
    P(1) = y;
    if(a_transformation!=NULL)
      m_points[i] = a_transformation->apply(P);
    else
      m_points[i] = P;
    m_nodes[i].putPoint(m_points[i]);
  }

  // Read volumes
  dgm_file >> marker;
  while(marker.compare("$VOLUMES")) {
    dgm_file >> marker;
    assert(!dgm_file.eof());
  }
  int NT;
  dgm_file >> NT;
  std::vector<Cell> volumes;
  m_cells.reserve(NT);
  std::cout << "[ReadDGMMesh] Reading " << NT << " volumes" << std::endl;
  for(Integer i=0; i<NT; i++) {
    Integer type, region, NN;
    dgm_file >> type >> region >> NN;

    Face area(NN, 0, 0);
    Cell cellk(NN);
    for(Integer j=0; j<NN; j++) {
      Integer node;
      dgm_file >> node;
      area.addPoint(j, m_points[node]);
      cellk.setParameter(Cell::P_NODENUMBER, node);
    }
    area.compute(Face::P_MEASURE | Face::P_CENTEROFGRAVITY);
    cellk.setParameter(Cell::P_CENTER, area.centerOfGravity());
    cellk.setParameter(Cell::P_MEASURE, area.measure());	   
	   
    m_cells.push_back(cellk);
  }

  // Read the number of interfaces
  dgm_file >> marker;
  while(marker.compare("$INTERFACES")) {
    dgm_file >> marker;
    assert(!dgm_file.eof());
  }
  int NIF;
  dgm_file >> NIF;
  std::cout << "[ReadDGMMesh] Reading " << NIF << " interfaces" << std::endl;
  std::vector<Face> interfaces;
  interfaces.reserve(NIF);
  for(Integer i=0; i<NIF; i++) {
    int type, K1, pos1, K2, pos2, NN;
    dgm_file >> type >> K1 >> pos1 >> K2 >> pos2 >> NN;
    Face edge(NN, K1, K2);
    for(Integer j=0; j<NN; j++) {
      Integer node;
      dgm_file >> node;
      edge.addPoint(node, m_points[node]);
      // faces sharing a node
      m_nodes[node].putNumeroFace(i);
      // cells sharing a node
      m_nodes[node].putNumeroMaille(K1);
      m_nodes[node].putNumeroMaille(K2);
    }
    // faces of a cell
    m_cells[K1].setParameter(Cell::P_FACENUMBER, i);
    m_cells[K2].setParameter(Cell::P_FACENUMBER, i);
    edge.compute(Face::P_NORMAL | Face::P_BARYCENTER | Face::P_MEASURE);
    interfaces.push_back(edge);
  }

  // Read the number of boundary faces
  dgm_file >> marker;
  while(marker.compare("$BOUNDARYFACES")) {
    dgm_file >> marker;
    assert(!dgm_file.eof());
  }
  int NBF;
  dgm_file >> NBF;
  std::cout << "[ReadDGMMesh] Reading " << NBF << " boundary faces" << std::endl;
  std::vector<Face> boundary_faces;
  boundary_faces.reserve(NBF);
  for(Integer i=0; i<NBF; i++) {
    Integer type, K1, pos1, region, NN;
    dgm_file >> type >> K1 >> pos1 >> region >> NN;
    Face edge(NN, K1, -1);
    edge.addRegion(region);
    for(Integer j=0; j<NN; j++) {
      Integer node;
      dgm_file >> node;
      edge.addPoint(node, m_points[node]);
      // faces sharing a node
      m_nodes[node].putNumeroFace(NIF+i);
      // cells sharing a node
      m_nodes[node].putNumeroMaille(K1);
    }
    // faces of a cell
    m_cells[K1].setParameter(Cell::P_FACENUMBER, NIF+i);
    edge.compute(Face::P_NORMAL | Face::P_BARYCENTER | Face::P_MEASURE);
    boundary_faces.push_back(edge);
  }

  // Copy everything to face vector
  m_faces.reserve(NIF+NBF);
  for(std::vector<Face>::const_iterator iF=interfaces.begin(); iF!=interfaces.end(); iF++)
    m_faces.push_back(*iF);
  for(std::vector<Face>::const_iterator iF=boundary_faces.begin(); iF!=boundary_faces.end(); iF++)
    m_faces.push_back(*iF);

  // Compute maximum number of faces
  m_max_n_faces = 0;
  for(Integer iK = 0; iK < this->numberOfCells(); iK++) {
    const Cell & K = this->cell(iK);
    m_max_n_faces = std::max(m_max_n_faces, K.numberOfFaces());
  }
}

/*----------------------------------------------------------------------------*/

void Mesh::buildFaceGroups() 
{
  std::cout << "-- Building face groups" << std::endl;
  for(int iF = 0; iF < this->numberOfFaces(); iF++) {
    const Face & F = this->m_faces[iF];
    if(F.frontCell() != -1) 
      m_internal_faces.push_back(iF);
    else
      m_boundary_faces.push_back(iF);
  }
  std::cout << m_internal_faces.size() << " internal faces" << std::endl;
  std::cout << m_boundary_faces.size() << " boundary faces" << std::endl;
}

/*----------------------------------------------------------------------------*/

void Mesh::buildSubMesh()
{
  for(Integer iT=0; iT<this->numberOfCells(); iT++) {
    Cell & T = this->m_cells[iT];
    T.buildSubMesh(this);
  }
}

/*----------------------------------------------------------------------------*/

Mesh::PointVectorConstIterators Mesh::points() const
{
  return PointVectorConstIterators(m_points.begin(), m_points.end());
}

/*----------------------------------------------------------------------------*/

Mesh::FaceVectorConstIterators Mesh::faces() const
{
  return FaceVectorConstIterators(m_faces.begin(), m_faces.end());
}

/*----------------------------------------------------------------------------*/

Mesh::CellVectorConstIterators Mesh::cells() const
{
  return CellVectorConstIterators(m_cells.begin(), m_cells.end());
}

/*----------------------------------------------------------------------------*/

Mesh::NodeVectorConstIterators Mesh::nodes() const
{
  return NodeVectorConstIterators(m_nodes.begin(), m_nodes.end());
}

/*----------------------------------------------------------------------------*/

Mesh::IdListConstIterators Mesh::internalFaces() const
{
  return IdListConstIterators(m_internal_faces.begin(), m_internal_faces.end());
}

/*----------------------------------------------------------------------------*/

Mesh::IdListConstIterators Mesh::boundaryFaces() const
{
  return IdListConstIterators(m_boundary_faces.begin(), m_boundary_faces.end());
}

/*----------------------------------------------------------------------------*/

Integer Mesh::numberOfPoints() const
{
  return m_points.size();
}

/*----------------------------------------------------------------------------*/

Integer Mesh::numberOfFaces() const
{
  return m_faces.size();
}

/*----------------------------------------------------------------------------*/

Integer Mesh::numberOfCells() const
{
  return m_cells.size();
}

/*----------------------------------------------------------------------------*/

Integer Mesh::numberOfNodes() const
{
  return m_nodes.size();
}

/*----------------------------------------------------------------------------*/

Integer Mesh::numberOfInternalFaces() const
{
  return m_internal_faces.size();
}

/*----------------------------------------------------------------------------*/

Integer Mesh::numberOfInternalFaces(const Integer & iT) const
{
  const Cell & T = m_cells[iT];
  Integer number_of_internal_faces_T = 0;
  for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
    if(m_faces[T.faceId(iF_loc)].isInternal()) { number_of_internal_faces_T++; }
  } // for iF_loc
  return number_of_internal_faces_T;
}

/*----------------------------------------------------------------------------*/

Integer Mesh::numberOfBoundaryFaces() const
{
  return m_boundary_faces.size();
}

/*----------------------------------------------------------------------------*/

Real Mesh::meshsize() const {
  Real h = 0.;
  for(const auto & K : this->cellVector()) {
    const Real & mK = K.measure();
    Real pK = 0.;
    for(Integer iF = 0; iF < K.numberOfFaces(); iF++) {
      pK +=  this->face(iF).measure();
    }
    h = std::max(h, mK / pK);
  }
  return h;
}

/*----------------------------------------------------------------------------*/

const Point & Mesh::point(Integer a_i) const
{
  return m_points[a_i];
}

/*----------------------------------------------------------------------------*/

const Face & Mesh::face(Integer a_i) const
{
  return m_faces[a_i];
}

/*----------------------------------------------------------------------------*/

const Cell & Mesh::cell(Integer a_i) const
{
  return m_cells[a_i];
}

/*----------------------------------------------------------------------------*/

const Node & Mesh::node(Integer a_i) const
{
  return m_nodes[a_i];
}

/*----------------------------------------------------------------------------*/

Point & Mesh::point(Integer a_i)
{
  return m_points[a_i];
}

/*----------------------------------------------------------------------------*/

Face & Mesh::face(Integer a_i)
{
  return m_faces[a_i];
}

/*----------------------------------------------------------------------------*/

Cell & Mesh::cell(Integer a_i)
{
  return m_cells[a_i];
}

/*----------------------------------------------------------------------------*/

Node & Mesh::node(Integer a_i)
{
  return m_nodes[a_i];
}

/*----------------------------------------------------------------------------*/

std::ostream & operator<<(std::ostream & a_ostr, const Mesh & a_Th)
{
  a_ostr << "Mesh" << std::endl;
  a_ostr << "-- number of cells: " << a_Th.numberOfCells() << std::endl;
  a_ostr << "-- number of faces: " << a_Th.numberOfFaces() << std::endl;
  a_ostr << "-- number of nodes: " << a_Th.numberOfNodes() << std::flush;  
  return a_ostr;
}

/*----------------------------------------------------------------------------*/

Real cell_diameter(const Mesh * a_Th, const Integer & iT)
{
  const Cell & T = a_Th->cell(iT);
  const Real & mT = T.measure();
  Real pT = 0.;
  for(Integer iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
    pT += a_Th->face(T.faceId(iF_loc)).measure();
  }
  return mT / pT;
}

/*----------------------------------------------------------------------------*/


int id_cell_of_point(const Mesh * a_Th, const Point & x)
{
  const int nb_cells = a_Th->numberOfCells(); 
  bool not_identified = true; 
  int iT = 0;

  while(not_identified){
    const Cell & T = a_Th->cell(iT);
    const Real & mT = T.measure();
    const Real radius = 0.5 * std::sqrt(mT);
    const Point & x_T =  T.center();

    const Point dx = x-x_T;
    Real abs_dx_1 = std::fabs( dx(0)); 
    Real abs_dx_2 = std::fabs( dx(1)); 

    bool in_T = (abs_dx_1 <= radius) && (abs_dx_2 <= radius) ;

    if(in_T){
      not_identified = false; 
    } // if x is in T
    else{
      iT+=1;
    } //if x is not in T

  } // while 


  return iT;
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

