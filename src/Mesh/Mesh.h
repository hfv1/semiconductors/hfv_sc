// -*- C++ -*-
#ifndef MESH_H 
#define MESH_H 1

#include <list>
#include <vector>
#include <set>
#include <boost/numeric/ublas/matrix.hpp>

#include "Common/defs.h"
#include "Cell.h"
#include "Node.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

using namespace std;
namespace ublas = boost::numeric::ublas;

/*----------------------------------------------------------------------------*/

class ITransformation;
class Mesh 
{
public:
  typedef std::vector<Point> PointVectorType;
  typedef std::vector<Face> FaceVectorType;
  typedef std::vector<Cell> CellVectorType;
  typedef std::vector<Node> NodeVectorType;
  typedef std::list<Integer> IdListType;

  typedef std::pair<PointVectorType::const_iterator,
                    PointVectorType::const_iterator> PointVectorConstIterators;
  typedef std::pair<FaceVectorType::const_iterator,
                    FaceVectorType::const_iterator> FaceVectorConstIterators;
  typedef std::pair<CellVectorType::const_iterator,
                    CellVectorType::const_iterator> CellVectorConstIterators;
  typedef std::pair<NodeVectorType::const_iterator,
                    NodeVectorType::const_iterator> NodeVectorConstIterators;
  typedef std::pair<IdListType::const_iterator,
                    IdListType::const_iterator> IdListConstIterators;

public:
  //! Internal faces
  IdListType m_internal_faces; 
  //! Boundary faces
  IdListType m_boundary_faces;

  //! Default constructor
  Mesh()
  {
    // Do nothing
  }

  //! Constructor
  Mesh(int a_cells, int a_faces, int a_points)
    : m_points(a_points),
      m_faces(a_faces),
      m_cells(a_cells),
      m_nodes(a_points)
  {
    for(NodeVectorType::iterator iP=m_nodes.begin(); iP!=m_nodes.end(); iP++)
      iP->mj = -1;
  }

  
  //! Read mesh in fvca5 format typ1
  void readFVCA5File_typ1(const char * a_file, ITransformation * a_transformation = NULL);  
  //! Read mesh in fvca5 format typ12 ()
  void readFVCA5File_typ12(const char * a_file, ITransformation * a_transformation = NULL);
  //! Read mesh in dgm format
  void readDGMFile(const char * a_file, ITransformation * a_transformation = NULL);
  //! Build face groups
  void buildFaceGroups();
  //! Build subfaces
  void buildSubMesh();

  //! Traverse points
  PointVectorConstIterators points() const;
  //! Traverse faces
  FaceVectorConstIterators faces() const;
  //! Traverse cells
  CellVectorConstIterators cells() const;
  const std::vector<Cell> & cellVector() const 
  {
    return m_cells;
  }
  //! Traverse nodes
  NodeVectorConstIterators nodes() const;
  //! Traverse internal faces
  IdListConstIterators internalFaces() const;
  //! Return the group of internal faces
  inline const Mesh::IdListType & internalFacesGroup() const
  {
    return m_internal_faces;
  }
  //! Traverse boundary faces
  IdListConstIterators boundaryFaces() const;
  //! Return the group of boundary faces
  inline const Mesh::IdListType & boundaryFacesGroup() const
  {
    return m_boundary_faces;
  }

  //! Return the \f$i\f$th point
  const Point & point(Integer a_i) const;
  //! Return the \f$i\f$th face
  const Face & face(Integer a_i) const;
  //! Return the \f$i\f$th cell
  const Cell & cell(Integer a_i) const;
  //! Return the \f$i\f$th node
  const Node & node(Integer a_i) const;

  //! Return the \f$i\f$th point
  Point & point(Integer a_i);
  //! Return the \f$i\f$th face
  Face & face(Integer a_i);
  //! Return the \f$i\f$th cell
  Cell & cell(Integer a_i);
  //! Return the \f$i\f$th node
  Node & node(Integer a_i);

  //! Return the number of points
  Integer numberOfPoints() const;
  //! Return the number of faces
  Integer numberOfFaces() const;
  //! Return the number of cells
  Integer numberOfCells() const;
  //! Return the number of nodes
  Integer numberOfNodes() const;
  //! Return the number of internal faces
  Integer numberOfInternalFaces() const;
  //! Return the number of internal faces of a cell
  Integer numberOfInternalFaces(const Integer & iT) const;
  //! Return the number of boundary faces
  Integer numberOfBoundaryFaces() const;
  //! Return the maximum number of faces of one element
  const Integer & maximumNumberOfFaces() const
  {
    return m_max_n_faces;
  }
  //! Estimate meshsize
  Real meshsize() const;

private:
  std::vector<Point> m_points;
  std::vector<Face> m_faces;
  std::vector<Cell> m_cells;
  std::vector<Node> m_nodes;
  Integer m_max_n_faces;
};

/*----------------------------------------------------------------------------*/

std::ostream & operator<<(std::ostream & a_ostr, const Mesh & a_Th);

/*----------------------------------------------------------------------------*/

Real cell_diameter(const Mesh * a_Th, const Integer & iT);

/*----------------------------------------------------------------------------*/

// given a point x, give the id of the cell in which x is 
// ONLY WORKS FOR CARTESIAN MESHES 
int id_cell_of_point(const Mesh * a_Th, const Point & x);

/*----------------------------------------------------------------------------*/

#endif
