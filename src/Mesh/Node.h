// -*- C++ -*-
#ifndef NODE_H
#define NODE_H 

#include<string>
#include<iostream>
#include<cmath>
#include<vector>

#include "Common/defs.h"

using namespace std;

struct Node {
  vector<int> numface;
  vector<int> nummaille;
  Point p;
  int mj;
  Node(){mj = -1;};
  int nbmaille() const;
  int nbface() const;
  const Point & getPoint() const;
  void putPoint(Point &n);
  void putNumeroMaille(int num);
  void putNumeroFace(int num);
  void ConnectiviteCommune(Node * n);
  void egale(const Node & np);
  void operator=(const Node & np){egale(np);};

  typedef vector<int> IdVectorType;

  typedef std::pair<IdVectorType::const_iterator, 
                    IdVectorType::const_iterator> CellVectorConstIterators;

  inline const Point & point() const
  {
    return p;
  }
  inline CellVectorConstIterators cells() const
  {
    return CellVectorConstIterators(nummaille.begin(), nummaille.end());
  }
  inline Unsigned numberOfCells() const
  {
    return nummaille.size();
  }
};
#endif
