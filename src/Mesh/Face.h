// -*- C++ -*-
#ifndef FACE_H
#define FACE_H 1
#include <ostream>
#include <vector>

#include "Common/defs.h"

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

class Face 
{
public:
  enum Properties {
    P_NORMAL          = 1 << 0,
    P_BARYCENTER      = 1 << 1,
    P_CENTEROFGRAVITY = 1 << 2,
    P_MEASURE         = 1 << 3
  };

  typedef std::vector<Point> PointVectorType;
  typedef std::vector<Integer> IdVectorType;
  typedef IdVectorType::const_iterator IdVectorConstIterator;

public:
  //! Constructor
  explicit Face(Integer a_number_of_points = 0, Integer a_back_cell = -1, Integer a_front_cell = -1) 
    : m_number_of_points(a_number_of_points),
      m_back_cell(a_back_cell), 
      m_front_cell(a_front_cell),
      m_barycenter(ZeroRealVector(DIM)),
      m_center_of_gravity(ZeroRealVector(DIM)),
      m_measure(0.),
      m_normal(ZeroRealVector(DIM)),
      m_properties(0)
  {
    m_points.reserve(a_number_of_points);
    m_point_ids.reserve(a_number_of_points);
  }

  //! Return the back cell
  Integer backCell() const;
  //! Return the front cell
  Integer frontCell() const;
  //! Return the back cell
  Integer & backCell();
  //! Return the front cell
  Integer & frontCell();
  //! Return properties
  const Integer & properties() 
  {
    return m_properties;
  }
  //! Check whether the face is internal
  inline bool isInternal() const
  {
    return (m_front_cell != -1);
  }
  //! Check whether the face lies on the boundary
  inline bool isBoundary() const
  {
    return (m_front_cell == -1);
  }

  //! Add physical region
  void addRegion(Integer a_region);
  //! Get physical region
  Integer getRegion() const;
  //! Add one point
  void addPoint(Integer a_iP, const Point & a_P);
  //! Return the \f$i\f$th point
  std::pair<Point, Integer>  point(Integer a_iP) const;
  //! Return the vector of points
  inline const PointVectorType & points() const {
    return m_points;
  }
  //! Return the \f$i\f$th point's id
  inline Integer pointId(Integer a_iP) const
  {
    return m_point_ids[a_iP];
  }
  //! Return the \f$i\f$th point's id (synonim)
  inline Integer nodeId(Integer a_iP) const
  {
    return m_point_ids[a_iP];
  }
  //! Return the vector if point ids
  inline const IdVectorType & pointIds() const
  {
    return m_point_ids;
  }
  //! Return the number of points
  inline Integer numberOfPoints() const
  {
    return m_number_of_points;
  }
  //! Return the number of points (synonim)
  inline Integer numberOfNodes() const
  {
    return m_number_of_points;
  }

  //! Compute selected properties
  void compute(Integer a_properties = P_NORMAL | P_BARYCENTER | P_CENTEROFGRAVITY | P_MEASURE);

  //! Normal
  const Point & normal() const;
  //! Normal pointing out of p
  Point normal(const Point & a_P) const;
  //! Barycenter
  const Point & barycenter() const;
  //! Center of gravity
  const Point & centerOfGravity() const;
  //! Measure
  const Real & measure() const;

  //! Print face properties
  friend std::ostream & operator<<(std::ostream & a_ostr, const Face & a_F);
private:
  Integer m_number_of_points;
  PointVectorType m_points;
  IdVectorType m_point_ids; 
  Integer m_back_cell;
  Integer m_front_cell;

  Point m_barycenter;
  Point m_center_of_gravity;
  Real m_measure;
  Point m_normal;

  Integer m_properties;
  Integer m_physical_region;

  void _compute_barycenter();
  template<Integer Dimension>
  void _compute_center_of_gravity();
  template<Integer Dimension>
  void _compute_measure();
  template<Integer Dimension>
  void _compute_normal();
};

/*----------------------------------------------------------------------------*/

template<Integer Dimension>
Real distance(const typename PointTraits<Dimension>::PointType & a_P, const Face & a_F)
{
  std::pair<typename PointTraits<Dimension>::PointType, Integer> V0 = a_F.point(0);
  return std::fabs(inner_prod(a_P-V0.first, a_F.normal()));
}

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#endif
