#include "polarCoordinates.h"

#include <boost/math/constants/constants.hpp>

Point polarCoordinates(const Point & a_P)
{
  Point a_Q;
  Real r=norm_2(a_P);
  Real theta=acos(a_P(0)/r);
  if(a_P(1)<0.) theta=2*boost::math::constants::pi<Real>()-theta;
  a_Q(0)=r;
  a_Q(1)=theta;
  return a_Q;
}

