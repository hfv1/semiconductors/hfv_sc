// -*- C++ -*-
#ifndef DEFS_H
#define DEFS_H

#include <cassert>
#include <iomanip>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/lu.hpp>

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

typedef int Integer;
typedef double Real;
typedef bool Bool;
typedef unsigned Unsigned;
typedef std::string String;

const Integer DIM = 2;

/*----------------------------------------------------------------------------*/

typedef boost::numeric::ublas::vector<Real> RealVector;
typedef boost::numeric::ublas::zero_vector<Real> ZeroRealVector;

typedef boost::numeric::ublas::bounded_vector<Real, 2> RealVector2;
typedef boost::numeric::ublas::bounded_vector<Real, 3> RealVector3;

typedef boost::numeric::ublas::vector_range<RealVector> RealVectorRange;

typedef boost::numeric::ublas::matrix<Real> RealMatrix;
typedef boost::numeric::ublas::matrix<Real, boost::numeric::ublas::column_major> RealFortranMatrix;
typedef boost::numeric::ublas::zero_matrix<Real> ZeroRealMatrix;
typedef boost::numeric::ublas::identity_matrix<Real> IdentityRealMatrix;
typedef boost::numeric::ublas::scalar_matrix<Real> ScalarRealMatrix;
typedef boost::numeric::ublas::permutation_matrix<std::size_t> PermutationMatrix;

typedef boost::numeric::ublas::bounded_matrix<Real, 2, 2> RealMatrix2x2;
typedef boost::numeric::ublas::bounded_matrix<Real, 3, 3> RealMatrix3x3;

typedef boost::numeric::ublas::matrix_column<RealMatrix> RealMatrixColumn;
typedef boost::numeric::ublas::matrix_column<const RealMatrix> ConstRealMatrixColumn;
typedef boost::numeric::ublas::matrix_column<RealFortranMatrix> RealFortranMatrixColumn;
typedef boost::numeric::ublas::matrix_column<const RealFortranMatrix> ConstRealFortranMatrixColumn;

typedef boost::numeric::ublas::matrix_row<RealMatrix> RealMatrixRow;
typedef boost::numeric::ublas::matrix_row<const RealMatrix> ConstRealMatrixRow;
typedef boost::numeric::ublas::matrix_range<RealMatrix> RealMatrixRange;
typedef boost::numeric::ublas::matrix_range<const RealMatrix> ConstRealMatrixRange;
typedef boost::numeric::ublas::range Range;

typedef std::size_t IndexType;
typedef boost::numeric::ublas::vector<std::size_t> IndexVector;

typedef boost::numeric::ublas::vector<Integer> IntegerVector;
typedef boost::numeric::ublas::zero_vector<Integer> ZeroIntegerVector;
typedef boost::numeric::ublas::matrix<Integer> IntegerMatrix;
typedef boost::numeric::ublas::zero_matrix<Integer> ZeroIntegerMatrix;
typedef boost::numeric::ublas::scalar_matrix<Integer> ScalarIntegerMatrix;

typedef boost::numeric::ublas::unit_vector<Real> RealCanonicalBasisVector;

/*----------------------------------------------------------------------------*/

template<Integer Dimension>
struct PointTraits
{
  typedef boost::numeric::ublas::bounded_vector<Real, Dimension> PointType;
};

typedef boost::numeric::ublas::bounded_vector<Real, DIM> Point;

/*----------------------------------------------------------------------------*/

#ifdef VF_DEBUG
#  define VF_ASSERT(a) assert(a)
#else
#  define VF_ASSERT(a)
#endif

#define FORMAT(W)                                                       \
  std::setiosflags(std::ios_base::left) << std::setw(W) << std::setfill(' ')

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

#endif
