#ifndef _CHRONO_HPP_
#define _CHRONO_HPP_
#include <time.h>

namespace common {
  class chrono {
    clock_t _t1, _t2, _dt;
  public:
    chrono() {
      _dt = 0;
    }
    void start() {
      _t1 = clock();
    }
    void stop() {
      _t2 = clock();
      _dt += _t2 - _t1;
    }
    double diff() {
      return ( 1. * ( _t2 - _t1 ) ) / CLOCKS_PER_SEC;
    }
    double diff_cumul() {
      return ( 1. * _dt / CLOCKS_PER_SEC );
    }
  };
}

#endif

