// -*- C++ -*-
#ifndef HFVSC_H
#define HFVSC_H

#include "VectorBasis.h"
#include "QuadratureRules/FaceIntegrator.h"
#include "QuadratureRules/PyramidIntegrator.h"
#include "Eigen/SparseCore"
#include "Eigen/SparseLU"
#include <Eigen/Eigenvalues>
#include "Eigen/Core"

#include <unsupported/Eigen/SparseExtra>

#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include "Common/chrono.hpp"


#include "postProcessing.h"

#include <vector>

#include <iostream>
#include <Eigen/Dense>

#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>


typedef Eigen::SparseMatrix<Real> SparseMatrixType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
typedef std::array<SolutionVectorType,3> SolutionSCType;

typedef Eigen::SparseLU<SparseMatrixType> SolverType;
typedef Eigen::Triplet<Real> TripletType;

typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> ComputeFluxType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> LocalVectorType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic > LocalMatrixType;

typedef Eigen::Matrix<Real, 2, 2 > TensorType;
typedef std::function<TensorType(const Point &)> DiffusivityType;
typedef std::function<Real(const Point &)> PotentialType;
typedef std::function<Eigen::Matrix<Real, 2, 1>(const Point &)> FieldType;
typedef std::function<Real(const Point &)> LoadType;
typedef std::function<Real(const Point &)> DopingType;
typedef std::function<Real(const Point &)> FctContType;
typedef std::function<Real(const Point &)> PotentialType;
typedef std::function<Real(const Point &)> DopingType;
typedef std::function<Real(const Point &)> InitialDataType;
typedef std::function<Real(const Point &)> BoundaryType;
typedef std::function<Real(const Point &)> BoundaryNeumannType;
typedef std::function<bool(const Face &)> DefBoundaryConditionType;

Real B_conv(const Real s);

Real B_conv(const Real s)
{  if(s==0.){ return 0.;} else{  return s/(std::exp(s) -1.) -1.;} }

//Definition of data (reaction and non-linearity)

    

    //nonlinearity, derivative, antiderivative, inverse and associated bounds
Real nl(const Real s);
Real d_nl(const Real s);
Real p_nl(const Real s);
Real inv_nl(const Real s);
Real d_inv_nl(const Real s);
bool Bounds_newton(const SolutionSCType & U,const SolutionSCType & X);

//Boltzmann stat
     /*
Real nl(const Real s)
    {return log(s);}

Real d_nl(const Real s)
    {return 1./s;}

Real p_nl(const Real s)
    {return s*log(s) - s + 1.;}

Real inv_nl(const Real s) // inverse of the function nl
    {return exp(s);}

Real d_inv_nl(const Real s) // derivative of the inv_nl function
    {return exp(s);}

bool Bounds_newton(const SolutionSCType & U,const SolutionSCType & X)
{
    Real min_N = min( U[0].minCoeff() , X[0].minCoeff());
    Real min_P = min( U[1].minCoeff() , X[1].minCoeff());
    Real min_density =  min(min_P, min_N);

//    Real max_N = max( U[0].maxCoeff() , X[0].maxCoeff());
//    Real max_P = min( U[1].maxCoeff() , X[1].maxCoeff());
//    Real max_density =  max(max_N,max_P);

    return (min_density > 0.);
}
     */

// Blakemore stat
    // /*
Real nl(const Real s)
    {return log(s/(1.-0.27*s) );}

Real d_nl(const Real s)
    {return 1./(s* (1.-0.27*s ));}

Real prim_log(const Real s)
    {return s*log(s) - s + 1.;}

Real p_nl(const Real s)
    {return prim_log(s)  + (1./0.27) * prim_log(1. - 0.27 * s);}

Real inv_nl(const Real s) // inverse of the function nl
    {return 1./(0.27 +  exp(-s));}

Real d_inv_nl(const Real s) // derivative of the inv_nl function
    {return exp(s)/std::pow(1. + 0.27 * exp(s),2.);}

bool Bounds_newton(const SolutionSCType & U,const SolutionSCType & X)
{
    Real min_N = min( U[0].minCoeff() , X[0].minCoeff());
    Real min_P = min( U[1].minCoeff() , X[1].minCoeff());
    Real min_density =  min(min_P, min_N);

    Real max_N = max( U[0].maxCoeff() , X[0].maxCoeff());
    Real max_P = min( U[1].maxCoeff() , X[1].maxCoeff());
    Real max_density =  max(max_N,max_P);

    return (min_density > 0.) && (max_density < 1./0.27);
}
    // */

//reaction and partial derivatives
Real Reaction(const Real N, const Real P);
Real Partial_N_Reac(const Real N, const Real P);
Real Partial_P_Reac(const Real N, const Real P);

// No recombination
     /*
Real Reaction(const Real N, const Real P)
    {return 0.;}

Real Partial_N_Reac(const Real N, const Real P)
    {return 0.;}

Real Partial_P_Reac(const Real N, const Real P)
    {return 0.;}

     */
// Auger
/*
Real Reaction(const Real N, const Real P)
    {return 0.1 * (N +P)*(exp(nl(N)+nl(P))-1.) ;}

Real Partial_N_Reac(const Real N, const Real P)
    {return 0.1*( (exp(nl(N)+nl(P))-1.) + (N+P) * d_nl(N)* exp(nl(N)+nl(P))  ) ;}

Real Partial_P_Reac(const Real N, const Real P)
    {return 0.1*( (exp(nl(N)+nl(P))-1.) + (N+P) * d_nl(P)* exp(nl(N)+nl(P))  ) ;}
*/
 //SRH
    // /*
Real Reaction(const Real N, const Real P)
    {return 10. * (exp(nl(N)+nl(P))-1.) / (N +P +1. );}

Real Partial_N_Reac(const Real N, const Real P)
    {return 10.*( -(exp(nl(N)+nl(P))-1.)*std::pow(N+P+1,-2)  +  d_nl(N)* exp(nl(N)+nl(P)) /(N +P +1. ) )   ;}

Real Partial_P_Reac(const Real N, const Real P)
    {return 10.*( -(exp(nl(N)+nl(P))-1.)*std::pow(N+P+1,-2)  +  d_nl(P)* exp(nl(N)+nl(P)) /(N +P +1. ) )   ;}
    // */


//Declaration, general (precomputation)

void comp_contrib_diff(const Mesh * Th, const int iT,const DiffusivityType & nu, std::vector<TensorType> & Lamb);
void comp_contrib_diff_harmo(const Mesh * Th, const int iT,const DiffusivityType & nu, const FctContType & omega, const int type_moy,std::vector<TensorType> & Lamb);
void comp_y_vectors(const Mesh * Th, const int iT,const  Real  eta, std::vector<vector<Point>> & Y );
void comp_V_on_edge(const Mesh * Th, const int iT,const  FieldType V, std::vector<Real> & V_edge );
void comp_DiffInt(const Mesh * Th, const  DiffusivityType Lambda,  std::vector<Real> & Diff_Int );
void comp_A_elt(const Mesh *Th, const DiffusivityType & Lambda, const Real eta, vector<vector<vector<Real>>> & A);
void comp_A_elt_harmo(const Mesh *Th, const DiffusivityType & Lambda, const FctContType & omega, const int type_moy,  const Real eta, vector<vector<vector<Real>>> & A);
void comp_B_elt(const Mesh *Th, const vector<vector<vector<Real>>> & _A, std::vector<vector<Real>> & _B);
void comp_Brev_elt(const Mesh *Th, const vector<vector<vector<Real>>> & _A, std::vector<vector<Real>> & _C);
void comp_Alpha_elt(const Mesh *Th, const std::vector<vector<Real>> & _B, std::vector<Real> & Alpha);
void comp_Conv_elt(const Mesh *Th, const  std::vector<Real> & Diff_Int, const FieldType V, std::vector<vector<Real>> & Conv_N, std::vector<vector<Real>> & Conv_P);

void comp_Mat_loc_bilin(const Mesh *Th, const std::vector<vector<LocalVectorType>> & Flux, std::vector<LocalVectorType> & Sum_Flux, std::vector<LocalMatrixType> & Mat_loc);


LocalVectorType loc_vec(const Mesh *Th, const int iT,const SolutionVectorType & U_vol, const SolutionVectorType & X_edge);
LocalVectorType loc_nlvec(const Mesh *Th, const int iT,const SolutionVectorType & U_vol, const SolutionVectorType & X_edge);

void precomp_flux(const Mesh *Th, const vector<vector<vector<Real>>> & _A, const std::vector<vector<Real>> & _B, std::vector<vector<LocalVectorType>> & Flux);
void precomp_Somme_flux(const Mesh *Th, const std::vector<vector<LocalVectorType>> & Flux, std::vector<LocalVectorType> & Somme_Flux);
void precomp_reconstruc(const Mesh *Th,  std::vector<LocalVectorType> & Recons);

void comp_flux_potential(const Mesh *Th, const std::vector<vector<LocalVectorType>> & Flux, const SolutionVectorType & Phi_vol, const SolutionVectorType & Phi_edge,   std::vector<vector<Real>> & Flux_Pot);
void comp_Sumflux_potential(const Mesh *Th, const std::vector<LocalVectorType> & Sum_Flux, const SolutionVectorType & Phi_vol, const SolutionVectorType & Phi_edge,   std::vector<Real> & SFlux_Pot);

// Proj

SolutionVectorType proj_UKN_vol(const Mesh *Th, const Real seuil_pos, const SolutionVectorType & U_vol);
SolutionVectorType proj_UKN_edge(const Mesh *Th, const Real seuil_pos, const SolutionVectorType & X_edge);

SolutionSCType proj_SC_vol(const Mesh *Th, const Real seuil_pos, const SolutionSCType & U);
SolutionSCType proj_SC_edge(const Mesh *Th, const Real seuil_pos, const SolutionSCType & X);

//Declaration, matrices (evolutionary problem)

void comp_sec_memb_Neumann(const Mesh *Th  , const LoadType & f,SolutionVectorType & Sec_m_vol );

void triplets_invJ1andj2(const Mesh *Th, const Real & pas_temps , const std::vector<Real> & Alpha, const std::vector<vector<Real>> & B,
    const std::vector<LocalVectorType> & Recons,const std::vector<LocalVectorType> & Somme_Flux, const std::vector<Real> & Somme_Flux_Phi,
    const SolutionVectorType & U_vol, const SolutionVectorType & X_edge, std::vector<TripletType> & Triplets_invJ1, std::vector<TripletType> & Triplets_J2);

void triplets_J3andJ4(const Mesh *Th, const DefBoundaryConditionType & isDir, const std::vector<vector<Real>> & B,  const std::vector<vector<vector<Real>>> & A, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<vector<Real>> & Flux_Phi,
    const SolutionVectorType & U_vol, const SolutionVectorType & X_edge, std::vector<TripletType> & Triplets_J3, std::vector<TripletType> & Triplets_J4);

void comp_G_vect(const Mesh *Th, const Real & pas_temps ,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu , const SolutionVectorType G_Neu,
    const SolutionVectorType & U_n , const SolutionVectorType & X_n , const std::vector<LocalVectorType> & Recons ,
    const std::vector<LocalVectorType> & Somme_Flux, const std::vector<Real> & Somme_Flux_Phi,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<vector<Real>> & Flux_Phi,
    const SolutionVectorType & V_vol, const SolutionVectorType & X_v_edge, SolutionVectorType & G_vol , SolutionVectorType & G_edge );
//Declaration, Resolution iteration Newton


Real comp_sol_iter_newton(const Mesh *Th, const Real & pas_temps,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const SolutionVectorType & U_n , const SolutionVectorType & X_n ,
    const std::vector<vector<vector<Real>>> & A, const std::vector<vector<Real>> & B, const std::vector<Real> & Alpha, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<vector<Real>> & Flux_Phi,
    const std::vector<LocalVectorType> & Somme_Flux, const std::vector<Real> & Somme_Flux_Phi,
    const SolutionVectorType & U_ini , const SolutionVectorType & X_ini,
    SolutionVectorType & R_sol_vol , SolutionVectorType & R_sol_edge );


// Declaration, computation potential
void comp_sec_memb_pot(const Mesh *Th, const SolutionVectorType & X_b ,const DopingType & C, const DefBoundaryConditionType & isNeu , const BoundaryNeumannType g,
    const std::vector<vector<vector<Real>>> & A, const std::vector<vector<Real>> & B,
    SolutionVectorType & Sec_m_vol , SolutionVectorType & Sec_m_edge );

void triplets_invM1andM2(const Mesh *Th, const std::vector<Real> & Alpha, const std::vector<vector<Real>> & B,const std::vector<vector<vector<Real>>> & A,
    std::vector<TripletType> & Triplets_invM1, std::vector<TripletType> & Triplets_M2);

void triplets_M3andM4(const Mesh *Th, const std::vector<vector<Real>> & B,  const std::vector<vector<vector<Real>>> & A,
    std::vector<TripletType> & Triplets_M3, std::vector<TripletType> & Triplets_M4);

void comp_Potentialsolver(const Mesh* Th, const Real eta, const DiffusivityType & Lambda_Phi, const DopingType & C,
    const DefBoundaryConditionType & isDir, const BoundaryType & Phi_b , const DefBoundaryConditionType & isNeu , const BoundaryNeumannType & g_neu,
    SolutionVectorType & Potential_relev_boundary ,
    SparseMatrixType & Resize_Dir, SparseMatrixType & invM1, SparseMatrixType & M2 , SparseMatrixType & M3,
    SolutionVectorType & Sec_m_vol, SolutionVectorType & Sec_m_edge , SolverType & solver_potential);

void comp_Potential(const Mesh *Th, const SolverType & solver_pot, const SolutionVectorType & N_vol , const SolutionVectorType & P_vol ,
     const SparseMatrixType & M_mass ,const SparseMatrixType & Resize_Dir, const SparseMatrixType & invM1, const SparseMatrixType & M2 , const SparseMatrixType & M3,
    const SolutionVectorType & Sec_m_vol  , const SolutionVectorType & Sec_m_edge , const SolutionVectorType & Potential_relev_boundary,
    SolutionVectorType & Phi_sol_vol , SolutionVectorType & Phi_sol_edge );

//Declaration, nonlinear scheme for thermal equilibrium


void comp_G_eq(const Mesh *Th, const Real & alpha_P, const Real & alpha_N , const DopingType & C,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu , const SolutionVectorType G_Neu,
    const std::vector<LocalVectorType> & Somme_Flux, const std::vector<vector<LocalVectorType>> & Flux,
    const SolutionVectorType & V_vol, const SolutionVectorType & V_edge, SolutionVectorType & G_vol , SolutionVectorType & G_edge );

void triplets_invJ1andj2_eq(const Mesh *Th,  const Real & alpha_P, const Real & alpha_N ,
    const std::vector<Real> & Alpha, const std::vector<vector<Real>> & B,
    const SolutionVectorType & U_vol,  std::vector<TripletType> & Triplets_invJ1, std::vector<TripletType> & Triplets_J2);

void triplets_J3andJ4_eq(const Mesh *Th, const Real & alpha_P, const Real & alpha_N ,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu , const SolutionVectorType G_Neu,
    const std::vector<vector<Real>> & B,  const std::vector<vector<vector<Real>>> & A,
    std::vector<TripletType> & Triplets_J3, std::vector<TripletType> & Triplets_J4);

Real comp_sol_iter_newton_eq(const Mesh *Th, const Real & alpha_P, const Real & alpha_N ,const DopingType & C,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const std::vector<vector<vector<Real>>> & A, const std::vector<vector<Real>> & B, const std::vector<Real> & Alpha,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<LocalVectorType> & Somme_Flux,
    const SolutionVectorType & U_ini , const SolutionVectorType & X_ini, SolutionVectorType & R_sol_vol , SolutionVectorType & R_sol_edge );


//test jacobian
void test_Jaco_eq(const Mesh *Th, const Real & alpha_P, const Real & alpha_N ,const DopingType & C,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const std::vector<vector<vector<Real>>> & A, const std::vector<vector<Real>> & B, const std::vector<Real> & Alpha,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<LocalVectorType> & Somme_Flux);


// nonlinear coupled schem for Semi-conductors

void comp_G_sc(const Mesh *Th, const Real & pas_temps , const SolutionVectorType C,
    const DefBoundaryConditionType & isDir , const SolutionSCType & X_b , const DefBoundaryConditionType & isNeu , const SolutionVectorType G_Neu,
    const SolutionSCType & U_n, const std::vector<LocalVectorType> & Recons ,
    const std::vector<vector<vector<LocalVectorType>>> & Flux , const std::vector<vector<LocalVectorType>> & Somme_Flux,
    const std::vector<vector<vector<LocalVectorType>>> & Flux_Potential , const std::vector<vector<LocalVectorType>> & Sum_Flux_Potential,
    const SolutionSCType & U , const SolutionSCType & X, SolutionVectorType & G_vol , SolutionVectorType & G_edge );

void triplets_invJ1andj2_sc(const Mesh *Th, const Real & pas_temps , const std::vector<vector<Real>> & Alpha, const std::vector<vector<vector<Real>>> & B, const std::vector<vector<vector<Real>>> & Brev,
    const std::vector<LocalVectorType> & Recons,const std::vector<vector<LocalVectorType>> & Somme_Flux, const std::vector<vector<Real>> & Sum_Flux_Potential,
    const SolutionSCType & U, const SolutionSCType & X, std::vector<TripletType> & Triplets_invJ1, std::vector<TripletType> & Triplets_J2);

void triplets_J3andJ4_sc(const Mesh *Th,const DefBoundaryConditionType & isDir,
    const std::vector<vector<vector<Real>>> & B,const std::vector<vector<vector<Real>>> & Brev,  const std::vector<vector<vector<vector<Real>>>> & A, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<vector<LocalVectorType>>> & Flux , const std::vector<vector<vector<Real>>> & Flux_Potential,
    const SolutionSCType & U, const SolutionSCType & X, std::vector<TripletType> & Triplets_J3, std::vector<TripletType> & Triplets_J4);

Real comp_sol_iter_newton_sc(const Mesh *Th, const Real & pas_temps,const SolutionVectorType C,
    const DefBoundaryConditionType & isDir , const SolutionSCType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const SolutionSCType & U_n , const SolutionSCType & X_n ,
    const std::vector<vector<vector<vector<Real>>>> & A, const std::vector<vector<vector<Real>>> & B,const std::vector<vector<vector<Real>>> & Brev, const std::vector<vector<Real>> & Alpha, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<vector<LocalVectorType>>> & Flux , const std::vector<vector<LocalVectorType>> & Somme_Flux,
    const SolutionSCType & U , const SolutionSCType & X,
    SolutionVectorType & R_sol_vol , SolutionVectorType & R_sol_edge );


SolutionSCType SolVec_to_SC_vol(const Mesh *Th, const SolutionVectorType & R_vol);
SolutionSCType SolVec_to_SC_edge(const Mesh *Th, const SolutionVectorType & R_edge);

void test_Jaco_sc(const Mesh *Th, const Real & pas_temps,const SolutionVectorType C,
    const DefBoundaryConditionType & isDir , const SolutionSCType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const SolutionSCType & U_n , const SolutionSCType & X_n ,
    const std::vector<vector<vector<vector<Real>>>> & A, const std::vector<vector<vector<Real>>> & B, const std::vector<vector<vector<Real>>> & Brev,const std::vector<vector<Real>> & Alpha, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<vector<LocalVectorType>>> & Flux , const std::vector<vector<LocalVectorType>> & Somme_Flux);

 //entropies and energies

 Real comp_elec_en(const Mesh *Th, const std::vector<vector<LocalVectorType>> & Flux, const std::vector<LocalVectorType> & Sum_Flux, const SolutionVectorType & Phi_vol ,const SolutionVectorType & Phi_edge);

// ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~

//Implementation


void comp_contrib_diff(const Mesh * Th, const int iT,const  DiffusivityType & nu, std::vector<TensorType> & Lamb)
{
  const  Cell & T = Th->cell(iT);
  const Integer & m = T.numberOfFaces();
  const Point & x_T = T.center();

  Lamb.resize(m);
  for(int iF_loc=0;iF_loc<m;iF_loc++ ){
    const int & iF=T.faceId(iF_loc);
    const Face & F = Th->face(iF);
    const Real & m_F = F.measure();
    const Point & x_F = F.barycenter();
    const Point & n_F= F.normal(x_T);
    const Real & d_F = inner_prod(n_F, x_F-x_T);

    // Lamb[iF_loc]=0.5* d_F* m_F* nu(x_T);
    Lamb[iF_loc]=0.5* d_F* m_F* nu(x_T);


  }// for iF_loc
}

void comp_contrib_diff_harmo(const Mesh * Th, const int iT,const  DiffusivityType & nu,const FctContType & omega, const int type_moy, std::vector<TensorType> & Lamb)
{
  const  Cell & T = Th->cell(iT);
  const Integer & m = T.numberOfFaces();
  const Point & x_T = T.center();

  Lamb.resize(m);
  TensorType L_T =  nu(x_T);



 if(type_moy == 0){
    for(int iF_loc=0;iF_loc<m;iF_loc++ ){
        const int & iF=T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        const Real & m_F = F.measure();
        const Point & x_F = F.barycenter();
        const Point & n_F= F.normal(x_T);
        const Real & d_F = inner_prod(n_F, x_F-x_T);
        const Point x_1 = (x_T + F.point(0).first )*(1./2.);
        const Point x_2 = (x_T + F.point(1).first)*(1./2.);
        const Point x_3 = (F.point(0).first + F.point(1).first)*(1./2.);
        Real M = omega(x_1) + omega(x_2) + omega(x_3);
        M = M/3.;

         //Lamb[iF_loc]=0.5* d_F* m_F*omega(x_T) *nu(x_T);
        Lamb[iF_loc]= 0.5* d_F* m_F* M  *L_T ;
        }// for iF_loc
    }// if type_moy == 2


 if(type_moy == 1){
    for(int iF_loc=0;iF_loc<m;iF_loc++ ){
        const int & iF=T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        const Real & m_F = F.measure();
        const Point & x_F = F.barycenter();
        const Point & n_F= F.normal(x_T);
        const Real & d_F = inner_prod(n_F, x_F-x_T);
        const Point x_1 = (x_T + F.point(0).first )*(1./2.);
        const Point x_2 = (x_T + F.point(1).first)*(1./2.);
        const Point x_3 = (F.point(0).first + F.point(1).first)*(1./2.);
        Real M = 1./omega(x_1) + 1./omega(x_2) + 1./omega(x_3);
        M = M/3.;

         //Lamb[iF_loc]=0.5* d_F* m_F*omega(x_T) *nu(x_T);
        Lamb[iF_loc]= 0.5* d_F* m_F* (1. / (M ))  *L_T ;
        }// for iF_loc
    }// if type_moy == 2

    if(type_moy == 2){
    for(int iF_loc=0;iF_loc<m;iF_loc++ ){
        const int & iF=T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        const Real & m_F = F.measure();
        const Point & x_F = F.barycenter();
        const Point & n_F= F.normal(x_T);
        const Real & d_F = inner_prod(n_F, x_F-x_T);
        const Point x_1 = (x_T + F.point(0).first )*(1./2.);
        const Point x_2 = (x_T + F.point(1).first)*(1./2.);
        const Point x_3 = (F.point(0).first + F.point(1).first)*(1./2.);
        Real M = std::pow(omega(x_1),-2.) +std::pow(omega(x_2),-2.) +std::pow(omega(x_3),-2.);
        M = M/3.;

         //Lamb[iF_loc]=0.5* d_F* m_F*omega(x_T) *nu(x_T);
        Lamb[iF_loc]= 0.5* d_F* m_F* std::pow(M,-1./2.)  *L_T ;
        }// for iF_loc
    }// if type_moy == 2
    if(type_moy == 3){
    for(int iF_loc=0;iF_loc<m;iF_loc++ ){
        const int & iF=T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        const Real & m_F = F.measure();
        const Point & x_F = F.barycenter();
        const Point & n_F= F.normal(x_T);
        const Real & d_F = inner_prod(n_F, x_F-x_T);
        const Point x_1 = (x_T + F.point(0).first )*(1./2.);
        const Point x_2 = (x_T + F.point(1).first)*(1./2.);
        const Point x_3 = (F.point(0).first + F.point(1).first)*(1./2.);
        Real M = std::pow(omega(x_1),-10.) +std::pow(omega(x_2),-10.) +std::pow(omega(x_3),-10.);
        M = M/3.;

         //Lamb[iF_loc]=0.5* d_F* m_F*omega(x_T) *nu(x_T);
        Lamb[iF_loc]= 0.5* d_F* m_F* std::pow(M,-1./10.)  *L_T ;
        }// for iF_loc
    }// if type_moy == 2
    if(type_moy == 4){
    for(int iF_loc=0;iF_loc<m;iF_loc++ ){
        const int & iF=T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        const Real & m_F = F.measure();
        const Point & x_F = F.barycenter();
        const Point & n_F= F.normal(x_T);
        const Real & d_F = inner_prod(n_F, x_F-x_T);
        const Point x_1 = (x_T + F.point(0).first )*(1./2.);
        const Point x_2 = (x_T + F.point(1).first)*(1./2.);
        const Point x_3 = (F.point(0).first + F.point(1).first)*(1./2.);
        Real M = std::pow(omega(x_1),-15.) +std::pow(omega(x_2),-15.) +std::pow(omega(x_3),-15.);
        M = M/3.;

         //Lamb[iF_loc]=0.5* d_F* m_F*omega(x_T) *nu(x_T);
        Lamb[iF_loc]= 0.5* d_F* m_F* std::pow(M,-1./15.)  *L_T ;
        }// for iF_loc
    }// if type_moy == 2

}
//Création matrice Y

void comp_y_vectors(const Mesh * Th, const int iT,const Real eta,std::vector<vector<Point>> & Y)
{
  const  Cell & T = Th->cell(iT);
  const Integer& m = T.numberOfFaces();
  const Real &m_T= T.measure();
  const Point & x_T = T.center();

  Y.resize(m);

  for(int iF_loc1=0;iF_loc1<m;iF_loc1++ ){
    Y[iF_loc1].resize(m);
    const int iF1=T.faceId(iF_loc1);
    const Face & F1 = Th->face(iF1);
    const Real & m_F1 = F1.measure();
    const Point & x_F1 = F1.barycenter();
    const Point & n_F1= F1.normal(x_T);
    const Real & d_F1 = inner_prod(n_F1, x_F1-x_T);
    Real sc =  (m_F1/m_T) +( eta /d_F1)*(1 - (m_F1/m_T)*d_F1);
    Y[iF_loc1][iF_loc1] = sc * n_F1;

    for(int iF_loc2=0;iF_loc2<m;iF_loc2++ ){
      const int iF2=T.faceId(iF_loc2);
      const Face & F2 = Th->face(iF2);
      const Real & m_F2 =F2.measure()  ;
      //const Point & x_F2 = F2.barycenter();
      const Point & n_F2 = F2.normal(x_T);

      if(iF_loc1!= iF_loc2){

	Real r_1 =(m_F2/m_T);
	Real r_2 = eta * (m_F2  /(d_F1 * m_T ))*  inner_prod(n_F2, x_F1-x_T);
        Y[iF_loc1][iF_loc2]= r_1 *n_F2 - r_2* n_F1;


      }//if iF1!==iF2, terme diagonal déjà géré avant la seconde boucle sur les faces
   }//for iF_loc2

  } // for iF_loc1
}

void comp_V_on_edge(const Mesh * Th, const int iT,const  FieldType V, std::vector<Real> & V_T )
{
    const  Cell & T = Th->cell(iT);
    const Integer& m = T.numberOfFaces();
    const Point & x_T = T.center();

    V_T.resize(m);
    for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
        const int iF=T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        const Real & m_F = F.measure();
        const Point & x_F = F.barycenter();
        const Point & n_F= F.normal(x_T);
        //Ici, conversion matrice eigen V en point pour le prod scalaire.
        Point v_s;
        v_s(0) =V(x_F)(0,0) ;
        v_s(1) =V(x_F)(1,0) ;
        Real v = boost::numeric::ublas::inner_prod(v_s,n_F)*(1/m_F);
        V_T[iF_loc] = v;

    }//for iF_loc


}

void comp_DiffInt(const Mesh * Th, const  DiffusivityType Lambda,  std::vector<Real> & Diff_Int )
{
    int N_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    Diff_Int.resize(N_edge);
    for(int iF = 0; iF< N_edge; iF ++){
        Diff_Int[iF] = 1.;
    } //for iF

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        const Point & x_T = T.center();
        const Real & m_T = T.measure();
        TensorType lambda_T = Lambda(x_T);
        Eigen::SelfAdjointEigenSolver<TensorType> es(lambda_T);
        Real vp_L_T = (es.eigenvalues()).minCoeff();
        for(int iF_loc = 0; iF_loc < m; iF_loc ++){
            const int iF=T.faceId(iF_loc);
            Real mu_F = Diff_Int[iF];

            Diff_Int[iF] = min( mu_F , vp_L_T);
        }//for iF_loc
    }//for iT
}


//void comp_A_elt(const Mesh *Th, const DiffusivityType & Lambda, const Real eta, std::vector<vector<vector<Real>>> & A)
//{
//    //std::vector<vector<vector<Real>>> A;
//    A.resize(Th->numberOfCells());
//
//    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
//        //for(Integer iT = 0; iT<  2;iT++ ){
//        const Cell & T = Th->cell(iT);
//        const Integer & m = T.numberOfFaces();
//        A[iT].resize(m);
//
//        //Calcul (local) des y_K s\s' et des Lambda_K,s
//        std::vector<vector<Point>> Y_T;
//        std::vector<TensorType> L_T;
//        L_T.resize(m);
//        Y_T.resize(m);
//        comp_y_vectors( Th,iT,eta,Y_T); // calcul des y
//        comp_contrib_diff(Th, iT, Lambda,L_T);
//
//        for(int i=0; i< m;i++){
//            A[iT][i].resize(m);
//            }
//
//        for(int i=0; i< m;i++){
//          for(int j=0;j< m; j++){
//            Real coef_a = 0.;
//
//            for(int k =0;k<m;k++){
//                Eigen::Vector2d Y_T_kj(Y_T[k][j](0),Y_T[k][j](1));
//                Eigen::Vector2d L_Y_vec = L_T[k] * Y_T_kj;
//                Point L_Y;
//                L_Y(0) = L_Y_vec(0);
//                L_Y(1) = L_Y_vec(1);
//                //coef_a +=boost::numeric::ublas::inner_prod(Y_T[k][i],L_T[k]* Y_T[k][j]); //ICI, rajouter le terme issu du tenseur de diffusion avec produit matriciel si besoin !!!!!!!!!???????
//                coef_a +=boost::numeric::ublas::inner_prod(Y_T[k][i],L_Y);
//                }//for k
//            A[iT][i][j] = coef_a;
//
//            }//for j
//        } //for i
//    } //for iT
//
//}

void comp_A_elt(const Mesh *Th, const DiffusivityType & Lambda, const Real eta, std::vector<vector<vector<Real>>> & A)
{
    //std::vector<vector<vector<Real>>> A;
    A.resize(Th->numberOfCells());

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        //for(Integer iT = 0; iT<  2;iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        A[iT].resize(m);

        //Calcul (local) des y_K s\s' et des Lambda_K,s
        std::vector<vector<Point>> Y_T;
        std::vector<TensorType> L_T;
        L_T.resize(m);
        Y_T.resize(m);
        comp_y_vectors( Th,iT,eta,Y_T); // calcul des y
        comp_contrib_diff(Th, iT, Lambda,L_T);

        for(int i=0; i< m;i++){
            A[iT][i].resize(m);
            }

        for(int i=0; i< m;i++){
          for(int j=0;j< m; j++){
            Real coef_a = 0.;

            for(int k =0;k<m;k++){
                Eigen::Vector2d Y_T_kj(Y_T[k][j](0),Y_T[k][j](1));
                Eigen::Vector2d L_Y_vec = L_T[k] * Y_T_kj;
                Point L_Y;
                L_Y(0) = L_Y_vec(0);
                L_Y(1) = L_Y_vec(1);
                //coef_a +=boost::numeric::ublas::inner_prod(Y_T[k][i],L_T[k]* Y_T[k][j]); //ICI, rajouter le terme issu du tenseur de diffusion avec produit matriciel si besoin !!!!!!!!!???????
                coef_a +=boost::numeric::ublas::inner_prod(Y_T[k][i],L_Y);
                }//for k
            A[iT][i][j] = coef_a;

            }//for j
        } //for i
    } //for iT

}

void comp_A_elt_harmo(const Mesh *Th, const DiffusivityType & Lambda,const FctContType & omega, const int type_moy, const Real eta, std::vector<vector<vector<Real>>> & A)
{
    //std::vector<vector<vector<Real>>> A;
    A.resize(Th->numberOfCells());

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        //for(Integer iT = 0; iT<  2;iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        A[iT].resize(m);

        //Calcul (local) des y_K s\s' et des Lambda_K,s
        std::vector<vector<Point>> Y_T;
        std::vector<TensorType> L_T;
        L_T.resize(m);
        Y_T.resize(m);
        comp_y_vectors( Th,iT,eta,Y_T); // calcul des y
        comp_contrib_diff_harmo(Th, iT, Lambda, omega , type_moy,L_T);

        for(int i=0; i< m;i++){
            A[iT][i].resize(m);
            }

        for(int i=0; i< m;i++){
          for(int j=i;j< m; j++){
            Real coef_a = 0.;

            for(int k =0;k<m;k++){
                Eigen::Vector2d Y_T_kj(Y_T[k][j](0),Y_T[k][j](1));
                Eigen::Vector2d L_Y_vec = L_T[k] * Y_T_kj;
                Point L_Y;
                L_Y(0) = L_Y_vec(0);
                L_Y(1) = L_Y_vec(1);
                //coef_a +=boost::numeric::ublas::inner_prod(Y_T[k][i],L_T[k]* Y_T[k][j]); //ICI, rajouter le terme issu du tenseur de diffusion avec produit matriciel si besoin !!!!!!!!!???????
                coef_a +=boost::numeric::ublas::inner_prod(Y_T[k][i],L_Y);
                }//for k
            A[iT][i][j] = coef_a;
            A[iT][j][i] = coef_a;

            }//for j
        } //for i
    } //for iT

}

void comp_B_elt(const Mesh *Th, const vector<vector<vector<Real>>> & _A, std::vector<vector<Real>> & _B)
{

    _B.resize(Th->numberOfCells());

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        //for(Integer iT = 0; iT<  2;iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        _B[iT].resize(m);
        for(int iF_loc= 0; iF_loc<m;iF_loc++){
            Real b = 0.;
            for(int j =0;j<m;j++){
                b += _A[iT][iF_loc][j];
                }//for j, calcul somme A_K s s'
            _B[iT][iF_loc]= b;
        }//for iF_loc
    }//for iT

}

void comp_Brev_elt(const Mesh *Th, const vector<vector<vector<Real>>> & _A, std::vector<vector<Real>> & _C)
{

    _C.resize(Th->numberOfCells());

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        //for(Integer iT = 0; iT<  2;iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        _C[iT].resize(m);
        for(int iF_loc= 0; iF_loc<m;iF_loc++){
            Real c = 0.;
            for(int j =0;j<m;j++){
                c += _A[iT][j][iF_loc];
                }//for j, calcul somme A_K s s'
            _C[iT][iF_loc]= c;
        }//for iF_loc
    }//for iT

}

void comp_Alpha_elt(const Mesh *Th, const std::vector<vector<Real>> & _B, std::vector<Real> & Alpha)
{
    Alpha.resize(Th->numberOfCells());
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        //for(Integer iT = 0; iT<  2;iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        Real alpha = 0.;
        for(int iF_loc= 0; iF_loc<m;iF_loc++){
            alpha += _B[iT][iF_loc];
        }//for iF_loc
        Alpha[iT] = alpha;
    }//for iT

}

void comp_Conv_elt(const Mesh *Th, const  std::vector<Real> & Diff_Int, const FieldType V, std::vector<vector<Real>> & Conv_N, std::vector<vector<Real>> & Conv_P)
{
    Conv_N.resize(Th->numberOfCells());
    Conv_P.resize(Th->numberOfCells());

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        //for(Integer iT = 0; iT<  2;iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        const Point & x_T = T.center();
        Conv_N[iT].resize(m);
        Conv_P[iT].resize(m);

        std::vector<Real> V_T;
        V_T.resize(m);
        comp_V_on_edge(Th, iT, V, V_T);

        for(int iF_loc=0;iF_loc < m; iF_loc ++){
            const int iF=T.faceId(iF_loc);
            const Face & F = Th->face(iF);
            const Real & m_F = F.measure();
            const Point & x_F = F.barycenter();
            const Point & n_F= F.normal(x_T);
            const Real d_F = inner_prod(n_F, x_F-x_T);
            Real mu_F = Diff_Int[iF];
            Real b_arg = d_F * m_F * V_T[iF_loc];
            Real b_p = m_F * mu_F *  B_conv(b_arg/mu_F);
            Real b_n = m_F * mu_F * B_conv(-b_arg/mu_F);

            Conv_N[iT][iF_loc] = b_n/d_F;
            Conv_P[iT][iF_loc] = b_p/d_F;
            }//for iF_loc
    }//for iT
}

void comp_Mat_loc_bilin(const Mesh *Th, const std::vector<vector<LocalVectorType>> & Flux, std::vector<LocalVectorType> & Sum_Flux, std::vector<LocalMatrixType> & Mat_loc)
{
    Mat_loc.resize(Th->numberOfCells());
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        Mat_loc[iT].resize(1+m,1+m);
        Mat_loc[iT].col(0) = Sum_Flux[iT];
        for(int iF_loc = 0; iF_loc < m; iF_loc ++){
            Mat_loc[iT].col(1+iF_loc) = - Flux[iT][iF_loc];
        }//for iF_loc
    }//for iT
}


LocalVectorType loc_vec(const Mesh *Th, const int iT,const SolutionVectorType & U_vol, const SolutionVectorType & X_edge)
{
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        LocalVectorType Vec_loc = LocalVectorType::Zero(m+1);
        Vec_loc(0) = U_vol(iT);
        for(int iF_loc = 0; iF_loc < m; iF_loc ++){
            const int & iF = T.faceId(iF_loc);
            Vec_loc(1+iF_loc) = X_edge(iF);
            } // for iF_loc

        return Vec_loc;
}

LocalVectorType loc_nlvec(const Mesh *Th, const int iT,const SolutionVectorType & U_vol, const SolutionVectorType & X_edge)
{
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        LocalVectorType Vec_loc = LocalVectorType::Zero(m+1);
        Vec_loc(0) = nl(U_vol(iT));
        for(int iF_loc = 0; iF_loc < m; iF_loc ++){
            const int & iF = T.faceId(iF_loc);
            Vec_loc(1+iF_loc) = nl(X_edge(iF));
            } // for iF_loc

        return Vec_loc;
}

void precomp_flux(const Mesh *Th, const vector<vector<vector<Real>>> & _A, const std::vector<vector<Real>> & _B, std::vector<vector<LocalVectorType>> & Flux)
{
    Flux.resize(Th->numberOfCells());

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        Flux[iT].resize(m);

        for(int iF_loc=0; iF_loc < m ; iF_loc ++){
            Flux[iT][iF_loc].resize(1+m,1);
            Flux[iT][iF_loc](0) = _B[iT][iF_loc];
            for(int iF_loc2=0; iF_loc2 < m ; iF_loc2 ++){
                Flux[iT][iF_loc](1+iF_loc2)= - _A[iT][iF_loc][iF_loc2];

            }// for iF_loc2
        }// for iF_loc
    } //for iT

}

void precomp_Somme_flux(const Mesh *Th, const std::vector<vector<LocalVectorType>> & Flux, std::vector<LocalVectorType> & Somme_Flux)
{
    Somme_Flux.resize(Th->numberOfCells());

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        Somme_Flux[iT].resize(1+m,1);
        LocalVectorType SF = LocalVectorType::Zero(1+m,1);
        for(int iF_loc=0; iF_loc < m ; iF_loc ++){
            SF = SF + Flux[iT][iF_loc];
        }// for iF_loc
        Somme_Flux[iT] = SF;
    } //for iT
}

void precomp_reconstruc(const Mesh *Th,  std::vector<LocalVectorType> & Recons)
{
    Recons.resize(Th->numberOfCells());

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        Recons[iT].resize(1+m,1);
        Recons[iT](0) = 0.5;
        Real r = 0.5/m;
        for(int iF_loc=0; iF_loc < m ; iF_loc ++){
                Recons[iT](1+iF_loc) = r;
        }// for iF_loc
    } //for iT
}

void comp_flux_potential(const Mesh *Th, const std::vector<vector<LocalVectorType>> & Flux, const SolutionVectorType & Phi_vol, const SolutionVectorType & Phi_edge ,  std::vector<vector<Real>> & Flux_Pot)
{
    Flux_Pot.resize(Th->numberOfCells());
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        Flux_Pot[iT].resize(m);

        for(int iF_loc=0; iF_loc < m ; iF_loc ++){
            Flux_Pot[iT][iF_loc] = loc_vec(Th,iT,Phi_vol, Phi_edge).dot(Flux[iT][iF_loc]);
        }// for iF_loc
    } //for iT
}

void comp_Sumflux_potential(const Mesh *Th, const std::vector<LocalVectorType> & Sum_Flux, const SolutionVectorType & Phi_vol, const SolutionVectorType & Phi_edge,   std::vector<Real> & SFlux_Pot)
{
    SFlux_Pot.resize(Th->numberOfCells());
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        SFlux_Pot[iT] = loc_vec(Th,iT,Phi_vol, Phi_edge).dot(Sum_Flux[iT]);
    } //for iT
}

// projections

SolutionVectorType proj_UKN_vol(const Mesh *Th, const Real seuil_pos, const SolutionVectorType & U_vol)
{
    const int  n_vol = Th->numberOfCells();
    SolutionVectorType U_proj = SolutionVectorType::Zero(n_vol);
    for(int iT = 0; iT < n_vol; iT ++){
        U_proj(iT) = max(U_vol(iT), seuil_pos);
    } //fot iT
    return U_proj;
}

SolutionVectorType proj_UKN_edge(const Mesh *Th, const Real seuil_pos, const SolutionVectorType & X_edge)
{
    const int  n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    SolutionVectorType X_proj = SolutionVectorType::Zero(n_edge);
    for(int iF = 0; iF < n_edge; iF ++){
        X_proj(iF) = max(X_edge(iF), seuil_pos);
    } //fot iF
    return X_proj;
}

SolutionSCType proj_SC_vol(const Mesh *Th, const Real seuil_pos, const SolutionSCType & U)
{
    SolutionSCType U_proj;
    for(int i =0; i< 2; i++){
        U_proj[i] =  proj_UKN_vol(Th, seuil_pos,  U[i]);
    }
    U_proj[2] = U[2];
    return U_proj;
}

SolutionSCType proj_SC_edge(const Mesh *Th, const Real seuil_pos, const SolutionSCType & X)
{
    SolutionSCType X_proj;
    for(int i = 0; i<2; i++){
        X_proj[i] =  proj_UKN_edge(Th, seuil_pos, X[i]);
    }
    X_proj[2] =  X[2];
    return X_proj;
}
//Matrix

void comp_sec_memb_Neumann(const Mesh *Th  , const LoadType & f,SolutionVectorType & Sec_m_vol )
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const Point & x_T = T.center();

        Real s = m_T * f(x_T) ;
        Sec_m_vol(iT) = s;
    }//for iT
}




// non-linear

//Matrix

void comp_G_vect(const Mesh *Th, const Real & pas_temps ,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu , const SolutionVectorType G_Neu,
    const SolutionVectorType & U_n , const SolutionVectorType & X_n , const std::vector<LocalVectorType> & Recons ,
    const std::vector<LocalVectorType> & Somme_Flux, const std::vector<Real> & Somme_Flux_Phi,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<vector<Real>> & Flux_Phi,
    const SolutionVectorType & V_vol, const SolutionVectorType & X_v_edge, SolutionVectorType & G_vol , SolutionVectorType & G_edge )
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        const Real & m_T = T.measure();
        const Real r_T_v =  Recons[iT].dot(loc_vec(Th, iT, V_vol, X_v_edge) );
        Real g1 = 0. ;

        g1 += ( m_T / pas_temps ) * (V_vol(iT) - U_n(iT)) ;
        g1 +=  r_T_v * (loc_nlvec(Th, iT, V_vol ,X_v_edge).dot(Somme_Flux[iT]) + Somme_Flux_Phi[iT] ) ;

        G_vol(iT) = + g1;

        for(int iF_loc = 0; iF_loc < m ; iF_loc ++){
            const int & iF = T.faceId(iF_loc);
            Real g2 = 0. ;
            g2 += loc_nlvec(Th, iT, V_vol ,X_v_edge).dot(Flux[iT][iF_loc]) + Flux_Phi[iT][iF_loc] ;
            g2 = r_T_v * g2;
            const Face & F =  Th->face(iF);
            if(isDir(F)){g2 = X_v_edge(iF) - X_b(iF) ;}
            if(isNeu(F)){g2 += - G_Neu(iF); }

            G_edge(iF) +=  g2;

        } //for iF_loc
    }//for iT
}

void triplets_invJ1andj2(const Mesh *Th, const Real & pas_temps , const std::vector<Real> & Alpha, const std::vector<vector<Real>> & B,
    const std::vector<LocalVectorType> & Recons,const std::vector<LocalVectorType> & Somme_Flux, const std::vector<Real> & Somme_Flux_Phi,
    const SolutionVectorType & U_vol, const SolutionVectorType & X_edge, std::vector<TripletType> & Triplets_invJ1, std::vector<TripletType> & Triplets_J2)
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const int & m = T.numberOfFaces();

        const Real j_1 = 0.5 * ( loc_nlvec(Th, iT, U_vol,X_edge).dot(Somme_Flux[iT]) + Somme_Flux_Phi[iT]);

        Real J = 0. ;
        J += m_T / pas_temps ;
        J += ( Alpha[iT] / U_vol[iT] ) * loc_vec(Th, iT, U_vol,X_edge).dot(Recons[iT]);
        J += j_1 ;
        Triplets_invJ1.push_back(TripletType(iT , iT , 1./J));

        for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            Real J_ = 0.;
            J_ += j_1 / m;
            J_ += - (B[iT][iF_loc] / X_edge[iF]) * loc_vec(Th, iT, U_vol,X_edge).dot(Recons[iT]);
            Triplets_J2.push_back(TripletType(iT, iF, J_));
        } //for iF_loc
    }//for iT
}

void triplets_J3andJ4(const Mesh *Th,const DefBoundaryConditionType & isDir, const std::vector<vector<Real>> & B,  const std::vector<vector<vector<Real>>> & A, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<vector<Real>> & Flux_Phi,
    const SolutionVectorType & U_vol, const SolutionVectorType & X_edge, std::vector<TripletType> & Triplets_J3, std::vector<TripletType> & Triplets_J4)
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const int & m = T.numberOfFaces();

        const Real r_T_v =  Recons[iT].dot(loc_vec(Th, iT, U_vol, X_edge) );
        const LocalVectorType log_v_T = loc_nlvec(Th, iT , U_vol, X_edge);

        for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            Real J_3 = 0. ;
            J_3 += (B[iT][iF_loc] / U_vol[iT]) * r_T_v ;
            J_3 += 0.5 * Flux_Phi[iT][iF_loc];
            J_3 += 0.5 * ( log_v_T.dot(Flux[iT][iF_loc]) );

            if(isDir(Th->face(iF))){J_3 = 0.; }

            Triplets_J3.push_back(TripletType(iF, iT, J_3));
            for(int iF_loc2 = 0; iF_loc2 < m; iF_loc2 ++ ){
                int iF2 = T.faceId(iF_loc2);
                Real J_4 = 0. ;
                J_4 +=  - A[iT][iF_loc][iF_loc2] * (r_T_v / X_edge[iF2]) ;

                J_4 +=  (0.5 / m) * (Flux_Phi[iT][iF_loc] + log_v_T.dot(Flux[iT][iF_loc]) );
                if(isDir(Th->face(iF))){
                    if(iF == iF2 ){J_4 = 1.;}
                    else {J_4 = 0.;}
                }//if F is Dir
                Triplets_J4.push_back(TripletType(iF, iF2, J_4));

            } //for iF_loc2
        }//for iF_loc
    }//for iT
}

//resol iteration

Real comp_sol_iter_newton(const Mesh *Th, const Real & pas_temps,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const SolutionVectorType & U_n , const SolutionVectorType & X_n ,
    const std::vector<vector<vector<Real>>> & A, const std::vector<vector<Real>> & B, const std::vector<Real> & Alpha, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<vector<Real>> & Flux_Phi,
    const std::vector<LocalVectorType> & Somme_Flux, const std::vector<Real> & Somme_Flux_Phi,
    const SolutionVectorType & U_ini , const SolutionVectorType & X_ini,
    SolutionVectorType & R_sol_vol , SolutionVectorType & R_sol_edge )
{
    //compute Triplets
    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    //std::cout << "entiers OK" << endl;
    R_sol_vol = SolutionVectorType::Zero(n_vol);
    R_sol_edge = SolutionVectorType::Zero(n_edge);

    std::vector<TripletType> Triplets_invJ1;
    Triplets_invJ1.reserve(n_vol);
    std::vector<TripletType> Triplets_J2;
    Triplets_J2.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_J3;
    Triplets_J3.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_J4;
    Triplets_J4.reserve(n_vol* n_edge_max * n_edge_max);

    //std::cout << "triplets declaration OK" << endl;

    triplets_invJ1andj2(Th, pas_temps , Alpha, B, Recons, Somme_Flux, Somme_Flux_Phi,  U_ini, X_ini  ,  Triplets_invJ1,  Triplets_J2);

    //std::cout << "triplets J1 et J2 OK" << endl;

    triplets_J3andJ4(Th,isDir, B, A, Recons, Flux , Flux_Phi    ,   U_ini , X_ini  ,   Triplets_J3, Triplets_J4);

    //std::cout << "triplets J3 et J4 OK" << endl;

    //compute matrix
    SparseMatrixType invJ1(n_vol, n_vol);
    invJ1.setFromTriplets(Triplets_invJ1.begin(), Triplets_invJ1.end());
    SparseMatrixType J2(n_vol, n_edge);
    J2.setFromTriplets(Triplets_J2.begin(), Triplets_J2.end());
    SparseMatrixType J3(n_edge, n_vol);
    J3.setFromTriplets(Triplets_J3.begin(), Triplets_J3.end());
    SparseMatrixType J4(n_edge, n_edge);
    J4.setFromTriplets(Triplets_J4.begin(), Triplets_J4.end());


    SolutionVectorType G1 = SolutionVectorType::Zero(n_vol);
    SolutionVectorType G2 = SolutionVectorType::Zero(n_edge);
    comp_G_vect(Th, pas_temps ,isDir, X_b , isNeu, G_Neu , U_n , X_n , Recons , Somme_Flux, Somme_Flux_Phi, Flux, Flux_Phi   ,   U_ini, X_ini,    G1 ,  G2 );
   // std::cout << "vecteurs G OK" << endl;
    SparseMatrixType LHS = J4 - J3 * invJ1 * J2;
    Eigen::VectorXd RHS = - G2 + J3 * invJ1  * G1;

    Eigen::SparseLU<SparseMatrixType> solver;
    // Compute the ordering permutation vector from the structural pattern of A
    solver.analyzePattern(LHS);
    // Compute the numerical factorization
    solver.factorize(LHS);
    //Use the factors to solve the linear system
    std::cout << "factorisation OK" << endl;
    R_sol_edge = solver.solve(RHS);
    std::cout << "solve edge OK" << endl;
    R_sol_vol = - invJ1 * ( J2 * R_sol_edge + G1 );
    Real Norm_G = std::max(G1.lpNorm<Eigen::Infinity>() , G2.lpNorm<Eigen::Infinity>());
    return Norm_G;
}


// computation potential

void comp_sec_memb_pot(const Mesh *Th, const SolutionVectorType & X_b , const DopingType & C, const DefBoundaryConditionType & isNeu , const BoundaryNeumannType g,
    const std::vector<vector<vector<Real>>> & A, const std::vector<vector<Real>> & B,
    SolutionVectorType & Sec_m_vol , SolutionVectorType & Sec_m_edge )
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        const Real & m_T = T.measure();
        const Point & x_T = T.center();

        Real s = m_T * C(x_T) ;

        for(int iF_loc = 0; iF_loc < m ; iF_loc ++){
            const int & iF = T.faceId(iF_loc);

            s += B[iT][iF_loc] * X_b(iF);
            Real s_F = 0.;

            for(int iF_loc2= 0; iF_loc2 < m; iF_loc2 ++){
                const int & iF2 = T.faceId(iF_loc2);
                s_F += A[iT][iF_loc][iF_loc2] * X_b(iF2);
            } //for iF_loc2
            Sec_m_edge(iF) += s_F;
        } //for iF_loc
        Sec_m_vol(iT) = s;
    }//for iT
    //ajout flux Neumann
    for(int iF = 0; iF < Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces(); iF ++){
        const Face & F = Th->face(iF);
        if(isNeu(F)){
            const Point & x_F = F.barycenter();
            const Real & m_F = F.measure();
            Sec_m_edge(iF) += m_F * g(x_F);
        } //if F is Neumann

    } //for iF
}

void triplets_invM1andM2(const Mesh *Th, const std::vector<Real> & Alpha, const std::vector<vector<Real>> & B, const std::vector<vector<vector<Real>>> & A,
    std::vector<TripletType> & Triplets_invM1, std::vector<TripletType> & Triplets_M2)
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const int & m = T.numberOfFaces();

        Triplets_invM1.push_back(TripletType(iT , iT , 1./Alpha[iT] ) );

        for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            Triplets_M2.push_back(TripletType(iT, iF, - B[iT][iF_loc]  ));
        } //for iF_loc
    }//for iT
}


void triplets_M3andM4(const Mesh *Th, const std::vector<vector<Real>> & B,  const std::vector<vector<vector<Real>>> & A,
    std::vector<TripletType> & Triplets_M3, std::vector<TripletType> & Triplets_M4)
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const int & m = T.numberOfFaces();

        for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);

            Triplets_M3.push_back(TripletType(iF, iT, B[iT][iF_loc]));
            for(int iF_loc2 = 0; iF_loc2 < m; iF_loc2 ++ ){
                int iF2 = T.faceId(iF_loc2);
                Triplets_M4.push_back(TripletType(iF, iF2, - A[iT][iF_loc][iF_loc2] ));
            } //for iF_loc2
        }//for iF_loc
    }//for iT
}


void comp_Potentialsolver(const Mesh* Th, const Real eta, const DiffusivityType & Lambda_Phi, const DopingType & C,
    const DefBoundaryConditionType & isDir, const BoundaryType & Phi_b , const DefBoundaryConditionType & isNeu , const BoundaryNeumannType & g_neu,
    SolutionVectorType & Potential_relev_boundary ,
    SparseMatrixType & Resize_Dir, SparseMatrixType & invM1, SparseMatrixType & M2 , SparseMatrixType & M3,
    SolutionVectorType & Sec_m_vol, SolutionVectorType & Sec_m_edge, SolverType & solver_potential){

    std::string sep = "\n----------------------------------------\n";
    // Pretty-print floating point numbers
    std::cout.precision(2);
    std::cout.flags(std::ios_base::scientific);
    Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");

    std::cout<< FORMAT(50) << "Precomputation : stationary diffusion problem for potential" << std::endl;

  //------------------------------------------------------------------------------
  // Estimate mesh size
  std::cout<< FORMAT(50) << "Stabilisation parameter" << eta << std::endl;

  Real h = Th->meshsize();

  //------------------------------------------------------------------------------
  // Assemble matrix
  //------------------------------------------------------------------------------


  int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
  int n_vol_unkw =  Th->numberOfCells();

  // Pre computation
  std::cout << "Pre-computation begins" << endl;
  common::chrono c_precomput;
  c_precomput.start();

  std::vector<vector<vector<Real>>> A;
  comp_A_elt(Th, Lambda_Phi, eta, A);

  std::vector<vector<Real>> B;
  comp_B_elt(Th, A, B );

  std::vector<Real> Alpha;
  comp_Alpha_elt(Th, B, Alpha );

  //discrétisation donnée au bord (relevement)
  Potential_relev_boundary = SolutionVectorType::Zero(nunkw);
  for(int iF = 0; iF < nunkw; iF ++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
       	if(isDir(F)){
            Potential_relev_boundary(iF) =  Phi_b(x_F);
        }// if
  }// for iF

  c_precomput.stop();
  cout << "Pre-computation ends" << endl;
  std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;

    common::chrono c_assembly;
    c_assembly.start();
    cout << "matrix assembly begins" << endl;



    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    int n_edge_Dir = 0.;
    for(int iF =0; iF < nunkw; iF ++){
        if(isDir(Th->face(iF))){n_edge_Dir += 1;}
    }

    // compute triplets
    std::vector<TripletType> Triplets_invM1;
    Triplets_invM1.reserve(n_vol);
    std::vector<TripletType> Triplets_M2;
    Triplets_M2.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M3;
    Triplets_M3.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_M4;
    Triplets_M4.reserve(n_vol* n_edge_max * n_edge_max);

    triplets_invM1andM2(Th,  Alpha, B, A , Triplets_invM1,  Triplets_M2);

    triplets_M3andM4(Th,  B, A, Triplets_M3, Triplets_M4);

    //compute matrix
    invM1.setFromTriplets(Triplets_invM1.begin(), Triplets_invM1.end());
    M2.setFromTriplets(Triplets_M2.begin(), Triplets_M2.end());
    M3.setFromTriplets(Triplets_M3.begin(), Triplets_M3.end());
    SparseMatrixType M4(n_edge, n_edge);
    M4.setFromTriplets(Triplets_M4.begin(), Triplets_M4.end());

    Sec_m_vol = SolutionVectorType::Zero(n_vol);
    Sec_m_edge =  SolutionVectorType::Zero(n_edge);

    comp_sec_memb_pot( Th, Potential_relev_boundary, C , isNeu,  g_neu, A ,  B ,
        Sec_m_vol ,  Sec_m_edge );

    //resize matrix for Dirichlet Boundary conditions
    //SparseMatrixType Resize_Dir(nunkw-n_edge_Dir,nunkw);
    int counter_Fb_Dir = 0;
    for(Integer iF = 0; iF < Th->numberOfFaces(); iF++){
        if(!(isDir(Th->face(iF)))){
            Resize_Dir.insert(iF - counter_Fb_Dir, iF)=1.;
        }//if isBoundary
    else{counter_Fb_Dir++;};
    }//for iF

    SparseMatrixType GLOBAL_LHS = Resize_Dir * (M4 -M3*invM1 * M2) * (Resize_Dir.transpose());
    //Eigen::VectorXd GLOBAL_RHS = Resize_Dir * (Sec_m_edge - M3*invM1 * Sec_m_vol);


    c_assembly.stop();
    cout << "Matrix assembly ends" << endl;
    std::cout << FORMAT(50) << "time_assembly"  << c_assembly.diff() << std::endl;

    // Solve linear system and reconstruct mesh unknowns

    common::chrono c_solve;
    c_solve.start();

    cout << "Linear solving begins" << endl;

    // Compute the ordering permutation vector from the structural pattern of ...
    solver_potential.analyzePattern(GLOBAL_LHS);
    std::cout << FORMAT(50) << "analyse pattern OK"  << std::endl;
    // Compute the numerical factorization
    solver_potential.factorize(GLOBAL_LHS);

    c_solve.stop();
    std::cout << FORMAT(50) << " LU factorisation Done !! "  << std::endl;
    std::cout << FORMAT(50) << "time_facto"  << c_solve.diff() << std::endl;

    //return solver_potential;
}




void comp_Potential(const Mesh *Th, const SolverType & solver_pot, const SolutionVectorType & N_vol , const SolutionVectorType & P_vol ,
    const SparseMatrixType & M_mass , const SparseMatrixType & Resize_Dir, const SparseMatrixType & invM1, const SparseMatrixType & M2 , const SparseMatrixType & M3,
    const SolutionVectorType & Sec_m_vol  , const SolutionVectorType & Sec_m_edge , const SolutionVectorType & Potential_relev_boundary,
    SolutionVectorType & Phi_sol_vol , SolutionVectorType & Phi_sol_edge ){

    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    Eigen::VectorXd GLOBAL_RHS = Resize_Dir * (Sec_m_edge - M3*invM1 * (Sec_m_vol + M_mass *(P_vol  - N_vol)));

    Phi_sol_vol = SolutionVectorType::Zero(n_vol);
    Phi_sol_edge = SolutionVectorType::Zero(n_edge);


    Phi_sol_edge= (Resize_Dir.transpose())*(solver_pot.solve(GLOBAL_RHS));
    Phi_sol_vol = invM1 * (Sec_m_vol+ M_mass * (P_vol -N_vol)  - M2*Phi_sol_edge);
    Phi_sol_edge = Phi_sol_edge + Potential_relev_boundary;

    std::cout << FORMAT(50) << "Potential computed "  << std::endl;


    }


// Thermal equilibrium

void comp_G_eq(const Mesh *Th, const Real & alpha_P, const Real & alpha_N , const DopingType & C,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu , const SolutionVectorType G_Neu,
    const std::vector<LocalVectorType> & Somme_Flux, const std::vector<vector<LocalVectorType>> & Flux,
    const SolutionVectorType & V_vol, const SolutionVectorType & V_edge, SolutionVectorType & G_vol , SolutionVectorType & G_edge )
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        const Real & m_T = T.measure();
        const Point & x_T = T.center();

        Real g1 = 0. ;

        g1 += - m_T * (C(x_T) + inv_nl(alpha_P-V_vol(iT)) - inv_nl(alpha_N + V_vol(iT)))  ;
        g1 += loc_vec(Th, iT, V_vol, V_edge).dot(Somme_Flux[iT]);

        G_vol(iT) = + g1;

        for(int iF_loc = 0; iF_loc < m ; iF_loc ++){
            const int & iF = T.faceId(iF_loc);
            Real g2 = 0. ;
            g2 += loc_vec(Th, iT, V_vol ,V_edge).dot(Flux[iT][iF_loc]) ;
            const Face & F =  Th->face(iF);
            if(isDir(F)){g2 = V_edge(iF) - X_b(iF) ;}
            if(isNeu(F)){g2 += - G_Neu(iF); }

            G_edge(iF) +=  g2;

        } //for iF_loc
    }//for iT
}

void triplets_invJ1andj2_eq(const Mesh *Th,  const Real & alpha_P, const Real & alpha_N ,
    const std::vector<Real> & Alpha, const std::vector<vector<Real>> & B,
    const SolutionVectorType & U_vol,  std::vector<TripletType> & Triplets_invJ1, std::vector<TripletType> & Triplets_J2)
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const int & m = T.numberOfFaces();


        Real J = 0. ;
        J +=  m_T * (d_inv_nl(alpha_P - U_vol(iT)) + d_inv_nl(alpha_N + U_vol(iT)) )  ;
        J += Alpha[iT];
        Triplets_invJ1.push_back(TripletType(iT , iT , 1./J));

        for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            Real J_ = 0.;
            J_ += - B[iT][iF_loc];
            Triplets_J2.push_back(TripletType(iT, iF, J_));
        } //for iF_loc
    }//for iT
}

void triplets_J3andJ4_eq(const Mesh *Th, const Real & alpha_P, const Real & alpha_N ,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu , const SolutionVectorType G_Neu,
    const std::vector<vector<Real>> & B,  const std::vector<vector<vector<Real>>> & A,
    std::vector<TripletType> & Triplets_J3, std::vector<TripletType> & Triplets_J4)
{
    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const int & m = T.numberOfFaces();

        for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            Real J_3 = 0. ;
            J_3 += B[iT][iF_loc];
            if(isDir(Th->face(iF))){J_3 = 0.; }

            Triplets_J3.push_back(TripletType(iF, iT, J_3));
            for(int iF_loc2 = 0; iF_loc2 < m; iF_loc2 ++ ){
                int iF2 = T.faceId(iF_loc2);
                Real J_4 = 0. ;
                J_4 +=  - A[iT][iF_loc][iF_loc2] ;
                if(isDir(Th->face(iF))){
                    if(iF == iF2 ){J_4 = 1.;}
                    else {J_4 = 0.;}
                }//if F is Dir
                Triplets_J4.push_back(TripletType(iF, iF2, J_4));

            } //for iF_loc2
        }//for iF_loc
    }//for iT
}

Real comp_sol_iter_newton_eq(const Mesh *Th, const Real & alpha_P, const Real & alpha_N ,const DopingType & C,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const std::vector<vector<vector<Real>>> & A, const std::vector<vector<Real>> & B, const std::vector<Real> & Alpha,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<LocalVectorType> & Somme_Flux,
    const SolutionVectorType & U_ini , const SolutionVectorType & X_ini, SolutionVectorType & R_sol_vol , SolutionVectorType & R_sol_edge )
{
    //compute Triplets
    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    //std::cout << "entiers OK" << endl;
    R_sol_vol = SolutionVectorType::Zero(n_vol);
    R_sol_edge = SolutionVectorType::Zero(n_edge);

    std::vector<TripletType> Triplets_invJ1;
    Triplets_invJ1.reserve(n_vol);
    std::vector<TripletType> Triplets_J2;
    Triplets_J2.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_J3;
    Triplets_J3.reserve(n_vol * n_edge_max);
    std::vector<TripletType> Triplets_J4;
    Triplets_J4.reserve(n_vol* n_edge_max * n_edge_max);

    //std::cout << "triplets declaration OK" << endl;
    triplets_invJ1andj2_eq(Th,  alpha_P,  alpha_N , Alpha,  B, U_ini, Triplets_invJ1,  Triplets_J2);

    //std::cout << "triplets J1 et J2 OK" << endl;
    triplets_J3andJ4_eq(Th , alpha_P , alpha_N , isDir , X_b , isNeu , G_Neu, B,   A,  Triplets_J3,  Triplets_J4);

    //std::cout << "triplets J3 et J4 OK" << endl;

    //compute matrix
    SparseMatrixType invJ1(n_vol, n_vol);
    invJ1.setFromTriplets(Triplets_invJ1.begin(), Triplets_invJ1.end());
    SparseMatrixType J2(n_vol, n_edge);
    J2.setFromTriplets(Triplets_J2.begin(), Triplets_J2.end());
    SparseMatrixType J3(n_edge, n_vol);
    J3.setFromTriplets(Triplets_J3.begin(), Triplets_J3.end());
    SparseMatrixType J4(n_edge, n_edge);
    J4.setFromTriplets(Triplets_J4.begin(), Triplets_J4.end());


    SolutionVectorType G1 = SolutionVectorType::Zero(n_vol);
    SolutionVectorType G2 = SolutionVectorType::Zero(n_edge);
    comp_G_eq(Th,  alpha_P, alpha_N ,  C, isDir ,  X_b ,  isNeu , G_Neu, Somme_Flux,  Flux,  U_ini, X_ini,    G1 ,  G2);

   // std::cout << "vecteurs G OK" << endl;
    SparseMatrixType LHS = J4 - J3 * invJ1 * J2;
    Eigen::VectorXd RHS = - G2 + J3 * invJ1  * G1;

    Eigen::SparseLU<SparseMatrixType> solver;
    // Compute the ordering permutation vector from the structural pattern of A
    solver.analyzePattern(LHS);
    // Compute the numerical factorization
    solver.factorize(LHS);
    //Use the factors to solve the linear system
    std::cout << "factorisation OK" << endl;
    R_sol_edge = solver.solve(RHS);
    std::cout << "solve edge OK" << endl;
    R_sol_vol = - invJ1 * ( J2 * R_sol_edge + G1 );
    Real Norm_G = std::max(G1.lpNorm<Eigen::Infinity>() , G2.lpNorm<Eigen::Infinity>());
    return Norm_G;
}

void test_Jaco_eq(const Mesh *Th, const Real & alpha_P, const Real & alpha_N ,const DopingType & C,
    const DefBoundaryConditionType & isDir , const SolutionVectorType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const std::vector<vector<vector<Real>>> & A, const std::vector<vector<Real>> & B, const std::vector<Real> & Alpha,
    const std::vector<vector<LocalVectorType>> & Flux , const std::vector<LocalVectorType> & Somme_Flux)
    {

    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    //std::cout << "entiers OK" << endl;
    cout << "Test de la Jacobienne, c'est parti !" << endl;

    for(int nb_base = 0; nb_base < 3; nb_base ++){
        cout << "Point de base numero "<< nb_base +1 << endl;
        SolutionVectorType U_vol = SolutionVectorType::Random(n_vol);
        SolutionVectorType U_edge = SolutionVectorType::Random(n_edge);

        SolutionVectorType G1_base = SolutionVectorType::Zero(n_vol);
        SolutionVectorType G2_base = SolutionVectorType::Zero(n_edge);
        comp_G_eq(Th,  alpha_P, alpha_N ,  C, isDir ,  X_b ,  isNeu , G_Neu, Somme_Flux,  Flux,  U_vol, U_edge,    G1_base ,  G2_base);

        std::vector<TripletType> Triplets_invJ1;
        Triplets_invJ1.reserve(n_vol);
        std::vector<TripletType> Triplets_J2;
        Triplets_J2.reserve(n_vol * n_edge_max);
        std::vector<TripletType> Triplets_J3;
        Triplets_J3.reserve(n_vol * n_edge_max);
        std::vector<TripletType> Triplets_J4;
        Triplets_J4.reserve(n_vol* n_edge_max * n_edge_max);

        triplets_invJ1andj2_eq(Th,  alpha_P,  alpha_N , Alpha,  B, U_vol, Triplets_invJ1,  Triplets_J2);
        triplets_J3andJ4_eq(Th , alpha_P , alpha_N , isDir , X_b , isNeu , G_Neu, B,   A,  Triplets_J3,  Triplets_J4);

        SparseMatrixType invJ1(n_vol, n_vol);
        invJ1.setFromTriplets(Triplets_invJ1.begin(), Triplets_invJ1.end());
        SparseMatrixType J2(n_vol, n_edge);
        J2.setFromTriplets(Triplets_J2.begin(), Triplets_J2.end());
        SparseMatrixType J3(n_edge, n_vol);
        J3.setFromTriplets(Triplets_J3.begin(), Triplets_J3.end());
        SparseMatrixType J4(n_edge, n_edge);
        J4.setFromTriplets(Triplets_J4.begin(), Triplets_J4.end());

        SparseMatrixType Schur = J4 - J3 * invJ1 * J2;

        for(int nb_dir = 0; nb_dir < 5; nb_dir++){
            cout << "Direction numero "<< nb_dir +1 << endl;
            cout << "   " << endl;
            SolutionVectorType X = SolutionVectorType::Random(n_edge);
            SolutionVectorType V = - invJ1 *  J2 * X;

            SolutionVectorType G_J_2 = Schur * X;
            for(int nb_h = 0; nb_h < 5; nb_h ++){
                Real h = 1. * std::pow(10, -nb_h);
                SolutionVectorType G1_dir = SolutionVectorType::Zero(n_vol);
                SolutionVectorType G2_dir = SolutionVectorType::Zero(n_edge);
                comp_G_eq(Th,  alpha_P, alpha_N ,  C, isDir ,  X_b ,  isNeu , G_Neu, Somme_Flux,  Flux,  U_vol+h *V , U_edge+h*X,    G1_dir ,  G2_dir);
                SolutionVectorType Err_vol = G1_dir - G1_base  ;
                SolutionVectorType Err_edge = G2_dir - G2_base - h * G_J_2;
                Real Norm_Err = std::max(Err_vol.lpNorm<Eigen::Infinity>() , Err_edge.lpNorm<Eigen::Infinity>());
                //Norm_Err =  Err_edge.lpNorm<Eigen::Infinity>();

                cout << "Pas h  :"<< h  <<", erreur / h^2  :" << Norm_Err/(h*h)   << endl;
            }//for nb_h
            cout << "\n "<< endl;
            cout << "Changement de direction "<< endl;
        } //for nb_dir
        cout << "\n \n"<< endl;
        cout << "Changement de point de base "<< endl;

    } //for nb_base

}

// Semiconductors, coupled scheme

void comp_G_sc(const Mesh *Th, const Real & pas_temps , const SolutionVectorType C,
    const DefBoundaryConditionType & isDir , const SolutionSCType & X_b , const DefBoundaryConditionType & isNeu , const SolutionVectorType G_Neu,
    const SolutionSCType & U_n, const std::vector<LocalVectorType> & Recons ,
    const std::vector<vector<vector<LocalVectorType>>> & Flux , const std::vector<vector<LocalVectorType>> & Somme_Flux,
    const std::vector<vector<vector<Real>>> & Flux_Potential , const std::vector<vector<Real>> & Sum_Flux_Potential,
    const SolutionSCType & U , const SolutionSCType & X, SolutionVectorType & G_vol , SolutionVectorType & G_edge )
{
    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();


    for(Integer iT = 0; iT< n_vol ; iT++ ){
        const Cell & T = Th->cell(iT);
        const Integer & m = T.numberOfFaces();
        const Real & m_T = T.measure();
        const Real r_T_N =  Recons[iT].dot(loc_vec(Th, iT, U[0], X[0]) );
        const Real r_T_P =  Recons[iT].dot(loc_vec(Th, iT, U[1], X[1]) );
        const Real reac = m_T *Reaction(U[0](iT),U[1](iT));
        Real gN = 0. ;
        Real gP = 0. ;
        Real gPhi = 0. ;

        gN += ( m_T / pas_temps ) * (U[0](iT) - U_n[0](iT)) ;
        gN +=  r_T_N * (loc_nlvec(Th, iT, U[0] ,X[0]).dot(Somme_Flux[0][iT]) + Sum_Flux_Potential[0][iT] ) ; // !! N -> -Phi
        gN +=  reac ;

        gP += ( m_T / pas_temps ) * (U[1](iT) - U_n[1](iT)) ;
        gP +=  r_T_P * (loc_nlvec(Th, iT, U[1] ,X[1]).dot(Somme_Flux[1][iT]) + Sum_Flux_Potential[1][iT] ) ;
        gP += reac ;

        gPhi += - m_T * (C(iT) + U[1](iT) - U[0](iT))  ;
        gPhi +=  Sum_Flux_Potential[2][iT];
        //gPhi += loc_vec(Th, iT, U[2], X[2]).dot(Somme_Flux[2][iT]);

        G_vol(iT) = + gN;
        G_vol(n_vol + iT) = + gP;
        G_vol(2*n_vol + iT) = + gPhi;

        for(int iF_loc = 0; iF_loc < m ; iF_loc ++){
            const int & iF = T.faceId(iF_loc);
            Real gN2 = 0. ;
            Real gP2 = 0. ;
            Real gPhi2 = 0. ;

            gN2 += loc_nlvec(Th, iT, U[0] ,X[0]).dot(Flux[0][iT][iF_loc]) + Flux_Potential[0][iT][iF_loc] ;
            gN2 = r_T_N * gN2;

            gP2 += loc_nlvec(Th, iT, U[1] ,X[1]).dot(Flux[1][iT][iF_loc]) + Flux_Potential[1][iT][iF_loc] ;
            gP2 = r_T_P * gP2;

            gPhi2 += Flux_Potential[2][iT][iF_loc];
           // gPhi2 += loc_vec(Th, iT, U[2] , X[2]).dot(Flux[2][iT][iF_loc]);

            const Face & F =  Th->face(iF);
            if(isDir(F)){
                gN2 = X[0](iF) - X_b[0](iF);
                gP2 = X[1](iF) - X_b[1](iF);
                gPhi2 = X[2](iF) - X_b[2](iF);
            } // if F is Dirichlet

            if(isNeu(F)){
                gN2 += - G_Neu(iF);
                gP2 += - G_Neu(iF);
                gPhi2 += - G_Neu(iF);
            }

            G_edge(iF) +=  gN2;
            G_edge(n_edge + iF) +=  gP2;
            G_edge(2*n_edge + iF) +=  gPhi2;

        } //for iF_loc
    }//for iT
}


void triplets_invJ1andj2_sc(const Mesh *Th, const Real & pas_temps , const std::vector<vector<Real>> & Alpha, const std::vector<vector<vector<Real>>> & B,const std::vector<vector<vector<Real>>> & Brev,
    const std::vector<LocalVectorType> & Recons,const std::vector<vector<LocalVectorType>> & Somme_Flux, const std::vector<vector<Real>> & Sum_Flux_Potential,
    const SolutionSCType & U, const SolutionSCType & X, std::vector<TripletType> & Triplets_invJ1, std::vector<TripletType> & Triplets_J2)
{
    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();

//    std::vector<TripletType> Triplets_J1;
//    Triplets_J1.reserve(5*n_vol);

    for(Integer iT = 0; iT<  n_vol;iT++ ){
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        const int & m = T.numberOfFaces();

        Real r_T_N = loc_vec(Th, iT, U[0],X[0]).dot(Recons[iT]);
        Real r_T_P = loc_vec(Th, iT, U[1],X[1]).dot(Recons[iT]);

        const Real j_N = 0.5 * ( loc_nlvec(Th, iT, U[0],X[0]).dot(Somme_Flux[0][iT]) + Sum_Flux_Potential[0][iT]);
        const Real j_P = 0.5 * ( loc_nlvec(Th, iT, U[1],X[1]).dot(Somme_Flux[1][iT]) + Sum_Flux_Potential[1][iT]);


        Real JN = 0. ;
        Real JP = 0. ;
        JN += m_T / pas_temps ;
        JN += r_T_N * d_nl(U[0](iT)) * Alpha[0][iT];
        JN += j_N ;
        JN += m_T *Partial_N_Reac(U[0](iT),U[1](iT));
        JP += m_T / pas_temps ;
        JP += r_T_P * d_nl(U[1](iT)) * Alpha[1][iT];
        JP += j_P ;
        JP += m_T*Partial_P_Reac(U[0](iT),U[1](iT));

        Real d_1 = JN;
        Real d_2 = JP;
        Real d_3 = Alpha[2][iT];
        Real m_1 = - Alpha[0][iT] * r_T_N;
        Real m_2 = + Alpha[1][iT] * r_T_P;
        Real m_3 = m_T;
        Real m_4 = -m_T;
//
//        Triplets_J1.push_back(TripletType(iT, iT ,d_1));
//        Triplets_J1.push_back(TripletType(n_vol +iT,n_vol+ iT ,  d_2));
//        Triplets_J1.push_back(TripletType(2*n_vol + iT,2*n_vol+ iT , d_3));
//
//        Triplets_J1.push_back(TripletType(iT, 2*n_vol+ iT , m_1 ));
//        Triplets_J1.push_back(TripletType(n_vol + iT, 2*n_vol+ iT , m_2 ));
//
//        Triplets_J1.push_back(TripletType(2*n_vol + iT,iT , m_3));
//        Triplets_J1.push_back(TripletType(2*n_vol + iT, n_vol + iT , m_4));

        Real E_inv = 1. / ( d_3 - m_3 * (m_1 / d_1 ) - m_4 * (m_2 /d_2) );

//        Real f_3_3 =  E_inv;
//        Real f_3_2 = -E_inv * (m_4 /d_2);
//        Real f_3_1 = -E_inv * (m_3 /d_1);
//
//        Triplets_invJ1.push_back(TripletType(2*n_vol+iT, iT , f_3_1));
//        Triplets_invJ1.push_back(TripletType(2*n_vol+iT, n_vol + iT , f_3_2));
//        Triplets_invJ1.push_back(TripletType(2*n_vol+iT, 2*n_vol + iT ,  f_3_3  ));
//
//        Triplets_invJ1.push_back(TripletType(n_vol+iT, iT , -(1./d_2) * m_2 * f_3_1));
//        Triplets_invJ1.push_back(TripletType(n_vol+iT, n_vol + iT , (1./d_2) * (1. - m_2*f_3_2)));
//        Triplets_invJ1.push_back(TripletType(n_vol+iT, 2*n_vol + iT , -(1./d_2)*m_2* f_3_3  ));
//
//        Triplets_invJ1.push_back(TripletType(iT, iT , (1./d_1)* (1. -m_1* f_3_1) ));
//        Triplets_invJ1.push_back(TripletType(iT, n_vol + iT , -(1./d_1)*m_1* f_3_2  ));
//        Triplets_invJ1.push_back(TripletType(iT, 2*n_vol + iT , -(1./d_1)*m_1* f_3_3  ));

        Real f_3_3 =  E_inv;
        Real f_2_3 = -E_inv * (m_2 /d_2);
        Real f_1_3 = -E_inv * (m_1 /d_1);

        Triplets_invJ1.push_back(TripletType(iT,2*n_vol+ iT , f_1_3));
        Triplets_invJ1.push_back(TripletType(n_vol +iT,2*n_vol+ iT , f_2_3));
        Triplets_invJ1.push_back(TripletType(2*n_vol + iT,2*n_vol+ iT , f_3_3));

        Triplets_invJ1.push_back(TripletType(iT,n_vol+ iT , -f_1_3*(m_4/d_2)));
        Triplets_invJ1.push_back(TripletType(n_vol +iT,n_vol+ iT , (1.-f_2_3*m_4) / d_2));
        Triplets_invJ1.push_back(TripletType(2*n_vol + iT,n_vol+ iT , -f_3_3 *(m_4/d_2)));

        Triplets_invJ1.push_back(TripletType(iT,iT , (1.-f_1_3*m_3) /d_1));
        Triplets_invJ1.push_back(TripletType(n_vol +iT,iT , -f_2_3* (m_3 / d_1)));
        Triplets_invJ1.push_back(TripletType(2*n_vol + iT,iT , -f_3_3 *(m_3/d_1)));

        for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            Real J_N = 0.;
            Real J_P = 0.;
            Real J_Phi = 0.;
            Real J_N_Phi = 0.;
            Real J_P_Phi =0.;

            J_N += j_N / m;
            J_N +=  - r_T_N * d_nl(X[0](iF)) * Brev[0][iT][iF_loc];
            J_P += j_P / m;
            J_P += - r_T_P * d_nl(X[1](iF)) * Brev[1][iT][iF_loc];
            J_Phi += - B[2][iT][iF_loc];
            Triplets_J2.push_back(TripletType(iT, iF, J_N));
            Triplets_J2.push_back(TripletType(n_vol + iT, n_edge + iF, J_P));
            Triplets_J2.push_back(TripletType(2*n_vol + iT, 2*n_edge + iF, J_Phi));

            J_N_Phi += Brev[0][iT][iF_loc] * r_T_N;
            J_P_Phi += - Brev[1][iT][iF_loc] * r_T_P;

            Triplets_J2.push_back(TripletType(iT, 2*n_edge + iF, J_N_Phi));
            Triplets_J2.push_back(TripletType(n_vol + iT, 2*n_edge + iF, J_P_Phi));
        } //for iF_loc
    }//for iT

//    SparseMatrixType invJ1(3*n_vol, 3*n_vol);
//    invJ1.setFromTriplets(Triplets_invJ1.begin(), Triplets_invJ1.end());
//    SparseMatrixType J1(3*n_vol, 3*n_vol);
//    J1.setFromTriplets(Triplets_J1.begin(), Triplets_J1.end());
//    std::cout << "\n \n \n J1 * invJ1" << endl;
//    std::cout << J1 * invJ1<< endl;

}

void triplets_J3andJ4_sc(const Mesh *Th,const DefBoundaryConditionType & isDir,
    const std::vector<vector<vector<Real>>> & B, const std::vector<vector<vector<Real>>> & Brev, const std::vector<vector<vector<vector<Real>>>> & A, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<vector<LocalVectorType>>> & Flux , const std::vector<vector<vector<Real>>> & Flux_Potential,
    const SolutionSCType & U, const SolutionSCType & X, std::vector<TripletType> & Triplets_J3, std::vector<TripletType> & Triplets_J4)
{
    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();

    for(Integer iT = 0; iT<  n_vol;iT++ ){
        const Cell & T = Th->cell(iT);
        const int & m = T.numberOfFaces();

        const Real r_T_N =  Recons[iT].dot(loc_vec(Th, iT, U[0], X[0] ) );
        const Real r_T_P =  Recons[iT].dot(loc_vec(Th, iT, U[1], X[1] ) );

        const LocalVectorType nl_N_T = loc_nlvec(Th, iT , U[0], X[0]);
        const LocalVectorType nl_P_T = loc_nlvec(Th, iT , U[1], X[1]);

        for(int iF_loc = 0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            Real J_3N = 0. ;
            Real J_3P = 0. ;
            Real J_3Phi = 0. ;

            Real J_3_N_Phi = 0. ;
            Real J_3_P_Phi = 0. ;

            J_3N += B[0][iT][iF_loc] * d_nl(U[0](iT)) * r_T_N ;
            J_3N += 0.5 * Flux_Potential[0][iT][iF_loc];
            J_3N += 0.5 * ( nl_N_T.dot(Flux[0][iT][iF_loc]) );
            J_3P += B[1][iT][iF_loc] * d_nl(U[1](iT)) * r_T_P ;
            J_3P += 0.5 * Flux_Potential[1][iT][iF_loc];
            J_3P += 0.5 * ( nl_P_T.dot(Flux[1][iT][iF_loc]) );
            J_3Phi +=  B[2][iT][iF_loc];

            J_3_N_Phi += -B[0][iT][iF_loc] *r_T_N;
            J_3_P_Phi += B[1][iT][iF_loc] *r_T_P;

            if(isDir(Th->face(iF))){
            J_3N = 0.;
            J_3P = 0.;
            J_3Phi = 0.;
            J_3_N_Phi = 0. ;
            J_3_P_Phi = 0. ;
            } //if Dir

            Triplets_J3.push_back(TripletType(iF, iT, J_3N));
            Triplets_J3.push_back(TripletType(n_edge +iF,n_vol + iT, J_3P));
            Triplets_J3.push_back(TripletType(2*n_edge + iF,2*n_vol + iT, J_3Phi));
            Triplets_J3.push_back(TripletType(iF, 2*n_vol + iT, J_3_N_Phi));
            Triplets_J3.push_back(TripletType(n_edge +iF, 2*n_vol+  iT, J_3_P_Phi));

            for(int iF_loc2 = 0; iF_loc2 < m; iF_loc2 ++ ){
                int iF2 = T.faceId(iF_loc2);
                Real J_4N = 0. ;
                Real J_4P = 0. ;
                Real J_4Phi = 0. ;
                Real J_4_N_Phi = 0. ;
                Real J_4_P_Phi = 0. ;

                J_4N +=  - A[0][iT][iF_loc][iF_loc2] * r_T_N * d_nl(X[0](iF2)) ;
                J_4N +=  (0.5 / m) * (Flux_Potential[0][iT][iF_loc] + nl_N_T.dot(Flux[0][iT][iF_loc]) );
                J_4P +=  - A[1][iT][iF_loc][iF_loc2] * r_T_P * d_nl( X[1](iF2)) ;
                J_4P +=  (0.5 / m) * (Flux_Potential[1][iT][iF_loc] + nl_P_T.dot(Flux[1][iT][iF_loc]) );
                J_4Phi +=- A[2][iT][iF_loc][iF_loc2] ;

                J_4_N_Phi += A[0][iT][iF_loc][iF_loc2] * r_T_N  ;
                J_4_P_Phi += - A[1][iT][iF_loc][iF_loc2] * r_T_P ;

                if(isDir(Th->face(iF))){
                    if(iF == iF2 ){
                        J_4N = 1.;
                        J_4P = 1.;
                        J_4Phi = 1.;
                        J_4_N_Phi = 0.;
                        J_4_P_Phi = 0.;}
                    else {
                        J_4N = 0. ;
                        J_4P = 0. ;
                        J_4Phi = 0. ;
                        J_4_N_Phi = 0.;
                        J_4_P_Phi = 0.;
                        }
                }

                Triplets_J4.push_back(TripletType(iF, iF2, J_4N));
                Triplets_J4.push_back(TripletType(n_edge + iF,n_edge + iF2, J_4P));
                Triplets_J4.push_back(TripletType(2*n_edge +iF, 2*n_edge +iF2, J_4Phi));
                Triplets_J4.push_back(TripletType(iF, 2*n_edge +iF2, J_4_N_Phi));
                Triplets_J4.push_back(TripletType(n_edge + iF, 2*n_edge + iF2, J_4_P_Phi));

            } //for iF_loc2
        }//for iF_loc
    }//for iT
}

Real comp_sol_iter_newton_sc(const Mesh *Th, const Real & pas_temps,const SolutionVectorType C,
    const DefBoundaryConditionType & isDir , const SolutionSCType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const SolutionSCType & U_n , const SolutionSCType & X_n ,
    const std::vector<vector<vector<vector<Real>>>> & A, const std::vector<vector<vector<Real>>> & B, const std::vector<vector<vector<Real>>> & Brev,const std::vector<vector<Real>> & Alpha, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<vector<LocalVectorType>>> & Flux , const std::vector<vector<LocalVectorType>> & Somme_Flux,
    const SolutionSCType & U , const SolutionSCType & X,
    SolutionVectorType & R_sol_vol , SolutionVectorType & R_sol_edge )
{
    //compute Triplets
    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    std::vector<vector<Real>>  Flux_Potential_N;
    std::vector<vector<Real>>  Flux_Potential_P;
    std::vector<vector<Real>>  Flux_Potential_Phi;
    comp_flux_potential(Th, Flux[0],-U[2] ,-X[2], Flux_Potential_N); // N with potential -Phi
    comp_flux_potential(Th, Flux[1], U[2] , X[2], Flux_Potential_P);
    comp_flux_potential(Th, Flux[2], U[2] , X[2], Flux_Potential_Phi);
    std::vector<vector<vector<Real>>> Flux_Potential_sc;
    Flux_Potential_sc.resize(3);
    Flux_Potential_sc[0] = Flux_Potential_N;
    Flux_Potential_sc[1] = Flux_Potential_P;
    Flux_Potential_sc[2] = Flux_Potential_Phi;

    std::vector<Real>  Sum_Flux_Potential_N;
    std::vector<Real>  Sum_Flux_Potential_P;
    std::vector<Real>  Sum_Flux_Potential_Phi;
    comp_Sumflux_potential(Th, Somme_Flux[0],-U[2] ,-X[2], Sum_Flux_Potential_N); // N with potential -Phi
    comp_Sumflux_potential(Th, Somme_Flux[1], U[2] , X[2], Sum_Flux_Potential_P);
    comp_Sumflux_potential(Th, Somme_Flux[2], U[2] , X[2], Sum_Flux_Potential_Phi);
    std::vector<vector<Real>> Sum_Flux_Potential_sc;
    Sum_Flux_Potential_sc.resize(3);
    Sum_Flux_Potential_sc[0] = Sum_Flux_Potential_N;
    Sum_Flux_Potential_sc[1] = Sum_Flux_Potential_P;
    Sum_Flux_Potential_sc[2] = Sum_Flux_Potential_Phi;

    //std::cout << "entiers OK" << endl;

    R_sol_vol = SolutionVectorType::Zero(3*n_vol);
    R_sol_edge = SolutionVectorType::Zero(3*n_edge);

    std::vector<TripletType> Triplets_invJ1;
    Triplets_invJ1.reserve(9 * n_vol);
    std::vector<TripletType> Triplets_J2;
    Triplets_J2.reserve(5* n_vol * n_edge_max);
    std::vector<TripletType> Triplets_J3;
    Triplets_J3.reserve(5*n_vol * n_edge_max);
    std::vector<TripletType> Triplets_J4;
    Triplets_J4.reserve(5*n_vol* n_edge_max * n_edge_max);

    //std::cout << "triplets declaration OK" << endl;
    triplets_invJ1andj2_sc(Th, pas_temps , Alpha,  B, Brev ,Recons, Somme_Flux, Sum_Flux_Potential_sc , U,  X, Triplets_invJ1, Triplets_J2);

    //std::cout << "triplets J1 et J2 OK" << endl;
    triplets_J3andJ4_sc( Th, isDir, B, Brev, A,  Recons, Flux ,  Flux_Potential_sc, U, X, Triplets_J3, Triplets_J4);

    //std::cout << "triplets J3 et J4 OK" << endl;

    //compute matrix
    SparseMatrixType invJ1(3*n_vol, 3*n_vol);
    invJ1.setFromTriplets(Triplets_invJ1.begin(), Triplets_invJ1.end());
    SparseMatrixType J2(3*n_vol, 3*n_edge);
    J2.setFromTriplets(Triplets_J2.begin(), Triplets_J2.end());
    SparseMatrixType J3(3*n_edge, 3*n_vol);
    J3.setFromTriplets(Triplets_J3.begin(), Triplets_J3.end());
    SparseMatrixType J4(3*n_edge, 3*n_edge);
    J4.setFromTriplets(Triplets_J4.begin(), Triplets_J4.end());


    SolutionVectorType G1 = SolutionVectorType::Zero(3*n_vol);
    SolutionVectorType G2 = SolutionVectorType::Zero(3*n_edge);
    //comp_G_vect(Th, pas_temps ,isDir, X_b , isNeu, G_Neu , U_n , X_n , Recons , Somme_Flux, Somme_Flux_Phi, Flux, Flux_Phi   ,   U_ini, X_ini,    G1 ,  G2 );

    comp_G_sc( Th, pas_temps , C, isDir ,  X_b , isNeu ,  G_Neu, U_n,
        Recons , Flux , Somme_Flux , Flux_Potential_sc , Sum_Flux_Potential_sc ,
        U , X , G1 , G2 );

   // std::cout << "vecteurs G OK" << endl;
    SparseMatrixType LHS = J4 - J3 * invJ1 * J2;
    Eigen::VectorXd RHS = - G2 + J3 * invJ1  * G1;

    Eigen::SparseLU<SparseMatrixType> solver;
    // Compute the ordering permutation vector from the structural pattern of A
    solver.analyzePattern(LHS);
    // Compute the numerical factorization
    solver.factorize(LHS);
    //Use the factors to solve the linear system
    std::cout << "factorisation OK" << endl;
    R_sol_edge = solver.solve(RHS);
    std::cout << "solve edge OK" << endl;
    R_sol_vol = - invJ1 * ( J2 * R_sol_edge + G1 );
    Real Norm_G = std::max(G1.lpNorm<Eigen::Infinity>() , G2.lpNorm<Eigen::Infinity>());
    return Norm_G;
}

SolutionSCType SolVec_to_SC_vol(const Mesh *Th, const SolutionVectorType & R_vol){
    int n_vol = Th->numberOfCells();
    SolutionSCType R_vol_sc;
    for(int i =0; i< 3 ; i ++){
        R_vol_sc[i] = SolutionVectorType::Zero(n_vol);
        for(int iT =0 ; iT< n_vol ; iT ++){
            R_vol_sc[i](iT) = R_vol(i*n_vol + iT);
        }
    }
    return R_vol_sc;
}

SolutionSCType SolVec_to_SC_edge(const Mesh *Th, const SolutionVectorType & R_edge){
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    SolutionSCType R_edge_sc;
    for(int i =0; i< 3 ; i ++){
        R_edge_sc[i] = SolutionVectorType::Zero(n_edge);
        for(int iF =0 ; iF< n_edge ; iF ++){
            R_edge_sc[i](iF) = R_edge(i*n_edge + iF);
        }
    }
    return R_edge_sc;
}


void test_Jaco_sc(const Mesh *Th, const Real & pas_temps,const SolutionVectorType C,
    const DefBoundaryConditionType & isDir , const SolutionSCType & X_b , const DefBoundaryConditionType & isNeu ,const SolutionVectorType G_Neu,
    const SolutionSCType & U_n , const SolutionSCType & X_n ,
    const std::vector<vector<vector<vector<Real>>>> & A, const std::vector<vector<vector<Real>>> & B,const std::vector<vector<vector<Real>>> & Brev, const std::vector<vector<Real>> & Alpha, const std::vector<LocalVectorType> & Recons,
    const std::vector<vector<vector<LocalVectorType>>> & Flux , const std::vector<vector<LocalVectorType>> & Somme_Flux)
    {

    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_edge_max =Th->maximumNumberOfFaces();

    //std::cout << "entiers OK" << endl;
    cout << "Test de la Jacobienne, c'est parti !" << endl;

    for(int nb_base = 0; nb_base < 3; nb_base ++){
        cout << "Point de base numero "<< nb_base +1 << endl;
        SolutionSCType U;
        SolutionSCType X;
        for(int i = 0; i < 2; i++){
            //U[i] = (SolutionVectorType::Random(n_vol)).cwiseAbs();
            //X[i] = SolutionVectorType::Random(n_edge).cwiseAbs();
            U[i] = proj_UKN_vol(Th, std::pow(1.,-10),SolutionVectorType::Random(n_vol));
            X[i] = proj_UKN_edge(Th, std::pow(1.,-10),SolutionVectorType::Random(n_edge));
        }

        U[2] = SolutionVectorType::Random(n_vol);
        X[2] = SolutionVectorType::Random(n_edge);


        std::vector<vector<Real>>  Flux_Potential_N;
        std::vector<vector<Real>>  Flux_Potential_P;
        std::vector<vector<Real>>  Flux_Potential_Phi;
        comp_flux_potential(Th, Flux[0], -U[2] , -X[2], Flux_Potential_N); // N with potential -Phi
        comp_flux_potential(Th, Flux[1], U[2] , X[2], Flux_Potential_P);
        comp_flux_potential(Th, Flux[2], U[2] , X[2], Flux_Potential_Phi);
        std::vector<vector<vector<Real>>> Flux_Potential_sc;
        Flux_Potential_sc.resize(3);
        Flux_Potential_sc[0] = Flux_Potential_N;
        Flux_Potential_sc[1] = Flux_Potential_P;
        Flux_Potential_sc[2] = Flux_Potential_Phi;

        std::vector<Real>  Sum_Flux_Potential_N;
        std::vector<Real>  Sum_Flux_Potential_P;
        std::vector<Real>  Sum_Flux_Potential_Phi;
        comp_Sumflux_potential(Th, Somme_Flux[0], -U[2] , -X[2], Sum_Flux_Potential_N); // N with potential -Phi
        comp_Sumflux_potential(Th, Somme_Flux[1], U[2] , X[2], Sum_Flux_Potential_P);
        comp_Sumflux_potential(Th, Somme_Flux[2], U[2] , X[2], Sum_Flux_Potential_Phi);
        std::vector<vector<Real>> Sum_Flux_Potential_sc;
        Sum_Flux_Potential_sc.resize(3);
        Sum_Flux_Potential_sc[0] = Sum_Flux_Potential_N;
        Sum_Flux_Potential_sc[1] = Sum_Flux_Potential_P;
        Sum_Flux_Potential_sc[2] = Sum_Flux_Potential_Phi;


        SolutionVectorType G1_base = SolutionVectorType::Zero(3*n_vol);
        SolutionVectorType G2_base = SolutionVectorType::Zero(3*n_edge);
        comp_G_sc(Th, pas_temps ,  C, isDir , X_b ,  isNeu ,  G_Neu,  U_n,
            Recons , Flux , Somme_Flux,  Flux_Potential_sc , Sum_Flux_Potential_sc,
            U , X , G1_base , G2_base );

        std::vector<TripletType> Triplets_invJ1;
        Triplets_invJ1.reserve(9*n_vol);
        std::vector<TripletType> Triplets_J2;
        Triplets_J2.reserve(5*n_vol * n_edge_max);
        std::vector<TripletType> Triplets_J3;
        Triplets_J3.reserve(5*n_vol * n_edge_max);
        std::vector<TripletType> Triplets_J4;
        Triplets_J4.reserve(5*n_vol* n_edge_max * n_edge_max);

        triplets_invJ1andj2_sc(Th, pas_temps , Alpha,  B, Brev, Recons, Somme_Flux, Sum_Flux_Potential_sc , U,  X, Triplets_invJ1, Triplets_J2);
        triplets_J3andJ4_sc( Th, isDir, B, Brev,  A,  Recons, Flux ,  Flux_Potential_sc, U, X, Triplets_J3, Triplets_J4);


        SparseMatrixType invJ1(3*n_vol, 3*n_vol);
        invJ1.setFromTriplets(Triplets_invJ1.begin(), Triplets_invJ1.end());
        SparseMatrixType J2(3*n_vol, 3*n_edge);
        J2.setFromTriplets(Triplets_J2.begin(), Triplets_J2.end());
        SparseMatrixType J3(3*n_edge, 3*n_vol);
        J3.setFromTriplets(Triplets_J3.begin(), Triplets_J3.end());
        SparseMatrixType J4(3*n_edge, 3*n_edge);
        J4.setFromTriplets(Triplets_J4.begin(), Triplets_J4.end());
//
//        std::cout << "invJ1" << endl;
//        std::cout << invJ1 << endl;
//        std::cout << "\n \n \n J2" << endl;
//        std::cout << J2 << endl;
//
//        std::cout << "\n \n \n J3" << endl;
//        std::cout << J3 << endl;
//
//        std::cout << "\n \n \n J4" << endl;
//        std::cout << J4 << endl;
        SparseMatrixType Schur = J4 - J3 * invJ1 * J2;

        for(int nb_dir = 0; nb_dir < 5; nb_dir++){
            cout << "Direction numero "<< nb_dir +1 << endl;
            cout << "   " << endl;
            SolutionSCType X_v;
            X_v[0] = (SolutionVectorType::Random(n_edge)).cwiseAbs();
            X_v[1] = (SolutionVectorType::Random(n_edge)).cwiseAbs();

           // X_v[0] = proj_UKN_edge(Th, std::pow(1.,-10),SolutionVectorType::Random(n_edge));
           // X_v[1] = proj_UKN_edge(Th, std::pow(1.,-10),SolutionVectorType::Random(n_edge));

            X_v[2] = SolutionVectorType::Random(n_edge);

            SolutionVectorType V_edge = SolutionVectorType::Zero(3*n_edge);
            for(int i =0; i<3; i++){
                for(int iF =0; iF< n_edge; iF ++){
                    V_edge(i*n_edge + iF) = X_v[i](iF);
                }
            }

            SolutionVectorType V_vol = - invJ1 *  J2 * V_edge;
            SolutionSCType U_v = SolVec_to_SC_vol(Th, V_vol);

            SolutionVectorType G_J_2 = Schur * V_edge;
            for(int nb_h = 0; nb_h < 8; nb_h ++){
                Real h = 1. * std::pow(10.,  -nb_h);

                SolutionSCType U_dir;
                SolutionSCType X_dir;
                for(int i =0; i<3; i++){
                    U_dir[i] = U[i] + h *U_v[i];
                    X_dir[i] = X[i] + h *X_v[i];
                }

                std::vector<vector<Real>>  Flux_Potential_N_dir;
                std::vector<vector<Real>>  Flux_Potential_P_dir;
                std::vector<vector<Real>>  Flux_Potential_Phi_dir;
                comp_flux_potential(Th, Flux[0], -U_dir[2] , -X_dir[2], Flux_Potential_N_dir); // N with potential -Phi
                comp_flux_potential(Th, Flux[1], U_dir[2] , X_dir[2], Flux_Potential_P_dir);
                comp_flux_potential(Th, Flux[2], U_dir[2] , X_dir[2], Flux_Potential_Phi_dir);
                std::vector<vector<vector<Real>>> Flux_Potential_sc_dir;
                Flux_Potential_sc_dir.resize(3);
                Flux_Potential_sc_dir[0] = Flux_Potential_N_dir;
                Flux_Potential_sc_dir[1] = Flux_Potential_P_dir;
                Flux_Potential_sc_dir[2] = Flux_Potential_Phi_dir;

                std::vector<Real>  Sum_Flux_Potential_N_dir;
                std::vector<Real>  Sum_Flux_Potential_P_dir;
                std::vector<Real>  Sum_Flux_Potential_Phi_dir;
                comp_Sumflux_potential(Th, Somme_Flux[0], -U_dir[2] , -X_dir[2], Sum_Flux_Potential_N_dir); // N with potential -Phi
                comp_Sumflux_potential(Th, Somme_Flux[1], U_dir[2] , X_dir[2], Sum_Flux_Potential_P_dir);
                comp_Sumflux_potential(Th, Somme_Flux[2], U_dir[2] , X_dir[2], Sum_Flux_Potential_Phi_dir);
                std::vector<vector<Real>> Sum_Flux_Potential_sc_dir;
                Sum_Flux_Potential_sc_dir.resize(3);
                Sum_Flux_Potential_sc_dir[0] = Sum_Flux_Potential_N_dir;
                Sum_Flux_Potential_sc_dir[1] = Sum_Flux_Potential_P_dir;
                Sum_Flux_Potential_sc_dir[2] = Sum_Flux_Potential_Phi_dir;

                SolutionVectorType G1_dir = SolutionVectorType::Zero(3*n_vol);
                SolutionVectorType G2_dir = SolutionVectorType::Zero(3*n_edge);

                comp_G_sc(Th, pas_temps ,  C, isDir , X_b ,  isNeu ,  G_Neu,  U_n,
                    Recons , Flux , Somme_Flux,  Flux_Potential_sc_dir , Sum_Flux_Potential_sc_dir,
                    U_dir , X_dir , G1_dir , G2_dir );


                SolutionVectorType Err_vol = G1_dir - G1_base  ;
                SolutionVectorType Err_edge = G2_dir - G2_base - h * G_J_2;
                Real Norm_Err = std::max(Err_vol.lpNorm<Eigen::Infinity>() , Err_edge.lpNorm<Eigen::Infinity>());

                SolutionSCType E_vol_sc = SolVec_to_SC_vol(Th,  Err_vol);
                SolutionSCType E_edge_sc = SolVec_to_SC_edge(Th, Err_edge);

                vector<Real> Norm_Er;
                Norm_Er.resize(3);

                for(int i =0; i< 3; i++){
                    Norm_Er[i] = std::max(E_vol_sc[i].lpNorm<Eigen::Infinity>() , E_edge_sc[i].lpNorm<Eigen::Infinity>() );
                    //Norm_Er[i] = E_vol_sc[i].lpNorm<1>() ;
                }
//                std::cout << FORMAT(50) << "Er_N / h^2  :"  <<  Norm_Er[0]/(h*h)  << std::endl;
//                std::cout << FORMAT(50) << "Er_N_vol / h^2  :"  << E_vol_sc[0].lpNorm<Eigen::Infinity>()/(h*h)  << std::endl;
//                std::cout << FORMAT(50) << "Er_N_edge / h^2  :"  << E_edge_sc[0].lpNorm<Eigen::Infinity>()/(h*h)  << std::endl;
//
//
//                std::cout << FORMAT(50) << "Er_P / h^2 :"  <<   Norm_Er[1]/(h*h)<< std::endl;
//                std::cout << FORMAT(50) << "Er_P_vol / h^2  :"  << E_vol_sc[1].lpNorm<Eigen::Infinity>()/(h*h)  << std::endl;
//                std::cout << FORMAT(50) << "Er_P_edge / h^2  :"  << E_edge_sc[1].lpNorm<Eigen::Infinity>()/(h*h)  << std::endl;
//                std::cout << FORMAT(50) << "Er_Phi / h^2  :"  <<   Norm_Er[2]/(h*h)<< std::endl;
//                std::cout << FORMAT(50) << "Er_Phi abs  :"  <<   Norm_Er[2]<< std::endl;


                cout << "Pas h  :"<< h  <<", erreur / h^2  :" << Norm_Err/(h*h)   << endl;
            }//for nb_h
            cout << "\n "<< endl;
            cout << "Changement de direction "<< endl;
        } //for nb_dir
        cout << "\n \n"<< endl;
        cout << "Changement de point de base "<< endl;

    } //for nb_base

}

Real comp_elec_en(const Mesh *Th, const std::vector<vector<LocalVectorType>> & Flux, const std::vector<LocalVectorType> & Sum_Flux,
    const SolutionVectorType & Phi_vol ,const SolutionVectorType & Phi_edge)
{
    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();

    Real Energy = 0.;

    for(int iT = 0; iT <n_vol; iT++){
        const Cell & T = Th->cell(iT);
        const int & m = T.numberOfFaces();

        const LocalVectorType & Phi_T = loc_vec(Th,iT,Phi_vol,Phi_edge);
        LocalVectorType F_en = LocalVectorType::Zero(1+m,1);

        for(int iF_loc=0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            F_en = F_en  - Phi_edge(iF) * Flux[iT][iF_loc];
        }
        F_en = F_en + Phi_vol[iT] *Sum_Flux[iT];

        Energy += F_en.dot(Phi_T);
    }
    /*
    for(int iT = 0; iT <n_vol; iT++){
        const Cell & T = Th->cell(iT);
        const int & m = T.numberOfFaces();

        const LocalVectorType & Phi_T = loc_vec(Th,iT,Phi_vol,Phi_edge);

        for(int iF_loc=0; iF_loc < m; iF_loc ++ ){
            int iF = T.faceId(iF_loc);
            Energy +=  (Flux[iT][iF_loc].dot(Phi_T)) * (Phi_vol[iT] - Phi_edge(iF));
        }
    }*/
    //std::cout <<  "Energie elec : " << Energy << std::endl;
    return Energy;
}



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
namespace ho
{

  namespace pho
  {
    template<std::size_t K>
    struct LocalContributions
    {
      typedef ho::HierarchicalScalarBasis2d<K+1> CellBasis;
      typedef ho::HierarchicalScalarBasis1d<K> FaceBasis;

      typedef std::function<typename CellBasis::ValueType(const Point &)> ExactSolutionType;
      typedef std::function<typename CellBasis::ValueType(const Point &)> LoadType;
      typedef std::function<typename CellBasis::ValueType(const Point &)> BCType;
      typedef std::function<Real(const Point &)> DiffusivityType;

      // Initialize and compute local operators
      LocalContributions(const Mesh * Th,
                         const Integer & iT,
                         const LoadType & load,
                         const BCType & bc,
                         const DiffusivityType & nu,
                         const bool & weak_bc = false,
                         const Real & eta = 1.);
      // Interpolate exact solution
      Eigen::VectorXd interpolate(const Mesh * Th, const Integer & iT, const ExactSolutionType & u);
      // Reconstruct local solution
      template<class SolutionVectorType, class IndexVectorType>
      Eigen::VectorXd reconstruct(const Mesh * Th,
                                  const Integer & iT,
                                  const SolutionVectorType & uFh,
                                  const IndexVectorType & idx_T);
      // High-order potential reconstruction
      Eigen::Matrix<Real, ho::HierarchicalScalarBasis2d<K+1>::size, 1>
      potentialReconstruction(const Mesh * Th,
                              const Integer & iT,
                              const Eigen::VectorXd & uh_TF);

      enum {
        nb_cell_dofs       = ho::HierarchicalScalarBasis2d<K>::size,
        nb_local_cell_dofs = ho::HierarchicalScalarBasis2d<K>::size,
        nb_local_face_dofs = ho::HierarchicalScalarBasis1d<K>::size,
        NG                 = HierarchicalScalarBasis2d<K+1>::size - 1
      };

      std::shared_ptr<CellBasis> basisT;
      std::vector<std::shared_ptr<FaceBasis> > basisF;

      Eigen::MatrixXd STF; // Stabilization
      Eigen::MatrixXd JTF; // Penalty for weakly enforced BCs

      Eigen::MatrixXd ATF;
      Eigen::VectorXd bTF;
      // RowMajor format is needed for compatibility with PETSc
      Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> AF;
      Eigen::VectorXd bF;

      Eigen::Matrix<Real, CellBasis::size, CellBasis::size> MTT;
      std::vector<Eigen::Matrix<Real, FaceBasis::size, FaceBasis::size> > MFF;

      Eigen::MatrixXd GT;

      std::size_t nb_face_dofs;
      std::size_t nb_tot_dofs;

      // Basis evaluation at
      std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF;
      std::vector<std::shared_ptr<BasisGradientEvaluation<CellBasis> > > deval_basisT_PTF;
      std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_F;
      std::vector<std::shared_ptr<BasisGradientEvaluation<CellBasis> > > deval_basisT_F;
      std::vector<std::shared_ptr<BasisFunctionEvaluation<FaceBasis> > > feval_basisF;

      std::vector<std::shared_ptr<PyramidIntegrator> > pims;
      std::vector<std::shared_ptr<FaceIntegrator> > fims;
    };

    //------------------------------------------------------------------------------
    // Implementation

    template<std::size_t K>
    LocalContributions<K>::LocalContributions(const Mesh * Th,
                                              const Integer & iT,
                                              const LoadType & load,
                                              const BCType & bc,
                                              const DiffusivityType & nu,
                                              const bool & weak_bc,
                                              const Real & eta)
    {
      const Cell & T = Th->cell(iT);
      const Point & xT = T.center();

      // Estimate hT
      Real hT = cell_diameter(Th, iT);

      Real nuT = nu(xT);

      //------------------------------------------------------------------------------
      // Precompute bases

      basisT.reset(new CellBasis(T.center(), hT));
      basisF.resize(T.numberOfFaces());
      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const Face & F = Th->face(T.faceId(iF_loc));
        const Point & xF = F.barycenter();
        const Real & hF = F.measure();
        basisF[iF_loc].reset(new FaceBasis(F.point(0).first, xF, hF));
      }

      // Evaluate bases at quadrature nodes
      feval_basisT_PTF.resize(T.numberOfFaces());
      deval_basisT_PTF.resize(T.numberOfFaces());
      feval_basisT_F.resize(T.numberOfFaces());
      deval_basisT_F.resize(T.numberOfFaces());
      feval_basisF.resize(T.numberOfFaces());

      pims.resize(T.numberOfFaces());
      fims.resize(T.numberOfFaces());

      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        Integer iF = T.faceId(iF_loc);

        pims[iF_loc].reset(new PyramidIntegrator(Th, iT, iF_loc, 2*K+2));
        const PyramidIntegrator & pim = *pims[iF_loc];

        feval_basisT_PTF[iF_loc].reset(new BasisFunctionEvaluation<CellBasis>(basisT.get(), pim.points()));
        deval_basisT_PTF[iF_loc].reset(new BasisGradientEvaluation<CellBasis>(basisT.get(), pim.points()));

        fims[iF_loc].reset(new FaceIntegrator(Th, iF, 2*K+2));
        const FaceIntegrator & fim = *fims[iF_loc];
        feval_basisT_F[iF_loc].reset(new BasisFunctionEvaluation<CellBasis>(basisT.get(), fim.points()));
        deval_basisT_F[iF_loc].reset(new BasisGradientEvaluation<CellBasis>(basisT.get(), fim.points(), 1));
        feval_basisF[iF_loc].reset(new BasisFunctionEvaluation<FaceBasis>(basisF[iF_loc].get(), fim.points()));
      } // for iF_loc

      //------------------------------------------------------------------------------
      // Count local unknowns

      nb_face_dofs = T.numberOfFaces() * FaceBasis::size;
      nb_tot_dofs = nb_cell_dofs + nb_face_dofs;

      bTF = Eigen::VectorXd::Zero(nb_tot_dofs);

      //------------------------------------------------------------------------------
      // Gradient reconstruction

      Eigen::MatrixXd MG = Eigen::MatrixXd::Zero(NG, NG);
      Eigen::MatrixXd BG = Eigen::MatrixXd::Zero(NG, nb_tot_dofs);
      Eigen::VectorXd bG = Eigen::VectorXd::Zero(NG);

      MTT = Eigen::Matrix<Real, CellBasis::size, CellBasis::size>::Zero();
      MFF.resize(T.numberOfFaces());
      std::vector<Eigen::Matrix<Real, FaceBasis::size, CellBasis::size> > MFT(T.numberOfFaces());
      std::vector<Eigen::Matrix<Real, FaceBasis::size, NG> > NFT(T.numberOfFaces());

      for(int iF_loc = 0 ; iF_loc < T.numberOfFaces(); iF_loc++) {
        int iF = T.faceId(iF_loc);
        const Face & F = Th->face(iF);
        Eigen::Matrix<Real, DIM, 1> nTF;
        { Point _nTF = F.normal(xT); nTF << _nTF(0), _nTF(1); }
        const Real & hF = F.measure();

        const PyramidIntegrator & pim = *pims[iF_loc];
        const FaceIntegrator & fim = *fims[iF_loc];

        //------------------------------------------------------------------------------
        // Initialize matrices

        MFT[iF_loc] = Eigen::Matrix<Real, FaceBasis::size, CellBasis::size>::Zero();
        MFF[iF_loc] = Eigen::Matrix<Real, FaceBasis::size, FaceBasis::size>::Zero();
        NFT[iF_loc] = Eigen::Matrix<Real, FaceBasis::size, NG>::Zero();

        //------------------------------------------------------------------------------
        // Volumetric terms

        for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
          const Point & xQN = pim.point(iQN);
          const Real & wQN = pim.weight(iQN);

          for(std::size_t i = 0; i < NG; i++) {
            const typename CellBasis::GradientType & dphi_i_iqn = (*deval_basisT_PTF[iF_loc])(basisT->degreeIndex(1) + i, iQN);

            // LHS
            for(std::size_t j = 0; j < NG; j++) {
              const typename CellBasis::GradientType & dphi_j_iqn = (*deval_basisT_PTF[iF_loc])(basisT->degreeIndex(1) + j, iQN);
              MG(i,j) += wQN * dphi_i_iqn.dot(dphi_j_iqn);
            } // for j

            // RHS (\GRAD vT, \GRAD w)_{PTF}
            for(std::size_t j = 0; j < nb_cell_dofs; j++) {
              const typename CellBasis::GradientType & dphi_j_iqn = (*deval_basisT_PTF[iF_loc])(j, iQN);
              BG(i,j) += wQN * dphi_i_iqn.dot(dphi_j_iqn);
            } // for j
          } // for i

          // MTT
          for(std::size_t i = 0; i < CellBasis::size; i++) {
            const Real & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
            for(std::size_t j = 0; j < CellBasis::size; j++) {
              const Real & phi_j_iqn = (*feval_basisT_PTF[iF_loc])(j, iQN);
              MTT(i,j) += wQN * phi_i_iqn * phi_j_iqn;
            } // for j
          } // for i

          // Forcing term
          for(std::size_t i = 0; i < nb_cell_dofs; i++) {
            const Real & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
            bTF(i) += wQN * phi_i_iqn * load(xQN);
          } // for i
        } // for iQN

        //------------------------------------------------------------------------------
        // Interface terms

        for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
          const Point & xQN = fim.point(iQN);
          const Real & wQN = fim.weight(iQN);

          // Offset for face unknowns
          std::size_t offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;

          for(std::size_t i = 0; i < NG; i++) {
            const typename CellBasis::GradientType & dphi_i_iqn = (*deval_basisT_F[iF_loc])(i, iQN);
            Real dphi_i_n_iqn = dphi_i_iqn.dot(nTF);

            // RHS (v_F, \GRAD w\SCAL n_{TF})_F
            for(std::size_t j = 0; j < FaceBasis::size; j++) {
              const typename FaceBasis::ValueType & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
              BG(i,offset_F + j) += wQN * dphi_i_n_iqn * phi_j_iqn;
            } // for j

            // RHS -(v_T, \GRAD w\SCAL n_{TF})_F
            for(std::size_t j = 0; j < nb_cell_dofs; j++) {
              const typename CellBasis::ValueType & phi_j_iqn = (*feval_basisT_F[iF_loc])(j, iQN);
              BG(i,j) -= wQN * dphi_i_n_iqn * phi_j_iqn;
            } // for j
          } // for i

          // MFT and NFT
          for(std::size_t i = 0; i < FaceBasis::size; i++) {
            const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);

            if(weak_bc && F.isBoundary()) {
              bTF(offset_F + i) += wQN * nuT * (eta / hF) * phi_i_iqn * bc(xQN);
            } // if

            for(std::size_t j = 0; j < CellBasis::size; j++) {
              const typename CellBasis::ValueType & phi_j_iqn = (*feval_basisT_F[iF_loc])(j, iQN);
              MFT[iF_loc](i,j) += wQN * phi_i_iqn * phi_j_iqn;
            } // for j

            for(std::size_t j = 0; j < NG; j++) {
              const typename CellBasis::GradientType & dphi_j_iqn = (*deval_basisT_F[iF_loc])(j, iQN);
              NFT[iF_loc](i,j) += wQN * phi_i_iqn * dphi_j_iqn.dot(nTF);
            } // for j
          } // for i

          // MFF
          for(std::size_t i = 0; i < FaceBasis::size; i++) {
            const auto & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
            for(std::size_t j = 0; j < FaceBasis::size; j++) {
              const auto & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
              MFF[iF_loc](i,j) += wQN * phi_i_iqn * phi_j_iqn;
            } // for j
          } // for i
        } // for iQN
      } // for iF_loc

      //------------------------------------------------------------------------------
      // Consistent terms

      GT = MG.ldlt().solve(BG);

      ATF = nuT * BG.transpose() * GT;

      //------------------------------------------------------------------------------
      // Stabilization

      STF = Eigen::MatrixXd::Zero(nb_tot_dofs, nb_tot_dofs);
      Eigen::MatrixXd piTK_rTK;
      Eigen::VectorXd rhs_piTK_rTK;
      {
        Eigen::LDLT<Eigen::MatrixXd> piKT;
        piKT.compute(MTT.topLeftCorner(basisT->degreeIndex(K+1), basisT->degreeIndex(K+1)));

        piTK_rTK = piKT.solve(MTT.block(0, basisT->degreeIndex(1), basisT->degreeIndex(K+1), NG) * GT);
      }

      // Compute face residual
      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        int iF = T.faceId(iF_loc);
	const Face & F = Th->face(iF);
	const Real & hF = F.measure();

        Eigen::MatrixXd piF_rTK_minus_uF;
        Eigen::MatrixXd piF_uT_minus_piTK_rTK;
        {
          Eigen::LDLT<Eigen::MatrixXd> piKF;
          piKF.compute(MFF[iF_loc]);

          piF_rTK_minus_uF = piKF.solve(MFT[iF_loc].block(0, basisT->degreeIndex(1), FaceBasis::size, NG) * GT);
          piF_rTK_minus_uF.block<FaceBasis::size, FaceBasis::size>(0, nb_cell_dofs + iF_loc * FaceBasis::size)
            -=  Eigen::Matrix<Real, FaceBasis::size, FaceBasis::size>::Identity();

          Eigen::MatrixXd uT_minus_piTK_rTK = -piTK_rTK;
          uT_minus_piTK_rTK.topLeftCorner(nb_cell_dofs, nb_cell_dofs) += Eigen::MatrixXd::Identity(nb_cell_dofs, nb_cell_dofs);
          piF_uT_minus_piTK_rTK =  piKF.solve(MFT[iF_loc].topLeftCorner(FaceBasis::size, basisT->degreeIndex(K+1)) * uT_minus_piTK_rTK);
        }

        auto BRF = piF_uT_minus_piTK_rTK + piF_rTK_minus_uF;

        STF += (nuT / hF) * BRF.transpose() * MFF[iF_loc] * BRF;
      } // for iF_loc

      //------------------------------------------------------------------------------
      // Weakly enforced bc

      if(weak_bc) {
        for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
          int iF = T.faceId(iF_loc);
          const Face & F = Th->face(iF);
          const Real & hF = F.measure();

          std::size_t offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;

          if(F.isBoundary()) {
            ATF.middleRows<FaceBasis::size>(offset_F) -= nuT * NFT[iF_loc] * GT;
            STF.block<FaceBasis::size, FaceBasis::size>(offset_F, offset_F) += (nuT * eta / hF) * MFF[iF_loc];
          } // if
        } // for
      } // if

      ATF += STF;

      //------------------------------------------------------------------------------
      // Static condensation

      Eigen::LDLT<Eigen::MatrixXd> LUATT;
      LUATT.compute(ATF.topLeftCorner(nb_cell_dofs, nb_cell_dofs));

      AF = ATF.bottomRightCorner(nb_face_dofs, nb_face_dofs)
        - ATF.bottomLeftCorner(nb_face_dofs, nb_cell_dofs) * LUATT.solve(ATF.topRightCorner(nb_cell_dofs, nb_face_dofs));

      bF = bTF.tail(nb_face_dofs) - ATF.bottomLeftCorner(nb_face_dofs, nb_cell_dofs) * LUATT.solve(bTF.head(nb_cell_dofs));

    } // LocalContributions

    //------------------------------------------------------------------------------

    template<std::size_t K>
    Eigen::VectorXd LocalContributions<K>::interpolate(const Mesh * Th,
                                                       const Integer & iT,
                                                       const ExactSolutionType & u)
    {
      const Cell & T = Th->cell(iT);

      Eigen::VectorXd uTF = Eigen::VectorXd::Zero(nb_cell_dofs + T.numberOfFaces() * FaceBasis::size);
      Eigen::VectorXd bT = Eigen::VectorXd::Zero(nb_cell_dofs);

      for(int iF_loc = 0; iF_loc < T.numberOfFaces(); iF_loc++) {
        const PyramidIntegrator & pim = *pims[iF_loc];

        for(std::size_t i = 0; i < nb_cell_dofs; i++) {
          for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
            const Point & xQN = pim.point(iQN);
            const Real & wQN = pim.weight(iQN);

            const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc]) (i, iQN);

            bT(i) += wQN * phi_i_iqn * u(xQN);
          } // for iQN
        } // for i

        const FaceIntegrator & fim = *fims[iF_loc];

        Eigen::VectorXd bF = Eigen::VectorXd::Zero(FaceBasis::size);

        for(std::size_t i = 0; i < FaceBasis::size; i++) {
          for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
            const Point & xQN = fim.point(iQN);
            const Real & wQN = fim.weight(iQN);

            const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc]) (i, iQN);

            bF(i) += wQN * phi_i_iqn * u(xQN);
          } // for iQN
        } // for i

        std::size_t offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;
        uTF.segment<FaceBasis::size>(offset_F) = MFF[iF_loc].llt().solve(bF);
      } // for iF_loc

      Eigen::LDLT<Eigen::MatrixXd> LUMT;
      LUMT.compute(MTT.topLeftCorner(nb_cell_dofs, nb_cell_dofs));
      uTF.head(nb_cell_dofs) = LUMT.solve(bT);

      return uTF;
    } // interpolate

    //------------------------------------------------------------------------------

    template<std::size_t K>
    template<class SolutionVectorType, class IndexVectorType>
    Eigen::VectorXd LocalContributions<K>::reconstruct(const Mesh * Th,
                                                       const Integer & iT,
                                                       const SolutionVectorType & uFh,
                                                       const IndexVectorType & idx_T)
    {
      Eigen::VectorXd uTF = Eigen::VectorXd::Zero(nb_tot_dofs);
      for(int i = 0; i < idx_T.size(); i++) { uTF(nb_cell_dofs + i) = uFh(idx_T(i)); }
      uTF.head(nb_cell_dofs) =
        ATF.topLeftCorner(nb_cell_dofs, nb_cell_dofs).ldlt().solve(
                                                                   bTF.head(nb_cell_dofs)
                                                                   - ATF.topRightCorner(nb_cell_dofs, nb_face_dofs) * uTF.tail(nb_face_dofs)
                                                                   );
      return uTF;
    } // reconstruct

//------------------------------------------------------------------------------

    template<std::size_t K>
    Eigen::Matrix<Real, ho::HierarchicalScalarBasis2d<K+1>::size, 1>
    LocalContributions<K>::potentialReconstruction(const Mesh * Th,
                                                   const Integer & iT,
                                                   const Eigen::VectorXd & uh_TF)
    {
      static const std::size_t NU = ho::HierarchicalScalarBasis2d<K+1>::size;
      typedef Eigen::Matrix<Real, NU, 1> ResultType;

      ResultType puT = ResultType::Zero();
      puT.tail(NG) = GT * uh_TF;
      Eigen::Matrix<Real, nb_cell_dofs, 1> uT_minus_piKpiuT =
        uh_TF.head<nb_cell_dofs>() - MTT.topLeftCorner(nb_cell_dofs, nb_cell_dofs).ldlt().solve(MTT.topLeftCorner(nb_cell_dofs, NU) * puT);

      puT.head(nb_cell_dofs) += uT_minus_piKpiuT;

      return puT;
    }

  } // namespace pho

} // namespace ho

#endif
