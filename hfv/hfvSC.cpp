#include "hfvSC.h"


#include <Common/GetPot>

#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include "Common/chrono.hpp"

#include "scSolution.h"
#include "postProcessing.h"

#include "Eigen/SparseCore"
#include "Eigen/SparseLU"
#include "Eigen/Core"

#include <unsupported/Eigen/SparseExtra>

#include <vector>

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>

#ifndef Eta
#define Eta 1.5
#endif

#ifndef s_pos
#define s_pos std::pow(10., -9)
#endif

#ifndef s_iter
#define s_iter 50
#endif

#ifndef s_G
#define s_G 5.*std::pow(10.,-9) //-13)
#endif

#ifndef s_R
#define s_R 5.*std::pow(10., -9)
#endif

#ifndef s_R_rel
#define s_R_rel 1.*std::pow(10., -9)
#endif

#ifndef dt_crit
#define dt_crit 1.*std::pow(10., -30)
#endif

#ifndef ratio_dt_max
#define ratio_dt_max 1.
#endif

#ifndef dt_max_ratio_tot
#define dt_max_ratio_tot 100.
#endif

#ifndef ratio_iter_max
#define ratio_iter_max 10
#endif



#ifndef nb_pts_in_graph
#define nb_pts_in_graph 50
#endif

#ifndef nb_pts_in_visu
#define nb_pts_in_visu 100
#endif

#ifndef Diagno
#define Diagno false
#endif




using boost::math::constants::pi;

using namespace ho;

//------------------------------------------------------------------------------

typedef Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> IndexVectorType;
typedef Eigen::SparseMatrix<Real> SparseMatrixType;
typedef Eigen::SparseLU<SparseMatrixType> SolverType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
typedef std::array<SolutionVectorType,3> SolutionSCType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> ComputeFluxType;
typedef Eigen::Triplet<Real> TripletType;
typedef Eigen::Matrix<Real, 2, 2 > TensorType;
typedef std::function<TensorType(const Point &)> DiffusivityType;
typedef std::function<Eigen::Matrix<Real, 2, 1>(const Point &)> FieldType;
typedef std::function<Real(const Point &)> FctContType;
typedef std::function<Real(const Point &)> PotentialType;
typedef std::function<Real(const Point &)> DopingType;

typedef std::function<Real(const Point &)> InitialDataType;
typedef std::function<Real(const Point &)> BoundaryType;
typedef std::function<Real(const Point &)> BoundaryNeumannType;
typedef std::function<bool(const Face &)> DefBoundaryConditionType;

// Mesh reader 
std::shared_ptr<Mesh> Mesh_read(const string & Mesh_file);

//int wversion(const Mesh *Th,const Real T_end, const Real D_t);
int sc_magnet_wv(const Mesh *Th, const Real T_end, const Real D_t, const string & geo_mesh, const bool & Visu);
//int semi_impli(const Mesh *Th,const Real T_end, const Real D_t, const string & geo_mesh);
int test_Jac_equilibrium(const Mesh *Th);
int test_Jac_sc(const Mesh *Th, const Real dt);


//------------------------------------------------------------------------------

int main(){
    string geo_mesh;
    Real T_end;
    Real D_t;
    bool Visu; 
    
    cout << "Drift-difffusion system simulation: nonlinear HFV scheme.\n\n ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~mesh\n" << endl;
    
    cout << "Mesh ? (name with number and type, ex : hexagonal_3.typ1,  mesh3_5.typ1 or PN_2.typ12)" << endl;
    cin >> geo_mesh ;

    cout << "Final time ? " << endl;
    cin >> T_end;

    cout << "Time step ? " << endl;
    cin >> D_t;    
    
    cout << "Do you want to save visualisation files?\n(1 for yes and 0 for no) " << endl;
    cin >> Visu;

    std::string Mesh_base = "../meshes/";
    //std::string Mesh_file = Mesh_base + geo_mesh +".typ12";    
    std::string Mesh_file = Mesh_base + geo_mesh ;


    std::shared_ptr<Mesh> Th = Mesh_read(Mesh_file);
    std::cout << "Mesh OK ! " << std::endl;

    int val;


    //val = semi_impli( Th.get(),  T_end, D_t, geo_mesh);
    val = sc_magnet_wv( Th.get(),  T_end, D_t, geo_mesh, Visu);
    //val = test_Jac_equilibrium(Th.get());
    //val = test_Jac_sc( Th.get(),  D_t );
    std::cout << "Done" << std::endl;
    return 0;
}

std::shared_ptr<Mesh> Mesh_read(const string & Mesh_file){

    std::string sep = "\n----------------------------------------\n";
    Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
    std::cout << "test1 ? "  << std::endl;

    // Read options and data
    //GetPot options(argc, argv);
    std::string mesh_file = Mesh_file;

    // Pretty-print floating point numbers
    std::cout.precision(2);
    std::cout.flags(std::ios_base::scientific);

    std::cout << "test2 ? "  << std::endl;
    // Create mesh
    std::shared_ptr<Mesh> Th(new Mesh());
    if(mesh_file.find("typ12")!=std::string::npos)
        Th->readFVCA5File_typ12(mesh_file.c_str());
    else if(mesh_file.find("typ1")!=std::string::npos)
        Th->readFVCA5File_typ1(mesh_file.c_str());
    else if(mesh_file.find("dgm")!=std::string::npos)
        Th->readDGMFile(mesh_file.c_str());
    else {
        std::cerr << "Unknown mesh format" << std::endl;
        exit(1);
    }

    std::cout << "test3 ? "  << std::endl;

    Th->buildFaceGroups();
    std::cout << "MESH: " << mesh_file << std::endl;
    std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
    std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
    std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells()  << std::endl;
    std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
    std::cout << FORMAT(50) << "num_unknws" << Th->numberOfCells() +  Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;
    std::cout << FORMAT(50) << "num_unknws_with_elimination" << Th->numberOfInternalFaces()  + Th->numberOfBoundaryFaces() << std::endl;

    //------------------------------------------------------------------------------
    // Estimate mesh size
    std::cout<< FORMAT(50) << "Paramètre de stabilisation" << Eta << std::endl;

    Real h = Th->meshsize();
    std::cout << FORMAT(50) << "meshsize" << h << std::endl;

    return Th;
}


//
//
//
//
//

int sc_magnet_wv(const Mesh *Th, const Real T_end, const Real D_t, const string & geo_mesh, const bool & Visu){

    std::cout << FORMAT(50) << " \n Coupled scheme : go !! \n"  <<  std::endl;

    common::chrono chrono_evol;
    chrono_evol.start();

    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_vol_unkw = Th->numberOfCells();
    int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();

    ho::pho::Regular sol;

    std::cout << "Pre-computation for the evolutionary problem" << endl;
    std::cout << "Pre-computation begins" << endl;
    common::chrono c_precomput;
    c_precomput.start();

    std::cout << "Pre-computation of scheme data (Fluxes) : go !" << endl;

    int n_edge_Dir = 0.;
    for(int iF =0; iF < n_edge; iF ++){
        if(sol.isDirichlet(Th->face(iF))){n_edge_Dir += 1;}
    }

    std::vector<TripletType> Triplets_mass;
    Triplets_mass.reserve(n_vol);
    for(int iT=0; iT < n_vol; iT ++){
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        Triplets_mass.push_back(TripletType(iT,iT,m_T));
    } // for iT

    SparseMatrixType M_mass(n_vol, n_vol);
    M_mass.setFromTriplets(Triplets_mass.begin(), Triplets_mass.end());


    std::vector<vector<vector<Real>>> A_N;
    std::vector<vector<vector<Real>>> A_P;
    std::vector<vector<vector<Real>>> A_Phi;
    comp_A_elt(Th, sol.Lambda_N, Eta, A_N);
    comp_A_elt(Th, sol.Lambda_P, Eta, A_P);
    comp_A_elt(Th, sol.Lambda_Phi, Eta, A_Phi);
    std::vector<vector<vector<vector<Real>>>> A_sc;
    A_sc.resize(3);
    A_sc[0] = A_N;
    A_sc[1] = A_P;
    A_sc[2] = A_Phi;


    std::vector<vector<Real>> B_N;
    std::vector<vector<Real>> B_P;
    std::vector<vector<Real>> B_Phi;
    comp_B_elt(Th, A_N, B_N );
    comp_B_elt(Th, A_P, B_P );
    comp_B_elt(Th, A_Phi, B_Phi );
    std::vector<vector<vector<Real>>> B_sc;
    B_sc.resize(3);
    B_sc[0] = B_N;
    B_sc[1] = B_P;
    B_sc[2] = B_Phi;

    std::vector<vector<Real>> Brev_N;
    std::vector<vector<Real>> Brev_P;
    std::vector<vector<Real>> Brev_Phi;
    comp_Brev_elt(Th, A_N, Brev_N );
    comp_Brev_elt(Th, A_P, Brev_P );
    Brev_Phi =B_Phi;
    std::vector<vector<vector<Real>>> Brev_sc;
    Brev_sc.resize(3);
    Brev_sc[0] = Brev_N;
    Brev_sc[1] = Brev_P;
    Brev_sc[2] = Brev_Phi;


    std::vector<Real> Alpha_N;
    std::vector<Real> Alpha_P;
    std::vector<Real> Alpha_Phi;
    comp_Alpha_elt(Th, B_N, Alpha_N );
    comp_Alpha_elt(Th, B_P, Alpha_P );
    comp_Alpha_elt(Th, B_Phi, Alpha_Phi );
    std::vector<vector<Real>> Alpha_sc;
    Alpha_sc.resize(3);
    Alpha_sc[0] = Alpha_N;
    Alpha_sc[1] = Alpha_P;
    Alpha_sc[2] = Alpha_Phi;


    std::vector<vector<LocalVectorType>> Flux_N;
    std::vector<vector<LocalVectorType>> Flux_P;
    std::vector<vector<LocalVectorType>> Flux_Phi;
    precomp_flux(Th, A_N, B_N, Flux_N );
    precomp_flux(Th, A_P, B_P, Flux_P );
    precomp_flux(Th, A_Phi, B_Phi, Flux_Phi );
    std::vector<vector<vector<LocalVectorType>>> Flux_sc;
    Flux_sc.resize(3);
    Flux_sc[0] = Flux_N;
    Flux_sc[1] = Flux_P;
    Flux_sc[2] = Flux_Phi;


    std::vector<LocalVectorType>  Sum_Flux_N;
    std::vector<LocalVectorType>  Sum_Flux_P;
    std::vector<LocalVectorType>  Sum_Flux_Phi;
    precomp_Somme_flux(Th, Flux_N, Sum_Flux_N);
    precomp_Somme_flux(Th, Flux_P, Sum_Flux_P);
    precomp_Somme_flux(Th, Flux_Phi, Sum_Flux_Phi);
    std::vector<vector<LocalVectorType>> Sum_Flux_sc;
    Sum_Flux_sc.resize(3);
    Sum_Flux_sc[0] = Sum_Flux_N;
    Sum_Flux_sc[1] = Sum_Flux_P;
    Sum_Flux_sc[2] = Sum_Flux_Phi;


    std::vector<LocalVectorType>  Reconstruction;
    precomp_reconstruc(Th,Reconstruction);


    cout << "Pre-computation of scheme data (Fluxes) : DONE ! " << endl;


    //Creation condition initiale discrete
    std::cout << "Discretisation of and boundary andinitial data : go !" << endl;

    SolutionVectorType C_pt = SolutionVectorType::Zero(n_vol);
    SolutionVectorType G_Neu = SolutionVectorType::Zero(n_edge);
    SolutionVectorType N_boundary = SolutionVectorType::Zero(n_edge);
    SolutionVectorType P_boundary = SolutionVectorType::Zero(n_edge);
    SolutionVectorType Phi_boundary = SolutionVectorType::Zero(n_edge);

    SolutionVectorType N_0_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType N_0_edge = SolutionVectorType::Zero(n_edge);
    SolutionVectorType P_0_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType P_0_edge = SolutionVectorType::Zero(n_edge);

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        N_0_vol(iT) = sol.N_ini(x_T);
        P_0_vol(iT) = sol.P_ini(x_T);
        C_pt(iT) = sol.C(x_T);
    }//for iT

    for(int iF = 0; iF< nunkw;iF++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        N_0_edge(iF) = sol.N_ini(x_F);
        P_0_edge(iF) = sol.P_ini(x_F);
        if(sol.isNeumann(F)){G_Neu(iF) = (F.measure()) * sol.g_n(x_F);} //if Neumann
        if(sol.isDirichlet(F)){
            N_boundary(iF) = sol.N_b(x_F);
            P_boundary(iF) = sol.P_b(x_F);
            Phi_boundary(iF) = sol.Phi_b(x_F);
            } // if Dirichlet
        }

    // precomputation for the stationary Potential problem
    SparseMatrixType invM1(n_vol, n_vol);
    SparseMatrixType M2(n_vol, n_edge);
    SparseMatrixType M3(n_edge, n_vol);
    SparseMatrixType Resize_Dir(n_edge-n_edge_Dir,n_edge);
    SolutionVectorType Potential_relev_boundary;
    SolutionVectorType  Sec_m_vol;
    SolutionVectorType  Sec_m_edge;
    SolverType solver_potential;

    comp_Potentialsolver(Th, Eta,  sol.Lambda_Phi, sol.C, sol.isDirichlet,  sol.Phi_b, sol.isNeumann, sol.g_n,  Potential_relev_boundary , Resize_Dir, invM1,  M2 ,  M3, Sec_m_vol,Sec_m_edge, solver_potential);
    //Discrete potential at time zero
    SolutionVectorType Phi_0_vol = SolutionVectorType::Zero(n_vol_unkw);
    SolutionVectorType Phi_0_edge = SolutionVectorType::Zero(nunkw);

    comp_Potential(Th, solver_potential ,  N_0_vol , P_0_vol ,
        M_mass, Resize_Dir,  invM1,  M2 , M3, Sec_m_vol  , Sec_m_edge , Potential_relev_boundary, Phi_0_vol ,  Phi_0_edge );


    SolutionSCType U_0;
    U_0[0] = N_0_vol;
    U_0[1] = P_0_vol;
    U_0[2] = Phi_0_vol;

    SolutionSCType X_b;
    X_b[0] = N_boundary;
    X_b[1] = P_boundary;
    X_b[2] = Phi_boundary;


    SolutionSCType X_0 ;
    X_0[0] = N_0_edge;
    X_0[1] = P_0_edge;
    X_0[2] = Phi_0_edge ;

    c_precomput.stop();

    /*
    if(false){ //thermal equibilibrium as initialisation
    std::cout << "Computation of the thermal equilibrium " << std::endl;



    SolutionVectorType R_Phieq_vol = SolutionVectorType::Zero(n_vol_unkw);
    SolutionVectorType R_Phieq_edge = SolutionVectorType::Zero(nunkw);

    SolutionVectorType Phieq_vol = SolutionVectorType::Zero(n_vol_unkw);
    SolutionVectorType Phieq_edge =  SolutionVectorType::Zero(nunkw);

    Phieq_vol = U_0[2];
    Phieq_edge = X_0[2];

    int k_eq = 0;

    bool Newton_eq_can_continue ;
    bool Newton_eq_is_CV  ;
    bool res_are_small ;
    bool too_much_iter ;

    do{ //boucle Newton

                Real Norm_G_eq = comp_sol_iter_newton_eq(Th, sol.alpha_P, sol.alpha_N , sol.C, sol.isDirichlet, Phi_boundary, sol.isNeumann , G_Neu,
                    A_Phi, B_Phi, Alpha_Phi , Flux_Phi ,  Sum_Flux_Phi,
                    Phieq_vol ,    Phieq_edge ,  R_Phieq_vol , R_Phieq_edge );

                k_eq +=1; //compte nombre resolution systemes lineaires


                //Real Norm_R_N_infty = std::max(R_N_vol.lpNorm<Eigen::Infinity>() , R_N_edge.lpNorm<Eigen::Infinity>());
                //Real Norm_R_P_infty = std::max(R_P_vol.lpNorm<Eigen::Infinity>() , R_P_edge.lpNorm<Eigen::Infinity>());

                Phieq_vol= R_Phieq_vol + Phieq_vol;
                Phieq_edge = R_Phieq_edge + Phieq_edge;

                Real Norm_R_Phieq_rel = (R_Phieq_vol.lpNorm<1>() + R_Phieq_edge.lpNorm<1>() ) /(Phieq_vol.lpNorm<1>() + Phieq_edge.lpNorm<1>());

                res_are_small = (Norm_R_Phieq_rel < s_R_rel || Norm_G_eq < s_G );
                //res_are_small = (Norm_R_Phieq_rel < s_R_rel );
                too_much_iter=(k_eq> ratio_iter_max * s_iter);
                //too_much_iter=(k_eq> 4);

                Newton_eq_is_CV = (!too_much_iter) && res_are_small ;
                Newton_eq_can_continue =  (! res_are_small) && (! too_much_iter);



                    //std::cout << "iter Newton OK" << endl;
                //std::cout << FORMAT(50) << "Sol_maille"  << Phieq_vol << std::endl;
                std::cout << FORMAT(50) << "N_G"  << Norm_G_eq << std::endl;
                  //std::cout << FORMAT(50) << "N_G _inf"  << << std::endl;
                    //std::cout << FORMAT(50) << "N_R rel"  <<  Norm_R_rel << std::endl;
                    //std::cout << FORMAT(50) << "N_R_infty"  <<  Norm_R_infty << std::endl;

                std::cout << FORMAT(50) << "R_Phieq_rel "  <<  Norm_R_Phieq_rel << std::endl;
                std::cout << FORMAT(50) << "Iter"  <<  k_eq  << "\n" << std::endl;
                //std::cout << FORMAT(50) << "U "  <<  U_ini  << "\n" << std::endl;



            //}while( (k < s_iter) && (Norm_G > s_G || Norm_R > s_R )) ; //boucle Newton
            }while(Newton_eq_can_continue ) ; //boucle Newton

    if(Newton_eq_is_CV){
        std::cout << FORMAT(50) << "Computation of the thermal equilibrium : DONE \n"  <<  std::endl;
    }
    else{
        std::cout << FORMAT(50) << "Computation of the thermal equilibrium : FAILURE  \n"   << std::endl;
    }

    SolutionVectorType N_eq_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType N_eq_edge = SolutionVectorType::Zero(n_edge);
    SolutionVectorType P_eq_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType P_eq_edge = SolutionVectorType::Zero(n_edge);

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        N_eq_vol(iT) = exp(sol.alpha_N + Phieq_vol(iT));
        P_eq_vol(iT) = exp(sol.alpha_P - Phieq_vol(iT));
    }//for iT

    for(int iF = 0; iF< nunkw;iF++){
        N_eq_edge(iF) = exp(sol.alpha_N + Phieq_edge(iF));
        P_eq_edge(iF) = exp(sol.alpha_P - Phieq_edge(iF));
        }

    SolutionVectorType Pert = 0.1 * (SolutionVectorType::Random(n_vol).cwiseAbs());
    SolutionSCType U_eq;
    SolutionSCType X_eq;
    U_eq[0]=N_eq_vol + Pert;
    U_eq[1]=P_eq_vol;
    U_eq[2]=Phieq_vol;
    X_eq[0]=N_eq_edge;
    X_eq[1]=P_eq_edge;
    X_eq[2]=Phieq_edge;

    U_0 = U_eq ;
    X_0 = X_eq;
    }
    */


    std::vector<SolutionSCType> Sol_vol;
    std::vector<SolutionSCType> Sol_edge;

    std::vector<Real> Temps;
    std::vector<int> Nb_iter_Newton;
    std::vector<int> Nb_resol_cumul;
    std::vector<Real> Min_N;
    std::vector<Real> Min_P; 
    std::vector<Real> Max_N;
    std::vector<Real> Max_P;
    // std::vector<Real> Diff_max_N;
    // std::vector<Real> Diff_max_P;


    Sol_vol.push_back(U_0);
    Sol_edge.push_back(X_0);

    std::cout << "Discretisation of initial data ends" << endl;
    Temps.push_back(0.);
    Nb_iter_Newton.push_back(0.);
    Nb_resol_cumul.push_back(0.);
    
    int Nb_resol_sys_lin =0;

    std::cout << "Pre-computation ends" << endl;
    std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;



    int n_iter = 0;
    //bool iter_is_pos;
    bool bounds;
    bool res_are_small;
    bool too_much_iter;
    bool Newton_is_CV;
    bool Newton_can_continue;

    Real dt = D_t;
    Real t = 0.;

    
    Real min_N_vol = N_0_vol.minCoeff();
    Real min_N_edge = N_0_edge.minCoeff();
    Real min_P_vol = P_0_vol.minCoeff(); 
    Real min_P_edge = P_0_edge.minCoeff();
    Real max_N_vol = N_0_vol.maxCoeff(); 
    Real max_N_edge = N_0_edge.maxCoeff();
    Real max_P_vol = P_0_vol.maxCoeff(); 
    Real max_P_edge = P_0_edge.maxCoeff();

    Min_N.push_back(min(min_N_vol,min_N_edge ));
    Min_P.push_back(min(min_P_vol,min_P_edge ));
    Max_N.push_back(max(max_N_vol,max_N_edge ));
    Max_P.push_back(max(max_P_vol,max_P_edge ));

    while((t < T_end- 0.5*std::pow(10.,-15)) && (dt > dt_crit )){

        //Real max_step = min(T_end - t, T_end / dt_max_ratio_tot);
        Real max_step = min(T_end - t, D_t);
        dt = min(dt, max_step);
        n_iter += 1;

        //std::cout << FORMAT(50) << "Phi vol \n"  <<  Phi_vol  << "\n" << std::endl;
        //std::cout << FORMAT(50) << "Phi edge \n"  <<  Phi_edge  << "\n" << std::endl;

        do{ //boucle newton temps adaptatif
            std::cout << FORMAT(50) << "\n n_iter "<< n_iter << std::endl;
            SolutionVectorType R_vol = SolutionVectorType::Zero(3*n_vol_unkw);
            SolutionVectorType R_edge = SolutionVectorType::Zero(3*nunkw);
            //std::cout << FORMAT(50) << "Sol_maille"<< Sol_maille[n_iter]<< std::endl;

            SolutionSCType U_ini;
            SolutionSCType X_ini;

            U_ini = proj_SC_vol(Th, s_pos, Sol_vol[n_iter-1]);
            X_ini = proj_SC_edge(Th, s_pos, Sol_edge[n_iter-1]);

            /*
            for(int i =0; i< 2; i++){
                U_ini[i] =  proj_UKN_vol(Th, s_pos,  Sol_vol[n_iter-1][i]);
                X_ini[i] =  proj_UKN_edge(Th, s_pos, Sol_edge[n_iter-1][i]);
            }
            //std::cout << FORMAT(50) << "New_iniOk " << endl;

            U_ini[2] =  Sol_vol[n_iter-1][2];
            X_ini[2] =  Sol_edge[n_iter-1][2];

            */

//            SolutionVectorType N_vol_ini = proj_UKN_vol(Th, s_pos,  Sol_N_vol[n_iter-1]);
//            SolutionVectorType N_edge_ini = proj_UKN_edge(Th, s_pos,  Sol_N_edge[n_iter-1]);
//            SolutionVectorType P_vol_ini = proj_UKN_vol(Th, s_pos,  Sol_P_vol[n_iter-1]);
//            SolutionVectorType P_edge_ini = proj_UKN_edge(Th, s_pos,  Sol_P_edge[n_iter-1]);


            int k = 0;
            std::cout << FORMAT(50) << "Newton go ! pas temps " << dt << endl;
            do{ //boucle Newton

//                Real Norm_G_N = comp_sol_iter_newton(Th, dt, sol.isDirichlet, N_boundary , sol.isNeumann , G_Neu ,
//                    Sol_N_vol[n_iter-1] , Sol_N_edge[n_iter-1],
//                    A_N, B_N, Alpha_N, Reconstruction, Flux_N, Flux_Phi_N, Sum_Flux_N, Sum_Flux_Phi_N,
//                    N_vol_ini , N_edge_ini, R_N_vol , R_N_edge );
//
//                Real Norm_G_P = comp_sol_iter_newton(Th, dt,sol.isDirichlet, P_boundary , sol.isNeumann , G_Neu ,
//                    Sol_P_vol[n_iter-1] , Sol_P_edge[n_iter-1],
//                    A_P, B_P, Alpha_P, Reconstruction, Flux_P, Flux_Phi_P, Sum_Flux_P, Sum_Flux_Phi_P,
//                    P_vol_ini , P_edge_ini, R_P_vol , R_P_edge );

                Real Norme_G = comp_sol_iter_newton_sc(Th, dt,  C_pt, sol.isDirichlet , X_b , sol.isNeumann , G_Neu,
                    Sol_vol[n_iter-1] , Sol_edge[n_iter-1],
                    A_sc, B_sc , Brev_sc, Alpha_sc, Reconstruction,  Flux_sc ,  Sum_Flux_sc,
                    U_ini , X_ini,  R_vol , R_edge );

                Nb_resol_sys_lin +=1; //compte nombre resolution systemes lineaires


                //Real Norm_R_N_infty = std::max(R_N_vol.lpNorm<Eigen::Infinity>() , R_N_edge.lpNorm<Eigen::Infinity>());
                //Real Norm_R_P_infty = std::max(R_P_vol.lpNorm<Eigen::Infinity>() , R_P_edge.lpNorm<Eigen::Infinity>());

                SolutionSCType R_vol_sc = SolVec_to_SC_vol(Th,  R_vol);
                SolutionSCType R_edge_sc = SolVec_to_SC_edge(Th, R_edge);


                vector<Real> Norm_R_rel;
                Norm_R_rel.resize(3);

                for(int i =0; i< 3; i++){
                    U_ini[i] =  R_vol_sc[i] + U_ini[i];
                    X_ini[i] =  R_edge_sc[i] + X_ini[i];
                    Norm_R_rel[i] = (R_vol_sc[i].lpNorm<1>() + R_edge_sc[i].lpNorm<1>() ) /(U_ini[i].lpNorm<1>() + X_ini[i].lpNorm<1>());
                }
                if(Diagno){
                    std::vector<string> Species;
                    Species.resize(3);
                    Species[0] ="N";
                    Species[1] ="P";
                    Species[2] ="Phi";

                    for(int i = 0; i < 3; i++){
                        string name = "output/impli_" + Species[i];
                        const char *name_char = name.c_str();
                        std::vector<Eigen::VectorXd> coeffs_u(Th->numberOfCells());
                        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
                            Eigen::VectorXd uh_F (1);
                            uh_F(0) = U_ini[i](iT);
                            coeffs_u[iT] = uh_F;
                        }

                        char str[20] = {0};
                        std::sprintf(str, "%d", Nb_resol_sys_lin);
                        char name_c[35];
                        strcpy(name_c, name_char);
                        strcat(name_c, "_diagno_" );
                        strcat(name_c, str);
                        strcat(name_c,".vtu");
                        postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(name_c, Th, coeffs_u);

                    }//for i

                }//if Diagno
                //Real Norm_R_N_rel = (R_N_vol.lpNorm<1>() + R_N_edge.lpNorm<1>() ) /(N_vol_ini.lpNorm<1>() + N_edge_ini.lpNorm<1>());
                //Real Norm_R_P_rel = (R_P_vol.lpNorm<1>() + R_P_edge.lpNorm<1>() ) /(P_vol_ini.lpNorm<1>() + P_edge_ini.lpNorm<1>());
                for(int i =0; i< 2; i++){
                    //X_ini[i] =  proj_UKN_edge(Th, 1.*std::pow(10., -13), X_ini[i]);
                    //U_ini[i] =  proj_UKN_vol(Th, 1.*std::pow(10., -13), U_ini[i]);
                }

                Real min_N_iter = min( U_ini[0].minCoeff() , X_ini[0].minCoeff());
                Real min_P_iter = min( U_ini[1].minCoeff() , X_ini[1].minCoeff());
                Real min_iter =  min(min_P_iter, min_N_iter);

                //iter_is_pos = (min_iter > 0.);
                bounds = Bounds_newton(U_ini, X_ini);

                //res_are_small = (Norm_R_infty < s_R && Norm_G < s_G && Norm_R_rel < s_R_rel );
                res_are_small = (Norm_R_rel[0] < s_R_rel && Norm_R_rel[1] < s_R_rel && Norm_R_rel[2] < s_R_rel ) || (Norme_G < s_G );
                k += 1;
                too_much_iter=(k>s_iter);

                Newton_is_CV = (!too_much_iter) && res_are_small && bounds;
                Newton_can_continue = bounds && (! res_are_small) && (! too_much_iter);



                    //std::cout << "iter Newton OK" << endl;
                  //std::cout << FORMAT(50) << "Sol_maille"  << U_ini<< std::endl;
                std::cout << FORMAT(50) << "N_G"  << Norme_G<< std::endl;
                  //std::cout << FORMAT(50) << "N_G _inf"  << << std::endl;
                    //std::cout << FORMAT(50) << "N_R rel"  <<  Norm_R_rel << std::endl;
                    //std::cout << FORMAT(50) << "N_R_infty"  <<  Norm_R_infty << std::endl;

                std::cout << FORMAT(50) << "R_N_rel "  <<  Norm_R_rel[0] << std::endl;
                std::cout << FORMAT(50) << "R_P_rel "  <<  Norm_R_rel[1] << std::endl;
                std::cout << FORMAT(50) << "R_Phi_rel "  <<  Norm_R_rel[2] << std::endl;

                std::cout << FORMAT(50) << "Min_iteration_N"  <<  min_N_iter << std::endl;
                std::cout << FORMAT(50) << "Min_edge_N"  << X_ini[0].minCoeff()<< std::endl;
                std::cout << FORMAT(50) << "Min_iteration_P"  <<  min_P_iter << std::endl;
                std::cout << FORMAT(50) << "Min_edge_P"  << X_ini[1].minCoeff()<< std::endl;
                std::cout << FORMAT(50) << "Iter"  <<  k  << "\n" << std::endl;
                //std::cout << FORMAT(50) << "U "  <<  U_ini  << "\n" << std::endl;



            //}while( (k < s_iter) && (Norm_G > s_G || Norm_R > s_R )) ; //boucle Newton
            }while(Newton_can_continue ) ; //boucle Newton

            std::cout << FORMAT(50) << "Sortie iteration Newton" << std::endl;
            //Newton_CV = (k == s_iter);
            std::cout << FORMAT(50) << "Valeur Newton_CV" << Newton_is_CV << std::endl;

            if(Newton_is_CV){
                t += dt;
                Temps.push_back(t);
                std::cout << FORMAT(50) << "Calcul sol au temps" << Temps.back() << "OK"<< std::endl;
                dt = dt * (1. + 0.4);

                Nb_iter_Newton.push_back(k);
                Nb_resol_cumul.push_back(Nb_resol_sys_lin);
                Sol_vol.push_back(U_ini);
                Sol_edge.push_back(X_ini);
                
                min_N_vol = min( min_N_vol,  U_ini[0].minCoeff());
                min_N_edge = min( min_N_edge , X_ini[0].minCoeff());
                min_P_vol = min( min_P_vol,  U_ini[1].minCoeff()); 
                min_P_edge =  min(min_P_edge , X_ini[1].minCoeff() ) ;
                max_N_vol = max( max_N_vol, U_ini[0].maxCoeff());
                max_N_edge = max(max_N_edge , X_ini[0].maxCoeff());
                max_P_vol = max( max_P_vol,  U_ini[1].maxCoeff());
                max_P_edge = max( max_P_edge, X_ini[1].maxCoeff());

                Min_N.push_back(min(U_ini[0].minCoeff(),X_ini[0].minCoeff()));
                Min_P.push_back(min(U_ini[1].minCoeff(),X_ini[1].minCoeff()));
                Max_N.push_back(max(U_ini[0].maxCoeff(),X_ini[0].maxCoeff()));
                Max_P.push_back(max(U_ini[1].maxCoeff(),X_ini[1].maxCoeff()));

            } //if Newton CV

           // else{Mod_pastemps += 1;}
            else{dt = 0.5 * dt;}

        }while(!Newton_is_CV && (dt > dt_crit )); // boucle newton avec pas temps adaptatif

        std::cout << "temps +1 " << std::endl;

        cout << FORMAT(50) << "Temps" << t << endl;
        cout << FORMAT(50) << "n_iter" << n_iter << endl;
    } //boucle temps

    if( (dt< dt_crit) && (t < T_end- 0.5*std::pow(10.,-15) ) ){
        std::cout << " \n \n "<< std::endl;
        std::cout << " !!!!!!!!!!!!!!!!!!! !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!! " << std::endl;
        std::cout << "Current time step : " << dt << std::endl;
        std::cout << "Resolution failure ! " << std::endl;
        return 0;
    } // if effective time step becomes too small


    std::cout << "Fin du calcul de la solution  du problème évolutif" << std::endl;

    std::cout << "Nombre d'itérations en temps "  << n_iter << "\n" <<  std::endl;
    SolutionSCType U_end = Sol_vol[n_iter-1];
    SolutionSCType X_end = Sol_edge[n_iter-1];
    chrono_evol.stop();


     //thermal equibilibrium
    std::cout << "Computation of the thermal equilibrium " << std::endl;



    SolutionVectorType R_Phieq_vol = SolutionVectorType::Zero(n_vol_unkw);
    SolutionVectorType R_Phieq_edge = SolutionVectorType::Zero(nunkw);

    SolutionVectorType Phieq_vol = SolutionVectorType::Zero(n_vol_unkw);
    SolutionVectorType Phieq_edge =  SolutionVectorType::Zero(nunkw);

    Phieq_vol = U_end[2];
    Phieq_edge = X_end[2];

    int k_eq = 0;

    bool Newton_eq_can_continue ;
    bool Newton_eq_is_CV  ;

    do{ //boucle Newton

                Real Norm_G_eq = comp_sol_iter_newton_eq(Th, sol.alpha_P, sol.alpha_N , sol.C, sol.isDirichlet, Phi_boundary, sol.isNeumann , G_Neu,
                    A_Phi, B_Phi, Alpha_Phi , Flux_Phi ,  Sum_Flux_Phi,
                    Phieq_vol ,    Phieq_edge ,  R_Phieq_vol , R_Phieq_edge );

                k_eq +=1; //compte nombre resolution systemes lineaires


                //Real Norm_R_N_infty = std::max(R_N_vol.lpNorm<Eigen::Infinity>() , R_N_edge.lpNorm<Eigen::Infinity>());
                //Real Norm_R_P_infty = std::max(R_P_vol.lpNorm<Eigen::Infinity>() , R_P_edge.lpNorm<Eigen::Infinity>());

                Phieq_vol= R_Phieq_vol + Phieq_vol;
                Phieq_edge = R_Phieq_edge + Phieq_edge;

                Real Norm_R_Phieq_rel = (R_Phieq_vol.lpNorm<1>() + R_Phieq_edge.lpNorm<1>() ) /(Phieq_vol.lpNorm<1>() + Phieq_edge.lpNorm<1>());

                res_are_small = (Norm_R_Phieq_rel < s_R_rel || Norm_G_eq < s_G );
                //res_are_small = (Norm_R_Phieq_rel < s_R_rel );
                too_much_iter=(k_eq> ratio_iter_max * s_iter);
                //too_much_iter=(k_eq> 4);

                Newton_eq_is_CV = (!too_much_iter) && res_are_small ;
                Newton_eq_can_continue =  (! res_are_small) && (! too_much_iter);



                    //std::cout << "iter Newton OK" << endl;
                //std::cout << FORMAT(50) << "Sol_maille"  << Phieq_vol << std::endl;
                std::cout << FORMAT(50) << "N_G"  << Norm_G_eq << std::endl;
                  //std::cout << FORMAT(50) << "N_G _inf"  << << std::endl;
                    //std::cout << FORMAT(50) << "N_R rel"  <<  Norm_R_rel << std::endl;
                    //std::cout << FORMAT(50) << "N_R_infty"  <<  Norm_R_infty << std::endl;

                std::cout << FORMAT(50) << "R_Phieq_rel "  <<  Norm_R_Phieq_rel << std::endl;
                std::cout << FORMAT(50) << "Iter"  <<  k_eq  << "\n" << std::endl;
                //std::cout << FORMAT(50) << "U "  <<  U_ini  << "\n" << std::endl;



            //}while( (k < s_iter) && (Norm_G > s_G || Norm_R > s_R )) ; //boucle Newton
            }while(Newton_eq_can_continue ) ; //boucle Newton

    if(Newton_eq_is_CV){
        std::cout << FORMAT(50) << "Computation of the thermal equilibrium : DONE \n"  <<  std::endl;
    }
    else{
        std::cout << FORMAT(50) << "Computation of the thermal equilibrium : FAILURE  \n"   << std::endl;
    }

    SolutionVectorType N_eq_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType N_eq_edge = SolutionVectorType::Zero(n_edge);
    SolutionVectorType P_eq_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType P_eq_edge = SolutionVectorType::Zero(n_edge);

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        N_eq_vol(iT) = inv_nl(sol.alpha_N + Phieq_vol(iT));
        P_eq_vol(iT) = inv_nl(sol.alpha_P - Phieq_vol(iT));
    }//for iT

    for(int iF = 0; iF< nunkw;iF++){
        N_eq_edge(iF) = inv_nl(sol.alpha_N + Phieq_edge(iF));
        P_eq_edge(iF) = inv_nl(sol.alpha_P - Phieq_edge(iF));
        }

    SolutionSCType U_eq;
    SolutionSCType X_eq;
    U_eq[0]=N_eq_vol;
    U_eq[1]=P_eq_vol;
    U_eq[2]=Phieq_vol;
    X_eq[0]=N_eq_edge;
    X_eq[1]=P_eq_edge;
    X_eq[2]=Phieq_edge;

    cout << "L^infty error between final time and equilibrium, N : " << (U_eq[0]-U_end[0]).lpNorm<Eigen::Infinity>() << endl;
    cout << "L^infty error between final time and equilibrium, P : " << (U_eq[1]-U_end[1]).lpNorm<Eigen::Infinity>() << endl;
    cout << "L^infty error between final time and equilibrium, Phi : " << (U_eq[2]-U_end[2]).lpNorm<Eigen::Infinity>() << endl;

    std::cout << " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n" << "Entropy and long-time behaviour " << std::endl;





/*
    std::vector<Real> Diff_eq_N_L2;
    std::vector<Real> Diff_eq_N_L1;
    std::vector<Real> Diff_eq_P_L2;
    std::vector<Real> Diff_eq_P_L1;
    std::vector<Real> Diff_eq_Phi_L2;
    std::vector<Real> Diff_eq_Phi_L1;
    for(int n = 0;n<  n_iter+1; n ++){

        Real diff_N_l2 = 0.;
        Real diff_N_l1 = 0.;
        Real diff_P_l2 = 0.;
        Real diff_P_l1 = 0.;
        Real diff_Phi_l2 = 0.;
        Real diff_Phi_l1 = 0.;

        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
            const Cell & T = Th->cell(iT);
            const Real & m_T = T.measure();
            const Point & x_T = T.center();
            const int & m = T.numberOfFaces();
            diff_N_l2+= m_T*std::pow( U_eq[0](iT) -Sol_vol[n][0](iT) , 2);
            diff_N_l1+= m_T*std::fabs(  U_eq[0](iT) -Sol_vol[n][0](iT));
            diff_P_l2+= m_T*std::pow( U_eq[1](iT) -Sol_vol[n][1](iT) , 2);
            diff_P_l1+= m_T*std::fabs(  U_eq[1](iT) -Sol_vol[n][1](iT));
            diff_Phi_l2+= m_T*std::pow( U_eq[2](iT) -Sol_vol[n][2](iT) , 2);
            diff_Phi_l1+= m_T*std::fabs(  U_eq[2](iT) -Sol_vol[n][2](iT));
//            diff_N_l2+= m_T*std::pow( N_ex_vol(iT) -Sol_N_vol[n](iT) , 2);
//            diff_N_l1+= m_T*std::fabs(  N_ex_vol(iT) -Sol_N_vol[n](iT));
//            diff_P_l2+= m_T*std::pow( P_ex_vol(iT) -Sol_P_vol[n](iT) , 2);
//            diff_P_l1+= m_T*std::fabs(  P_ex_vol(iT) -Sol_P_vol[n](iT));
//            diff_Phi_l2+= m_T*std::pow( Phiex_vol(iT) -Sol_Phi_vol[n](iT) , 2);
//            diff_Phi_l1+= m_T*std::fabs(  Phiex_vol(iT) -Sol_Phi_vol[n](iT));



        } // for iT

      Diff_eq_N_L2.push_back(sqrt(diff_N_l2));
      Diff_eq_N_L1.push_back(diff_N_l1);
      Diff_eq_P_L2.push_back(sqrt(diff_P_l2));
      Diff_eq_P_L1.push_back(diff_P_l1);
      Diff_eq_Phi_L2.push_back(sqrt(diff_Phi_l2));
      Diff_eq_Phi_L1.push_back(diff_Phi_l1);

    } // for n

    std::vector<Real> Diff_end_N_L2;
    std::vector<Real> Diff_end_N_L1;
    std::vector<Real> Diff_end_P_L2;
    std::vector<Real> Diff_end_P_L1;
    std::vector<Real> Diff_end_Phi_L2;
    std::vector<Real> Diff_end_Phi_L1;

    for(int n = 0;n<  n_iter+1; n ++){

        Real diff_N_l2 = 0.;
        Real diff_N_l1 = 0.;
        Real diff_P_l2 = 0.;
        Real diff_P_l1 = 0.;
        Real diff_Phi_l2 = 0.;
        Real diff_Phi_l1 = 0.;

        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
            const Cell & T = Th->cell(iT);
            const Real m_T = T.measure();
            const Point x_T = T.center();
            const int m = T.numberOfFaces();
            diff_N_l2+= m_T*std::pow( U_end[0](iT) -Sol_vol[n][0](iT) , 2);
            diff_N_l1+= m_T*std::fabs(  U_end[0](iT) -Sol_vol[n][0](iT));
            diff_P_l2+= m_T*std::pow( U_end[1](iT) -Sol_vol[n][1](iT) , 2);
            diff_P_l1+= m_T*std::fabs(  U_end[1](iT) -Sol_vol[n][1](iT));
            diff_Phi_l2+= m_T*std::pow( U_end[2](iT) -Sol_vol[n][2](iT) , 2);
            diff_Phi_l1+= m_T*std::fabs(  U_end[2](iT) -Sol_vol[n][2](iT));


        } // for iT

      Diff_end_N_L2.push_back(sqrt(diff_N_l2));
      Diff_end_N_L1.push_back(diff_N_l1);
      Diff_end_P_L2.push_back(sqrt(diff_P_l2));
      Diff_end_P_L1.push_back(diff_P_l1);
      Diff_end_Phi_L2.push_back(sqrt(diff_Phi_l2));
      Diff_end_Phi_L1.push_back(diff_Phi_l1);

    } // for n
*/

    SolutionVectorType nl_N_eq = SolutionVectorType::Zero(n_vol);
    SolutionVectorType nl_P_eq = SolutionVectorType::Zero(n_vol);
    SolutionVectorType p_nl_N_eq = SolutionVectorType::Zero(n_vol);
    SolutionVectorType p_nl_P_eq = SolutionVectorType::Zero(n_vol);


    for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
            const Cell & T = Th->cell(iT);
            const Real m_T = T.measure();
            nl_N_eq(iT) = nl(N_eq_vol(iT));
            nl_P_eq(iT) = nl(P_eq_vol(iT));
            p_nl_N_eq(iT) = p_nl(N_eq_vol(iT));
            p_nl_P_eq(iT) = p_nl(P_eq_vol(iT));
    } // for iT


    std::vector<Real> Entropy;
    std::vector<Real> Diff_eq_L2;
    std::vector<Real> Diff_end_L2;
    std::vector<Real> Weak_entro; // the quantity used by Gartner and gajewski with square roots
    std::vector<Real> Esti_nu;


       for(int n = 0;n<  n_iter+1; n ++){

        Real entro = 0.;
        Real diff_eq = 0.;
        Real diff_end= 0.;
        Real weak_entro = 0.;
        Real esti_nu = 0.;

        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
            const Cell & T = Th->cell(iT);
            const Real m_T = T.measure();

            entro += m_T * abs( p_nl(Sol_vol[n][0](iT)) - p_nl_N_eq(iT) - nl_N_eq(iT)*(Sol_vol[n][0](iT) - N_eq_vol(iT)));
            entro += m_T * abs( p_nl(Sol_vol[n][1](iT)) - p_nl_P_eq(iT) - nl_P_eq(iT)*(Sol_vol[n][1](iT) - P_eq_vol(iT)));

            diff_eq += m_T* ( std::pow( U_eq[0](iT) -Sol_vol[n][0](iT) , 2) + std::pow( U_eq[1](iT) -Sol_vol[n][1](iT) , 2) +std::pow( U_eq[2](iT) -Sol_vol[n][2](iT) , 2) );
            diff_end += m_T* ( std::pow( U_end[0](iT) -Sol_vol[n][0](iT) , 2) + std::pow( U_end[1](iT) -Sol_vol[n][1](iT) , 2) +std::pow( U_end[2](iT) -Sol_vol[n][2](iT) , 2) );

            weak_entro +=  m_T* ( std::pow( sqrt(U_eq[0](iT)) -sqrt(Sol_vol[n][0](iT)) , 2) + std::pow( sqrt(U_eq[1](iT)) -sqrt(Sol_vol[n][1](iT)) , 2));

        } // for iT

        const Real elec = comp_elec_en(Th,Flux_Phi,Sum_Flux_Phi,Sol_vol[n][2] - Phieq_vol, Sol_edge[n][2] - Phieq_edge);
        entro  += 0.5 * elec;
        weak_entro += elec;

        Entropy.push_back(entro);
        Diff_eq_L2.push_back(diff_eq);
        Diff_end_L2.push_back(diff_end);
        Weak_entro.push_back(weak_entro);

        if(n>1){
            esti_nu = Entropy[n] - Entropy[n-1];
            esti_nu = esti_nu / Entropy[n] ;
            esti_nu = esti_nu / (Temps[n] - Temps[n-1]);
            Esti_nu.push_back(-esti_nu);
            }
    } // for n

    Esti_nu.push_back(Esti_nu.back());


  std::cout << "Entropies computation : done"  << std::endl;

   Real h = Th->meshsize();
   std::ofstream myfile;
      myfile.open ("temps_long_impli.csv");
      myfile << "This is the first cell in the first column.\n";
      myfile <<"Description "<<"," <<"Derive-diffusion, schéma couplé.\n";
      myfile <<"Eta"<<"," << Eta  << endl;
      myfile <<"Pas de temps ref"<<"," << D_t  << endl;
      myfile <<"Nombre d'itérations effectif"<<"," << n_iter  << endl;
      myfile <<"Maillage"<<"," <<  geo_mesh << endl;
      myfile <<"Finesse de maillage"<<"," << h  << endl;
      myfile <<"" << endl;
      myfile << "Temps,Entropy,Diff_eq_L2,Weak_entropy,Diff_end_L2,Esti_nu,min_N,min_P,max_N,max_P,Diff_max_N,Diff_max_P,Nb_iter_Newton,Nb_resol_cumul,\n";
      for(int n=0; n< n_iter+1; n ++){
        myfile << Temps[n] <<"," << Entropy[n] <<","<<  Diff_eq_L2[n]  <<"," << Weak_entro[n]<<","<<Diff_end_L2[n] <<","<< Esti_nu[n] <<","<< Min_N[n] <<"," << Min_P[n] <<"," <<  Max_N[n] <<"," << Max_P[n] <<"," << 1./0.27 - Max_N[n] <<"," << 1./0.27 -Max_P[n] <<"," <<Nb_iter_Newton[n] <<"," <<Nb_resol_cumul[n] << endl;
      }
      myfile.close();

      std::ofstream myfile2;
      myfile2.open ("tps_impli");
      myfile2 << "Temps Entro Diff_eq_L2 Diff_eq_L2_square Weak_entropy Diff_end_L2 Esti_nu min_N min_P max_N max_P Diff_max_N Diff_max_P Nb_iter Nb_resol_cumul \n";
      const int mod =round(n_iter / nb_pts_in_graph) +1;
      for(int n=0; n< n_iter+1; n ++){
        if(n % mod ==0){
            myfile2 << Temps[n]<<" " << Entropy[n] <<" "<< Diff_eq_L2[n] <<" "<< sqrt(Diff_eq_L2[n])<< " "<< Weak_entro[n]<<" "<<Diff_end_L2[n] << " "<<Esti_nu[n] <<" " << Min_N[n] <<" " << Min_P[n] <<" " <<  Max_N[n] <<" " << Max_P[n] <<" " << 1./0.27 - Max_N[n] <<" " <<1./0.27 - Max_P[n] <<" " << Nb_iter_Newton[n] <<" " << Nb_resol_cumul[n] << endl;
        }// if n % 100 ==0
      }//for n
      myfile2.close();

    std::cout <<  "Entropies files saved " << std::endl;

//
//        std::vector<Eigen::VectorXd> coeffs_Phi_infty(Th->numberOfCells());
//        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
//            Eigen::VectorXd Phi_F_infty (1);
//            Phi_F_infty(0) =P_eq_vol(iT);
//            coeffs_Phi_infty[iT] = Phi_F_infty;
//        }
//      postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>("output/Phi_eq.vtu", Th, coeffs_Phi_infty);
//
//
//        std::vector<Eigen::VectorXd> coeffs_Phi_end(Th->numberOfCells());
//        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
//            Eigen::VectorXd Phi_F_end (1);
//            Phi_F_end(0) =P_end_vol(iT);
//            coeffs_Phi_end[iT] = Phi_F_end;
//        }
//      postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>("output/Phi_end.vtu", Th, coeffs_Phi_end);
//
        if(Visu){
            std::vector<string> Species;
            Species.resize(3);
            Species[0] ="N";
            Species[1] ="P";
            Species[2] ="Phi";

            const int mod_visu =round(n_iter / nb_pts_in_visu) +1;
            for(int i = 0; i < 3; i++){
                string name = "output/impli_" + Species[i];
                const char *name_char = name.c_str();

                std::vector<Eigen::VectorXd> coeffs_u_infty(Th->numberOfCells());
                for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
                    Eigen::VectorXd uh_F_infty (1);
                    uh_F_infty(0) =U_eq[i](iT);
                    coeffs_u_infty[iT] = uh_F_infty;
                }
                char name_eq[35];
                strcpy(name_eq,name_char);
                strcat(name_eq,"_eq.vtu");
                //printf ("%c",name_eq);
                postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(name_eq, Th, coeffs_u_infty);

                std::cout <<  "Enregistrement fichier visualisation - etat stationnaire " << std::endl;

                for(int n=0; n< n_iter+1; n++ ){
                    if(n % mod_visu ==0){
                        std::vector<Eigen::VectorXd> coeffs_u(Th->numberOfCells());
                        for(Integer iT = 0; iT < Th->numberOfCells(); iT++) {
                            Eigen::VectorXd uh_F (1);
                            uh_F(0) =Sol_vol[n][i](iT);
                            coeffs_u[iT] = uh_F;
                        } // for iT

                    char str[20] = {0};
                    std::sprintf(str, "%d", n);
                    char name_c[35];
                    strcpy(name_c, name_char);
                    strcat(name_c, "_" );
                    strcat(name_c, str);
                    strcat(name_c,".vtu");
                    postProcessing<ho::HierarchicalScalarBasis2d<0>, boost::static_unsigned_min<1,3>::value>(name_c, Th, coeffs_u);

                    }//if
                } //for n
            }//for i


        std::cout <<  "Enregistrement fichier visualisation - evolutif " << std::endl;
        }
    
    std::cout <<  "Max and min (cells & edges)" << std::endl;
    std::cout << FORMAT(50) << "Min_N : " <<  min(min_N_vol, min_N_edge)   << std::endl;
    std::cout << FORMAT(50) << "Min_P : " <<  min(min_P_vol, min_P_edge)  << std::endl;
    std::cout << FORMAT(50) << "Max_N : " <<  max(max_N_vol, max_N_edge)  << std::endl;    
    std::cout << FORMAT(50) << "Max_P : " <<  max(max_P_vol, max_P_edge)  << std::endl;
    
    std::cout <<  "\nMax and min (cells) " << std::endl;
    std::cout << FORMAT(50) << "Min_N : " <<  min_N_vol  << std::endl;
    std::cout << FORMAT(50) << "Min_P : " <<  min_P_vol  << std::endl;
    std::cout << FORMAT(50) << "Max_N : " <<  max_N_vol  << std::endl;    
    std::cout << FORMAT(50) << "Max_P : " <<  max_P_vol  << std::endl;
    
    std::cout <<  "\nMax and min (edges)" << std::endl;
    std::cout << FORMAT(50) << "Min_N : " <<   min_N_edge   << std::endl;
    std::cout << FORMAT(50) << "Min_P : " <<   min_P_edge  << std::endl;
    std::cout << FORMAT(50) << "Max_N : " <<   max_N_edge  << std::endl;    
    std::cout << FORMAT(50) << "Max_P : " <<   max_P_edge  << std::endl;


    std::cout << FORMAT(50) << "time_transient_scheme"  << chrono_evol.diff() << std::endl;

    return 0;


}


int test_Jac_sc(const Mesh *Th, const Real dt){

    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_vol_unkw = Th->numberOfCells();
    int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_U = 3 * n_vol; //number of cooridnates for a vector U  [cell unknowns]
    int n_X = 3 * n_edge; //number of cooridnates for a vector X  [face unknowns]
    ho::pho::Regular sol;

    std::cout << "Pre-computation for the evolutionary problem" << endl;
    std::cout << "Pre-computation begins" << endl;
    common::chrono c_precomput;
    c_precomput.start();

    std::cout << "Pre-computation of scheme data (Fluxes) : go !" << endl;

    int n_edge_Dir = 0.;
    for(int iF =0; iF < n_edge; iF ++){
        if(sol.isDirichlet(Th->face(iF))){n_edge_Dir += 1;}
    }

    std::vector<TripletType> Triplets_mass;
    Triplets_mass.reserve(n_vol);
    for(int iT=0; iT < n_vol; iT ++){
        const Cell & T = Th->cell(iT);
        const Real & m_T = T.measure();
        Triplets_mass.push_back(TripletType(iT,iT,m_T));
    } // for iT

    SparseMatrixType M_mass(n_vol, n_vol);
    M_mass.setFromTriplets(Triplets_mass.begin(), Triplets_mass.end());


    std::vector<vector<vector<Real>>> A_N;
    std::vector<vector<vector<Real>>> A_P;
    std::vector<vector<vector<Real>>> A_Phi;
    comp_A_elt(Th, sol.Lambda_N, Eta, A_N);
    comp_A_elt(Th, sol.Lambda_P, Eta, A_P);
    comp_A_elt(Th, sol.Lambda_Phi, Eta, A_Phi);
    std::vector<vector<vector<vector<Real>>>> A_sc;
    A_sc.resize(3);
    A_sc[0] = A_N;
    A_sc[1] = A_P;
    A_sc[2] = A_Phi;


    std::vector<vector<Real>> B_N;
    std::vector<vector<Real>> B_P;
    std::vector<vector<Real>> B_Phi;
    comp_B_elt(Th, A_N, B_N );
    comp_B_elt(Th, A_P, B_P );
    comp_B_elt(Th, A_Phi, B_Phi );
    std::vector<vector<vector<Real>>> B_sc;
    B_sc.resize(3);
    B_sc[0] = B_N;
    B_sc[1] = B_P;
    B_sc[2] = B_Phi;


    std::vector<vector<Real>> Brev_N;
    std::vector<vector<Real>> Brev_P;
    std::vector<vector<Real>> Brev_Phi;
    comp_Brev_elt(Th, A_N, Brev_N );
    comp_Brev_elt(Th, A_P, Brev_P );
    Brev_Phi =B_Phi;
    std::vector<vector<vector<Real>>> Brev_sc;
    Brev_sc.resize(3);
    Brev_sc[0] = Brev_N;
    Brev_sc[1] = Brev_P;
    Brev_sc[2] = Brev_Phi;


    std::vector<Real> Alpha_N;
    std::vector<Real> Alpha_P;
    std::vector<Real> Alpha_Phi;
    comp_Alpha_elt(Th, B_N, Alpha_N );
    comp_Alpha_elt(Th, B_P, Alpha_P );
    comp_Alpha_elt(Th, B_Phi, Alpha_Phi );
    std::vector<vector<Real>> Alpha_sc;
    Alpha_sc.resize(3);
    Alpha_sc[0] = Alpha_N;
    Alpha_sc[1] = Alpha_P;
    Alpha_sc[2] = Alpha_Phi;


    std::vector<vector<LocalVectorType>> Flux_N;
    std::vector<vector<LocalVectorType>> Flux_P;
    std::vector<vector<LocalVectorType>> Flux_Phi;
    precomp_flux(Th, A_N, B_N, Flux_N );
    precomp_flux(Th, A_P, B_P, Flux_P );
    precomp_flux(Th, A_Phi, B_Phi, Flux_Phi );
    std::vector<vector<vector<LocalVectorType>>> Flux_sc;
    Flux_sc.resize(3);
    Flux_sc[0] = Flux_N;
    Flux_sc[1] = Flux_P;
    Flux_sc[2] = Flux_Phi;


    std::vector<LocalVectorType>  Sum_Flux_N;
    std::vector<LocalVectorType>  Sum_Flux_P;
    std::vector<LocalVectorType>  Sum_Flux_Phi;
    precomp_Somme_flux(Th, Flux_N, Sum_Flux_N);
    precomp_Somme_flux(Th, Flux_P, Sum_Flux_P);
    precomp_Somme_flux(Th, Flux_Phi, Sum_Flux_Phi);
    std::vector<vector<LocalVectorType>> Sum_Flux_sc;
    Sum_Flux_sc.resize(3);
    Sum_Flux_sc[0] = Sum_Flux_N;
    Sum_Flux_sc[1] = Sum_Flux_P;
    Sum_Flux_sc[2] = Sum_Flux_Phi;


    std::vector<LocalVectorType>  Reconstruction;
    precomp_reconstruc(Th,Reconstruction);


    cout << "Pre-computation of scheme data (Fluxes) : DONE ! " << endl;


    //Creation condition initiale discrete
    std::cout << "Discretisation of and boundary andinitial data : go !" << endl;

    SolutionVectorType C_pt = SolutionVectorType::Zero(n_vol);
    SolutionVectorType G_Neu = SolutionVectorType::Zero(n_edge);
    SolutionVectorType N_boundary = SolutionVectorType::Zero(n_edge);
    SolutionVectorType P_boundary = SolutionVectorType::Zero(n_edge);
    SolutionVectorType Phi_boundary = SolutionVectorType::Zero(n_edge);

    SolutionVectorType N_0_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType N_0_edge = SolutionVectorType::Zero(n_edge);
    SolutionVectorType P_0_vol = SolutionVectorType::Zero(n_vol);
    SolutionVectorType P_0_edge = SolutionVectorType::Zero(n_edge);

    for(Integer iT = 0; iT<  Th->numberOfCells();iT++ ){
        const Cell & T = Th->cell(iT);
        const Point & x_T = T.center();
        N_0_vol(iT) = sol.N_ini(x_T);
        P_0_vol(iT) = sol.P_ini(x_T);
        C_pt(iT) = sol.C(x_T);
    }//for iT

    for(int iF = 0; iF< nunkw;iF++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        N_0_edge(iF) = sol.N_ini(x_F);
        P_0_edge(iF) = sol.P_ini(x_F);
        if(sol.isNeumann(F)){G_Neu(iF) = (F.measure()) * sol.g_n(x_F);} //if Neumann
        if(sol.isDirichlet(F)){
            N_boundary(iF) = sol.N_b(x_F);
            P_boundary(iF) = sol.P_b(x_F);
            Phi_boundary(iF) = sol.Phi_b(x_F);
            } // if Dirichlet
        }

    SolutionSCType U_0;
    U_0[0] = N_0_vol;
    U_0[1] = P_0_vol;
    U_0[2] = SolutionVectorType::Zero(n_vol);

    SolutionSCType X_b;
    X_b[0] = N_boundary;
    X_b[1] = P_boundary;
    X_b[2] = Phi_boundary;


    SolutionSCType X_0 ;
    X_0[0] = N_0_edge;
    X_0[1] = P_0_edge;
    X_0[2] = SolutionVectorType::Zero(n_vol);

    c_precomput.stop();


    std::vector<SolutionSCType> Sol_vol;
    std::vector<SolutionSCType> Sol_edge;

    std::vector<Real> Temps;
    std::vector<int> Nb_iter_Newton;

    Sol_vol.push_back(U_0);
    Sol_edge.push_back(X_0);

    std::cout << "Discretisation of initial data ends" << endl;
    Temps.push_back(0.);
    Nb_iter_Newton.push_back(0.);

    std::cout << "Pre-computation ends" << endl;
    std::cout << FORMAT(50) << "time_precomputation"  << c_precomput.diff() << std::endl;

    test_Jaco_sc(Th, dt,  C_pt,sol.isDirichlet , X_b , sol.isNeumann , G_Neu,
        U_0, X_0 , A_sc , B_sc , Brev_sc, Alpha_sc , Reconstruction, Flux_sc , Sum_Flux_sc);



    return 1;
}


int test_Jac_equilibrium(const Mesh *Th){

    int n_vol = Th->numberOfCells();
    int n_edge = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    int n_vol_unkw = Th->numberOfCells();
    int nunkw = Th->numberOfInternalFaces() + Th->numberOfBoundaryFaces();
    ho::pho::Regular sol;


    int n_edge_Dir = 0.;
    for(int iF =0; iF < n_edge; iF ++){
        if(sol.isDirichlet(Th->face(iF))){n_edge_Dir += 1;}
    }

    SolutionVectorType G_Neu = SolutionVectorType::Zero(nunkw);

    for(int iF = 0; iF< nunkw;iF++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
        if(sol.isNeumann(F)){G_Neu(iF) = (F.measure()) * sol.g_n(x_F);} //if Neumann
        }


     //thermal equibilibrium
    std::cout << "Computation of the thermal equilibrium " << std::endl;

    std::vector<vector<vector<Real>>> A_Phi;
    comp_A_elt(Th, sol.Lambda_Phi, Eta, A_Phi);

    std::vector<vector<Real>> B_Phi;
    comp_B_elt(Th, A_Phi, B_Phi );

    std::vector<Real> Alpha_Phi;
    comp_Alpha_elt(Th, B_Phi, Alpha_Phi );


    std::vector<vector<LocalVectorType>> Flux_Phi;
    precomp_flux(Th, A_Phi, B_Phi, Flux_Phi );

    std::vector<LocalVectorType>  Sum_Flux_Phi;
    precomp_Somme_flux(Th, Flux_Phi, Sum_Flux_Phi);

    SolutionVectorType Phieq_b = SolutionVectorType::Zero(nunkw);
    for(int iF = 0; iF < nunkw; iF ++){
        const Face & F = Th->face(iF);
        const Point & x_F = F.barycenter();
       	if(sol.isDirichlet(F)){
            Phieq_b(iF) =  sol.Phi_b(x_F);
        }// if
  }// for iF

    test_Jaco_eq(Th, sol.alpha_P, sol.alpha_N , sol.C, sol.isDirichlet, Phieq_b, sol.isNeumann , G_Neu,
                    A_Phi, B_Phi, Alpha_Phi , Flux_Phi ,  Sum_Flux_Phi);

    return 1;
}
//
//


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

