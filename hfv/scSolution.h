// -*- C++ -*-
#ifndef SCSOLUTION_H
#define SCSOLUTION_H

#include <boost/math/constants/constants.hpp>

#include "hfvSC.h"

using boost::math::constants::pi;

namespace ho
{
  namespace pho
  {
    struct ExactSolution
    {

      typedef std::function<TensorType(const Point &)> DiffusivityType;
      typedef std::function<Real(const Point &)> PotentialType;
      typedef std::function<Real(const Point &)> DopingType;
      typedef std::function<Real(const Point &)> LoadType;

      typedef std::function<Real(const Real t, const Point &)> ExactSolutionType;
      typedef std::function<Real(const Point &)> InitialDataType;
      typedef std::function<Real(const Point &)> BoundaryType;
      typedef std::function<Real(const Point &)> BoundaryNeumannType;

      typedef std::function<bool(const Face &)> DefBoundaryConditionType;

      typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> ExactGradientType;
      typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> FieldType;


      std::string description_tex;

      Real lambda;
      Real alpha_N;
      Real alpha_P;


      DiffusivityType Lambda_N;
      DiffusivityType Lambda_P;
      DiffusivityType Lambda_Phi;

      DefBoundaryConditionType isDirichlet;
      DefBoundaryConditionType isNeumann;

      DopingType C;

      BoundaryType N_b;
      BoundaryType P_b;
      BoundaryType Phi_b;
      BoundaryNeumannType g_n;

      InitialDataType N_ini;
      InitialDataType P_ini;
    };

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------

    struct Regular : public ExactSolution
    {

// /*

//        Regular(const Real _lambda = 0.4, const Real _lN = 1., const Real _lP = 1., const Real b = 2. , const Real N_0 = 1. , const Real  N_1 = 1., const Real alpha_0= 1.)
//       {
//       Real P_0 = inv_nl(alpha_0 - nl(N_0));
//       Real P_1 = inv_nl(alpha_0 - nl(N_1));

//       description_tex = "with Holger, full Neumann" ;

//       lambda = _lambda ;

//       Lambda_N = [_lN,b](const Point & x) -> TensorType {
//             TensorType L;
//             L <<
//                  1.,  b,
//                  -b , 1.;
//             Real scal_N  = _lN /(1. + b*b) ;
//           return   scal_N * L  ;
//         };

//         Lambda_P = [_lP,b](const Point & x) -> TensorType {
//             TensorType L;
//             L <<
//                 1. , -b ,
//                 b , 1.;
//             Real scal_P  = _lP /(1. + b*b) ;
//           return   scal_P * L;
//         };

//         Lambda_Phi = [_lambda](const Point & x) -> TensorType {
//             TensorType L;
//             L <<
//                 1. , 0. ,
//                 0. , 1.;
//           return  _lambda * _lambda * L;
//         };

//         isDirichlet = [](const Face & F) -> bool {
//         Point x_F = F.barycenter();
//         Real x = x_F(0);
//         Real y = x_F(1);
//         bool D_1 = ( y == 0.) ;
//         bool D_2 = ( y == 1.)&& (0.5- 0.01 < x && x < 0.5 + 0.01 ) ;
//         //bool D_2 = (y==1.) ;
//         bool Dir =  D_2;
//         //bool Dir = false;
//         bool Bound = F.isBoundary();
//         return Dir && Bound;
//       };

//       isNeumann = [this](const Face & F) -> bool {
//         //Point x_F = F.barycenter();
//         //Real x = x_F(0);
//         //Real y = x_F(1);
//         bool Bound = F.isBoundary();
//         bool Dir = this->isDirichlet(F);
//         return Bound && (! Dir) ;
//       };

//       C =[](const Point & x) -> Real {
//             return 0.;
//             // Real d_square = (x(0)-0.5)* (x(0)-0.5) + (x(1)-1.)* (x(1)- 1.);
//             // if(d_square < 0.1 * 0.1 ){return 5.;} //P-region
//             // else{return 0;} //N-region
//       };

//       N_b =[N_0,N_1](const Point & x) -> Real {
//             bool D_1 = ( x(1) == 0.) ;
//             bool D_2 = ( x(1) == 1.)&& (0.5- 0.05 < x(0) && x(0) < 0.5 + 0.05 ) ;
//             if(D_1){return N_0;}
//             else if(D_2){return N_1;}
//             else{return 1.; }

//         };

//     P_b =[P_0,P_1](const Point & x) -> Real {
//             bool D_1 = ( x(1) == 0.) ;
//             bool D_2 = ( x(1) == 1.)&& (0.5- 0.05 < x(0) && x(0) < 0.5 + 0.05 ) ;
//             if(D_1){return P_0;}
//             else if(D_2){return P_1;}
//             else{return 1.; }
//         };

//     Phi_b=[this,alpha_0](const Point & x) -> Real{
//         Real nl_N = nl(this->N_b(x));
//         Real nl_P = nl(this->P_b(x));
//         return 0.5 * (nl_N - nl_P);
//         //return alpha_0;
//         };

//     g_n =[](const Point & x) -> Real {
//             return 0.;
//         };

//     alpha_N = 0.5 * alpha_0;
//     alpha_P = 0.5 * alpha_0;
//     /*
//     N_ini =[N_0,N_1](const Point & x) -> Real {
//             return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
//       };

//     P_ini =[P_0,P_1](const Point & x) -> Real {
//             return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
//     };
//     */
// /*    
//     N_ini =[N_0,N_1](const Point & x) -> Real {
//             Real d_square = (x(0)-0.5)* (x(0)-0.5) + (x(1)-0.5)* (x(1)-0.5);
//             Real dist = sqrt(d_square);
//             if(dist<0.3){return (N_1 + N_0);}
//             else {return 0.5 * (N_1 + N_0);}
//       };

//     P_ini =[P_0,P_1](const Point & x) -> Real { 
//             Real d_square = (x(0)-0.5)* (x(0)-0.5) + (x(1)-0.5)* (x(1)-0.5);
//             Real dist = sqrt(d_square);
//             if(dist<0.3){return 0.1 *(P_1 + P_0);}
//             else {return 0.5 * (P_1 + P_0);}
//       }; */

//       N_ini =[N_0,N_1](const Point & x) -> Real {
//             Real d_square = ((x(0)-0.5)* (x(0)-0.5))/(0.4*0.4) + ((x(1)-0.5)* (x(1)-0.5))/(0.2*0.2);
//             Real dist = sqrt(d_square);
//             if(dist<1.){return (N_1 + N_0);}
//             else {return 0.5 * (N_1 + N_0);}
//       };

//     P_ini =[P_0,P_1](const Point & x) -> Real { 
//             Real d_square = ((x(0)-0.5)* (x(0)-0.5))/(0.4*0.4) + ((x(1)-0.5)* (x(1)-0.5))/(0.2*0.2);
//             Real dist = sqrt(d_square);
//             if(dist<1.){return (P_1 + P_0);}
//             else {return 0.5 * (P_1 + P_0);}
//       };
    
// }




/*
       Regular(const Real _lambda = 0.15, const Real _lN = 1., const Real _lP = 1., const Real b = 0. , const Real N_0 = exp(1.) , const Real  N_1 = 1., const Real alpha_0= 1.)
      {
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));

      description_tex = "with Holger, test 3" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.)&& (0.5- 0.05 < x && x < 0.5 + 0.05 ) ;
        //bool D_2 = (y==1.) ;
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            Real d_square = (x(0)-0.5)* (x(0)-0.5) + (x(1)-1.)* (x(1)- 1.);
            Real d = sqrt(d_square);
            if(d < 0.2  ){return -1.;} //P-region
            else if(d < 0.8) {return 1;} //N-region
            else{return 10;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.)&& (0.5- 0.05 < x(0) && x(0) < 0.5 + 0.05 ) ;
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.)&& (0.5- 0.05 < x(0) && x(0) < 0.5 + 0.05 ) ;
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * alpha_0;
    alpha_P = 0.5 * alpha_0;

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };
    
}
   */  


/*
/*
      Regular(const Real _lambda = 0.2, const Real _lN = 1., const Real _lP = 1., const Real b = 5. , const Real N_0 = 1. , const Real  N_1 = 1., const Real alpha_0= 1.)
      {
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));

      description_tex = "with Holger, test 2" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.)&& (0.5- 0.05 < x && x < 0.5 + 0.05 ) ;
        //bool D_2 = (y==1.) ;
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            Real d_square = (x(0)-0.5)* (x(0)-0.5) + (x(1)-1.)* (x(1)- 1.);
            if(d_square < 0.4 * 0.4 ){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.)&& (0.5- 0.05 < x(0) && x(0) < 0.5 + 0.05 ) ;
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.)&& (0.5- 0.05 < x(0) && x(0) < 0.5 + 0.05 ) ;
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * alpha_0;
    alpha_P = 0.5 * alpha_0;
    
    // N_ini =[N_0,N_1](const Point & x) -> Real {
    //         return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
    //   };

    // P_ini =[P_0,P_1](const Point & x) -> Real {
    //         return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    // };
   

      
    // N_ini =[N_0,N_1](const Point & x) -> Real {
    //         Real d_square = (x(0)-0.5)* (x(0)-0.5) + (x(1)-0.5)* (x(1)-0.5);
    //         Real dist = sqrt(d_square);
    //         if(dist<0.3){return (N_1 + N_0);}
    //         else {return 0.5 * (N_1 + N_0);}
    //   };

    // P_ini =[P_0,P_1](const Point & x) -> Real { 
    //         Real d_square = (x(0)-0.5)* (x(0)-0.5) + (x(1)-0.5)* (x(1)-0.5);
    //         Real dist = sqrt(d_square);
    //         if(dist<0.3){return 0.1 *(P_1 + P_0);}
    //         else {return 0.5 * (P_1 + P_0);}
    //   }; 
    

      N_ini =[N_0,N_1](const Point & x) -> Real {
            Real d_square = ((x(0)-0.5)* (x(0)-0.5))/(0.4*0.4) + ((x(1)-0.5)* (x(1)-0.5))/(0.2*0.2);
            Real dist = sqrt(d_square);
            if(dist<1.){return (N_1 + N_0);}
            else {return 0.5 * (N_1 + N_0);}
      };

    P_ini =[P_0,P_1](const Point & x) -> Real { 
            Real d_square = ((x(0)-0.5)* (x(0)-0.5))/(0.4*0.4) + ((x(1)-0.5)* (x(1)-0.5))/(0.2*0.2);
            Real dist = sqrt(d_square);
            if(dist<1.){return (P_1 + P_0);}
            else {return 0.5 * (P_1 + P_0);}
      };
    
} */


/*
      Regular(const Real _lambda = 0.1, const Real _lN = 1., const Real _lP = 1., const Real b = 2. , const Real N_0 = exp(1.) , const Real  N_1 = 1., const Real alpha_0= 1.)
      {
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));

      description_tex = "with Holger, test 1" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) ;
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if(x(1)>0.5){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) ;
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) ;
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * alpha_0;
    alpha_P = 0.5 * alpha_0;

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };
    
}
*/


Regular(const Real _lambda = 0.05, const Real _lN = 1., const Real _lP = 1., const Real b = 0.5 , const Real N_0 = 1.5 , const Real  N_1 = 1. , const Real alpha_0= 0)
      {
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));

      description_tex = "Diode Bessemoulin-Chainais, general nonlinearity with magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * alpha_0;
    alpha_P = 0.5 * alpha_0;

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };
    
}


/*
Regular(const Real _lambda = 0.1, const Real _lN = 1., const Real _lP = 1., const Real b = 0. , const Real N_0 = exp(1.) , const Real  N_1 = 1., const Real alpha_0= 1)
      {
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));
      
      Real Beta = exp(0.5 * alpha_0);

      description_tex = "Diode Bessemoulin-Chainais, Boltzmann stat, quasineutral initial condition,  with magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };



    N_ini =[this, Beta](const Point & x) -> Real {
            Real c = this->C(x);
            Real var = c /(2*Beta);
            Real argsh = var + sqrt(1 + var * var); 
            return Beta / argsh;
      };

    P_ini =[this, Beta](const Point & x) -> Real {
            Real c = this->C(x);
            Real var = c /(2*Beta);
            Real argsh = var + sqrt(1 + var * var); 
            return Beta * argsh;
    };
    
}
*/
/*
Regular(const Real _lambda = 0.02, const Real _lN = 1., const Real _lP = 1., const Real b = 0. , const Real N_0 = 0.9 , const Real  N_1 = 0.1)
      {
      const Real alpha_0= nl(N_0) + nl(N_1); 
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));
      Real pot_0 = nl(N_0)-0.5*alpha_0;
      Real pot_1 = nl(N_1)-0.5*alpha_0;
      Real phi = 0.5*(pot_0+pot_1); 

      description_tex = "Diode Chainais Filbet, general nonlinearity with magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * alpha_0;
    alpha_P = 0.5 * alpha_0;

    N_ini =[alpha_0,phi](const Point & x) -> Real {
            return inv_nl(0.5*alpha_0 + phi );
      };

    P_ini =[alpha_0,phi](const Point & x) -> Real {
            return inv_nl(0.5*alpha_0 - phi );
    };

}
*/
/*
Regular(const Real _lambda = 0.1, const Real _lN = 1., const Real _lP = 1., const Real b = 3. , const Real N_0 = 0.9 , const Real  N_1 = 0.1)
      {
      const Real alpha_0= nl(N_0) + nl(N_1); 
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));
      

      description_tex = "Diode Bessemoulin-Chainais-Vignal with perturbation, general nonlinearity with magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -0.8;} //P-region
            else{return 0.8;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * alpha_0;
    alpha_P = 0.5 * alpha_0;

    N_ini =[this,N_0,N_1,_lambda](const Point & x) -> Real {
          bool D_c = true; // (0.1 < x(1) && x(1) < 0.5)&& (0.6 < x(0) && x(0) < 0.9);
          if(D_c){return  (1. - this->C(x))/(2. +0.* _lambda);} 
          else {return  (1. + this->C(x))/(2. + 0.*_lambda);} 
      };

    P_ini =[this,P_0,P_1,_lambda](const Point & x) -> Real {
          bool D_c = true; //(0.1 < x(1) && x(1) < 0.5)&& (0.6 < x(0) && x(0) < 0.9);
          if(D_c){return  (1. + this->C(x))/(2. + 0.*_lambda);} 
          else{  return  (1. - this->C(x))/(2.  + 0.*_lambda);}
    };

}
*/
/*
Regular(const Real _lambda = 0.005, const Real _lN = 1., const Real _lP = 1., const Real b = 5. , const Real N_0 = 0.9 , const Real  N_1 = 0.1)
      {
      const Real alpha_0= nl(N_0) + nl(N_1); 
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));
      

      description_tex = "Diode Bessemoulin-Chainais-Vignal, general nonlinearity with magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -0.8;} //P-region
            else{return 0.8;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * alpha_0;
    alpha_P = 0.5 * alpha_0;

    N_ini =[this](const Point & x) -> Real {
            return 0.5 * (1. + this->C(x)) ;
      };

    P_ini =[this](const Point & x) -> Real {
            return 0.5 * (1. - this->C(x));
    };

}
*/
/*Regular(const Real _lambda = 0.05, const Real _lN = 1., const Real _lP = 1., const Real b = 1. , const Real N_0 = 3.5 , const Real  N_1 = 1.5, const Real alpha_0= 0.)
      {
      Real P_0 = inv_nl(alpha_0 - nl(N_0));
      Real P_1 = inv_nl(alpha_0 - nl(N_1));

      description_tex = "Diode Bessemoulin-Chainais, general nonlinearity with magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this,alpha_0](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return alpha_0;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * alpha_0;
    alpha_P = 0.5 * alpha_0;

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };
}
*/

/*
    Regular(const Real _lambda = 0.01581, const Real _lN = 0.04342944819, const Real _lP = 0.04342944819, const Real _v_up = 1.1 , const Real  _v_down = 0.1)
      {

      description_tex = "PNP-Transistor" ;

      lambda = _lambda ;

      Lambda_N = [_lN](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lN , 0. ,
                0. , _lN;
          return   L;
        };

        Lambda_P = [_lP](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lP , 0. ,
                0. , _lP;
          return   L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1., 0. ,
                0. , 1.;
          return   _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        bool D_3 = ( y == 1.) && (0.75 < x && x < 1);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2 || D_3;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            bool P_zone = (x(1)<1./3.) || ((x(0)< 0.5) && (x(1)>2./3.));
            if(P_zone){return -1.;} //P-region
            else{return 1.;} //N-region
      };

      N_b =[_v_up  , _v_down](const Point & x) -> Real {
            bool D_collector = ( x(1) == 0.) ;
            bool D_emmiter = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            bool D_base= ( x(1) == 1.) && (0.75 < x(0) && x(0) < 1.);
            if(D_collector){return _v_down;}
            else if(D_emmiter){return _v_down;}
            else if(D_base){return _v_up;}
            else{return 1.; }

        };

    P_b =[_v_up  , _v_down](const Point & x) -> Real {
            bool D_collector = ( x(1) == 0.) ;
            bool D_emmiter = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            bool D_base= ( x(1) == 1.) && (0.75 < x(0) && x(0) < 1.);
            if(D_collector){return _v_up;}
            else if(D_emmiter){return _v_up;}
            else if(D_base){return _v_down;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
            bool D_collector = ( x(1) == 0.) ;
            bool D_emmiter = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            bool D_base= ( x(1) == 1.) && (0.75 < x(0) && x(0) < 1.);
            if(D_collector){return -80.006;}
            else if(D_emmiter){return -0.;}
            else if(D_base){return -40.003;}
            else{return 1.; };
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = _v_down;
    alpha_P = _v_up;

    N_ini =[_v_up  , _v_down](const Point & x) -> Real {
        Real Inter_up;
        bool abs_emmiter = (0. < x(0) && x(0) < 0.25);
        bool abs_base= (0.75 < x(0) && x(0) < 1.);
        if(abs_emmiter){Inter_up = _v_down;}
        else if(abs_base){Inter_up = _v_up;}
        else{Inter_up = -2. *(x(0)-0.75) * _v_down + 2. *(x(0)-0.25) * _v_up;}
        return Inter_up * x(1) + _v_down *(1.-x(1));
      };

    P_ini =[_v_up  , _v_down](const Point & x) -> Real {
        Real Inter_up;
        bool abs_emmiter = (0. < x(0) && x(0) < 0.25);
        bool abs_base= (0.75 < x(0) && x(0) < 1.);
        if(abs_emmiter){Inter_up = _v_up;}
        else if(abs_base){Inter_up = _v_down;}
        else{Inter_up = -2. *(x(0)-0.75) * _v_up + 2. *(x(0)-0.25) * _v_down;}
        return Inter_up * x(1) + _v_up *(1.-x(1));
      };


    }
    */
    /*
    Regular(const Real _lambda = 0.13814755258, const Real _lN = 0.03619120682 , const Real _lP = 0.03619120682 , const Real N_0 =1.1 , const Real  N_1 = 0.1,const Real P_0 = 0.1, const Real  P_1 = 1.1)
      {

      description_tex = "Foward NP diode, Chainais_Peng " ;

      lambda = _lambda ;

      Lambda_N = [_lN](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lN , 0. ,
                0. , _lN;
          return   L;
        };

        Lambda_P = [_lP](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lP , 0. ,
                0. , _lP;
          return   L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1., 0. ,
                0. , 1.;
          return   _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return -15.156657196;}
            else if(D_2){return -12.854072103;}
            else{return 1.; }
        //return 0.;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_0 * P_0);
    alpha_P = 0.5 * log(N_0 * P_0);

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };

    }
    */
/*
    Regular(const Real _lambda = 0.01573365357, const Real _lN = 0.04342944819 , const Real _lP = 0.04342944819 , const Real N_0 =1.1 , const Real  N_1 = 0.1,const Real P_0 = 0.1, const Real  P_1 = 1.1)
      {

      description_tex = "reverse NP diode, Chainais_Peng " ;

      lambda = _lambda ;

      Lambda_N = [_lN](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lN , 0. ,
                0. , _lN;
          return   L;
        };

        Lambda_P = [_lP](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lP , 0. ,
                0. , _lP;
          return   L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1., 0. ,
                0. , 1.;
          return   _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return 91.5193567712;}
            else if(D_2){return -11.512925465;}
            else{return 1.; }
        //return 0.;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_0 * P_0);
    alpha_P = 0.5 * log(N_0 * P_0);

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };

    }
    */
/*
    Regular(const Real _lambda = 0.05, const Real _lN = 1., const Real _lP = 1., const Real N_0 =0.9 , const Real  N_1 = 0.1,const Real P_0 = 0.1, const Real  P_1 = 0.9)
      {

      description_tex = "N-P junction, long time, $\\lambda =  " + to_string(_lambda)+"$" ;

      lambda = _lambda ;

      Lambda_N = [_lN](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lN , 0. ,
                0. , _lN;
          return   L;
        };

        Lambda_P = [_lP](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lP , 0. ,
                0. , _lP;
          return   L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1., 0. ,
                0. , 1.;
          return   _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            return 0.;
            //if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            //else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
        Real logN = log(this->N_b(x));
        Real logP = log(this->P_b(x));
        return 0.5 * (logN - logP);
        //return 0.;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_0 * P_0);
    alpha_P = 0.5 * log(N_0 * P_0);

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };

    }
    */

/*
    Regular(const Real _lambda = 0.05, const Real _lN = 1., const Real _lP = 1.) // const Real N_0 =0.9 , const Real  N_1 = 0.1,const Real P_0 = 0.1, const Real  P_1 = 0.9)
      {
        const Real N_0 =0.9;
        const Real  N_1 = 0.1;
        const Real P_0 = 0.1;
        const Real  P_1 = 0.9;

      description_tex = "N-P junction, quasi neutral $\\lambda =  " + to_string(_lambda)+"$" ;

      lambda = _lambda ;

      Lambda_N = [_lN](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lN , 0. ,
                0. , _lN;
          return   L;
        };

        Lambda_P = [_lP](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lP , 0. ,
                0. , _lP;
          return   L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1., 0. ,
                0. , 1.;
          return   _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -0.8;} //P-region
            else{return 0.8;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return 1.1;}
            else if(D_2){return -1.1;}
            else{return 1.; }
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_0 * P_0);
    alpha_P = 0.5 * log(N_0 * P_0);

    N_ini =[this](const Point & x) -> Real {
            return 0.5 * (1. + this->C(x));
      };

    P_ini =[this](const Point & x) -> Real {
            return  0.5 * (1. - this->C(x));
    };

    }*/

/*
    Regular(const Real _lambda = 0.1, const Real _lN = 1., const Real _lP = 1., const Real b = 1. , const Real N_0 =0.9 , const Real  N_1 = 0.1,const Real P_0 = 0.1, const Real  P_1 = 0.9)
      {

      description_tex = "magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
        Real logN = log(this->N_b(x));
        Real logP = log(this->P_b(x));
        return 0.5 * (logN - logP);
        //return 0.;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_0 * P_0);
    alpha_P = 0.5 * log(N_0 * P_0);

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };

    */
/*
    Regular(const Real _lambda = 0.000001, const Real _lN = 1., const Real _lP = 1.) // const Real N_0 =0.9 , const Real  N_1 = 0.1,const Real P_0 = 0.1, const Real  P_1 = 0.9)
      {
        const Real N_0 =0.9;
        const Real  N_1 = 0.1;
        const Real P_0 = 0.1;
        const Real  P_1 = 0.9;


      description_tex = "N-P junction, quasi neutral $\\lambda =  " + to_string(_lambda)+"$" ;

      lambda = _lambda ;

      Lambda_N = [_lN](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lN , 0. ,
                0. , _lN;
          return   L;
        };

        Lambda_P = [_lP](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lP , 0. ,
                0. , _lP;
          return   L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1., 0. ,
                0. , 1.;
          return   _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -0.8;} //P-region
            else{return 0.8;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return 1.1;}
            else if(D_2){return -1.1;}
            else{return 1.; }
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_0 * P_0);
    alpha_P = 0.5 * log(N_0 * P_0);

    N_ini =[this](const Point & x) -> Real {
            Real f;
            if(x(1) * (x(1)-1.) * x(0)*(x(0)-1.) < 0.02){ f = 1.;}
            else{f=0.83;}
            return 0.5 * (f + this->C(x));
      };

    P_ini =[this](const Point & x) -> Real {
            Real f;
            if(x(1) * (x(1)-1.) * x(0)*(x(0)-1.) < 0.02){ f = 1.;}
            else{f=0.83;}
            return  0.5 * (f - this->C(x));
    };

    }*/

/*
    Regular(const Real _lambda = 0.1, const Real _lN = 1., const Real _lP = 1., const Real b = 0.3 , const Real N_0 =0.9 , const Real  N_1 = 0.1,const Real P_0 = 0.1, const Real  P_1 = 0.9)
      {

      description_tex = "magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
        Real logN = log(this->N_b(x));
        Real logP = log(this->P_b(x));
        return 0.5 * (logN - logP);
        //return 0.;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_0 * P_0);
    alpha_P = 0.5 * log(N_0 * P_0);

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };
} */

/*
Regular(const Real _lambda = 0.05, const Real _lN = 1., const Real _lP = 1., const Real b = 1.5 , const Real N_0 = exp(1.) , const Real  N_1 = 1. ,const Real P_0 = exp(-1.), const Real  P_1 = 1.)
      {

      description_tex = "Diode Bessemoulin-Chainais, with magnetic field" ;

      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return 0.;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * (nl(N_0) + nl(P_0));
    alpha_P = 0.5 * (nl(N_0) + nl(P_0));

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };
}
*/

/*
Regular(const Real _lambda = 0.05, const Real _lN = 1., const Real _lP = 1., const Real b = 2. , const Real N_0 = exp(1.) , const Real  N_1 = 1., const Real c_0 = 1. )
      {

      description_tex = "Diode Blackemore" ;


    const Real P_0 = inv_nl(c_0 - nl(N_0));
    const Real P_1 = inv_nl(c_0 - nl(N_1));


      lambda = _lambda ;

      Lambda_N = [_lN,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  b,
                 -b , 1.;
            Real scal_N  = _lN /(1. + b*b) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,b](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , -b ,
                b , 1.;
            Real scal_P  = _lP /(1. + b*b) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
        Real nl_N = nl(this->N_b(x));
        Real nl_P = nl(this->P_b(x));
        return 0.5 * (nl_N - nl_P);
        //return 0.;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * c_0;
    alpha_P = 0.5 * c_0;

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };
}
*/
/*
Regular(const Real _lambda = 0.1, const Real _lN = 1., const Real _lP = 1., const Real b = 1. , const Real N_0 = exp(1.) , const Real  N_1 = 1. ,const Real P_0 = exp(-1.), const Real  P_1 = 1.)
      {

      description_tex = "heterogeneous magnetic field" ;

      lambda = _lambda ;

      std::function<Real(const Point &)> B = [b](const Point & x ) -> Real{
            //if((x(0)< 0.5) && (x(1)>0.5)){return -1. * b;} //P-region
            //else{return 1. *b ;} //N-region
            return 4*(x(0)-0.5) *(x(0)-0.5) * b;
            };

      Lambda_N = [_lN,B](const Point & x) -> TensorType {
            TensorType L;
            L <<
                 1.,  B(x),
                 -B(x) , 1.;
            Real scal_N  = _lN /(1. + B(x)*B(x)) ;
          return   scal_N * L  ;
        };

        Lambda_P = [_lP,B](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , B(x) ,
                B(x) , 1.;
            Real scal_P  = _lP /(1. + B(x)*B(x)) ;
          return   scal_P * L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return  _lambda * _lambda * L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        bool Dir = D_1 || D_2;
        //bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            //return 0.;
            if((x(0)< 0.5) && (x(1)>0.5)){return -1.;} //P-region
            else{return 1.;} //N-region
      };

      N_b =[N_0,N_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return N_0;}
            else if(D_2){return N_1;}
            else{return 1.; }

        };

    P_b =[P_0,P_1](const Point & x) -> Real {
            bool D_1 = ( x(1) == 0.) ;
            bool D_2 = ( x(1) == 1.) && (0. < x(0) && x(0) < 0.25);
            if(D_1){return P_0;}
            else if(D_2){return P_1;}
            else{return 1.; }
        };

    Phi_b=[this](const Point & x) -> Real{
        Real logN = log(this->N_b(x));
        Real logP = log(this->P_b(x));
        return 0.5 * (logN - logP);
        //return 0.;
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_0 * P_0);
    alpha_P = 0.5 * log(N_0 * P_0);

    N_ini =[N_0,N_1](const Point & x) -> Real {
            return N_1 + (N_0 - N_1) * (1. -sqrt(x(1)));
      };

    P_ini =[P_0,P_1](const Point & x) -> Real {
            return P_1 + (P_0 - P_1) * (1. -sqrt(x(1)));
    };
}
*/
/*
    Regular(const Real _lambda = 1. , const Real _lN = 1., const Real _lP = 1., const Real N_D =2. , const Real  P_D = 2.)
      {

      description_tex = " " ;

      lambda = _lambda ;

      Lambda_N = [_lN](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lN , 0. ,
                0. , _lN;
          return   L;
        };

        Lambda_P = [_lP](const Point & x) -> TensorType {
            TensorType L;
            L <<
                _lP , 0. ,
                0. , _lP;
          return   L;
        };

        Lambda_Phi = [_lambda](const Point & x) -> TensorType {
            TensorType L;
            L <<
                1. , 0. ,
                0. , 1.;
          return   _lambda * _lambda *L;
        };

        isDirichlet = [](const Face & F) -> bool {
        Point x_F = F.barycenter();
        Real x = x_F(0);
        Real y = x_F(1);
        bool D_1 = ( y == 0.) ;
        bool D_2 = ( y == 1.) && (0. < x && x < 0.25);
        //bool D_2 = (y==1.) && (0. < x && x < 1.);
        //bool Dir = D_1 || D_2;
        bool Dir = true;
        bool Bound = F.isBoundary();
        return Dir && Bound;
      };

      isNeumann = [this](const Face & F) -> bool {
        //Point x_F = F.barycenter();
        //Real x = x_F(0);
        //Real y = x_F(1);
        bool Bound = F.isBoundary();
        bool Dir = this->isDirichlet(F);
        return Bound && (! Dir) ;
      };

      C =[](const Point & x) -> Real {
            return 0.; //N-region
      };

      N_b =[N_D](const Point & x) -> Real {
            return N_D;
        };

      P_b =[P_D](const Point & x) -> Real {
            return P_D;
        };

    Phi_b=[this](const Point & x) -> Real{
        Real logN = log(this->N_b(x));
        Real logP = log(this->P_b(x));
        return 0.5 * (logN - logP);
        };

    g_n =[](const Point & x) -> Real {
            return 0.;
        };

    alpha_N = 0.5 * log(N_D * P_D);
    alpha_P = 0.5 * log(N_D * P_D);

    N_ini =[N_D](const Point & x) -> Real {
        Real bord = x(0) * (x(0)-1.) * x(1) * (x(1)-1.);
        if(bord < 0.01){return N_D;}
        else{return 0.1;}
      };

    P_ini =[P_D](const Point & x) -> Real {
        Real bord = x(0) * (x(0)-1.) * x(1) * (x(1)-1.);
        if(bord < 0.01){return P_D;}
        else{return 0.9;}
    };

    }*/


   }; //struct Regular

  } // namespace pho
} // namespace ho

#endif
